# Sukina Framework
Sukina MVC Framework for Node.js created as replacement for PHP Kohana Framework in migration tasks.
The structure of application code is very similar to Kohana, with routes, Controllers, Models and Views.
