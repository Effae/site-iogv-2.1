module.exports = {

    'Sitemap'                                                   : 'Sitemap',
    'Nok'                                                       : 'НОК',
    'Welcome'                                                   : 'Главная',
    'Search'                                                    : 'Поиск',
    'Administrations'                                           : 'Администрация',
    'Subdivisions'                                              : 'Подразделения',
    'Subdivision'                                               : 'Подразделение',
    'Vacancys'                                                  : 'Вакансии',
    'Workers'                                                   : 'Работникик',
    'News'                                                      : 'Новости',
    'New'                                                       : 'Новость',
    'Events'                                                    : 'События',
    'Event'                                                     : 'Событие',
    'Articles'                                                  : 'Статьи',
    'Article'                                                   : 'Статья',
    'OrganizationsTable'                                        : 'Организации',
    'OrganizationsMap'                                          : 'Карта организаций',

}

