module.exports = class extends Application.System.Model{

    // config (считывание файла RequestHelperСonfig)
    map                 = Library_RequestHelperConfig.map;
    defaultServer       = Library_RequestHelperConfig.defaultServer;

    //функции
    async request(_path, _serverName){
        let result;

        try {
            //сервер
            if( typeof(_serverName) == "undefined") _serverName = this.defaultServer;
            let server = this.map[_serverName];

            //параметры запроса
            let options = {
                encoding:   server.encoding,
                uri:        server.url + "/" + _path,
                method:     server.method,
                headers:    {Authorization: "Basic " + new Buffer.from(server.login + ":" + server.password).toString("base64") },
                json:       true,
                timeout:    7000,
                body:       {}
            };

            //запрос
            await  Application.lib.request(options)
                .then(function (response) {
                    result = {
                        status:     true,
                        response:   response,
                        message:    null,
                    };
                })
                .catch(function (error) {
                    result = {
                        status:     false,
                        response:   null,
                        message:    error,
                    };
                });

        }catch (e) {}

        return result;
    }

}

