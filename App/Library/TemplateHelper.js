module.exports = class extends Application.System.Model{

    // config (считывание файла TemplateHelperСonfig)
    nameMap             = Library_TemplateHelperСonfig.nameMap;
    defaultTemplate     = Library_TemplateHelperСonfig.defaultTemplate;
    templatesFolder     = Library_TemplateHelperСonfig.templatesFolder;

    // GET функции
    getTemplateNameById(_templateId) {
        let templateName = this.defaultTemplate;

        if( !global.empty(this.nameMap[_templateId]) ) {
            templateName = this.nameMap[_templateId]
        }

        return this.templatesFolder + "." + templateName;
    }
    getIdByTemplateName(_templateName) {
        let id = 0;

        //по умолчнию
        for(let index in this.nameMap) {
            if(this.nameMap[index] == this.defaultTemplate){
                id = index;
                break;
            }
        }

        //искомый шаблон
        for(let index in this.nameMap) {
            if(this.nameMap[index] == _templateName){
                id = index;
                break;
            }
        }

        return id;
    }

}


