module.exports = {
    //карта alias сущностей и полей
    entities : {

        21: {
            name: "translations",
            fields: {
                156: "key",
                157: "description",
                158: "active",
                159: "order",
            }
        },

        22: {
            name: "translationsLocal",
            fields: {
                167: "text",
                168: "lang",
                169: "active",
                170: "order",
            }
        },

        25: {
            name: "organizations",
            fields: {
                213: "alias",
                214: "parent",
                215: "name",
                216: "fullname",
                217: "shortname",

                218: "type",
                219: "address",
                220: "phone",
                221: "site",
                222: "email",

                223: "coordinates",
                224: "photo",
                225: "managerPost",
                226: "managerFIO",
                227: "managerPhoto",

                228: "active",
                229: "order",
                230: "oid",
                231: "admin",
            }
        },

        26: {
            name: "buildings",
            fields: {
                238: "alias",
                239: "name",
                240: "address",
                241: "coordinates",
                242: "photo",

                243: "active",
                244: "order",
                245: "oid",
            }
        },

        27: {
            name: "subdivisions",
            fields: {
                252: "alias",
                253: "name",
                254: "fullname",
                255: "shortname",
                256: "address",

                257: "phone",
                258: "site",
                259: "email",
                260: "coordinates",
                261: "photo",

                262: "managerPost",
                263: "managerFIO",
                264: "managerPhoto",
                453: "buildingMain",
                454: "buildings",

                265: "active",
                266: "order",
                267: "iod",
            }
        },

        28: {
            name: "administrations",
            fields: {
                274: "post",
                275: "fio",
                276: "photo",
                277: "active",
                278: "order",
            }
        },

        29: {
            name: "vacancys",
            fields: {
                285: "name",
                286: "requirements",
                287: "salary",
                288: "contacts",
                289: "subdivision",

                290: "rate",
                291: "note",
                292: "housing",
                293: "active",
                294: "order",
            }
        },

        30: {
            name: "workers",
            fields: {
                301: "fio",
                302: "specialty",
                303: "qualification",
                304: "contacts",
                305: "photo",

                306: "level",
                307: "institution",
                308: "year",
                309: "active",
                310: "order",

                311: "oid",
            }
        },

        31: {
            name: "positions",
            fields: {
                318: "text",
                319: "schedule",
                320: "subdivisionOID",
                321: "active",
                322: "order",
            }
        },

        32: {
            name: "certificates",
            fields: {
                329: "text",
                330: "dateStart",
                331: "dateEnd",
                332: "active",
                333: "order",
            }
        },

        33: {
            name: "increases",
            fields: {
                340: "text",
                341: "dateStart",
                342: "dateEnd",
                343: "active",
                344: "order",
            }
        },

        34: {
            name: "articles",
            fields: {
                351: "alias",
                352: "name",
                353: "description",
                354: "parent",
                355: "active",

                356: "order",
                357: "nok",
            }
        },

        35: {
            name: "articlesLocal",
            fields: {
                364: "name",
                365: "fullname",
                366: "shortname",
                367: "text",
                368: "photo",

                369: "template",
                370: "lang",
                371: "meta",
                372: "active",
                373: "order",
            }
        },

        36: {
            name: "documents",
            fields: {
                380: "name",
                381: "fullname",
                382: "shortname",
                383: "number",
                384: "publication",

                385: "filename",
                386: "active",
                387: "order",
            }
        },

        37: {
            name: "news",
            fields: {
                394: "alias",
                395: "name",
                396: "description",
                397: "active",
                398: "order",
            }
        },

        38: {
            name: "newsLocal",
            fields: {
                405: "name",
                406: "fullname",
                407: "shortname",
                408: "text",
                409: "photo",

                410: "template",
                411: "lang",
                412: "meta",
                413: "active",
                414: "order",
            }
        },

        39: {
            name: "events",
            fields: {
                421: "alias",
                422: "name",
                423: "description",
                424: "active",
                425: "order",
            }
        },

        40: {
            name: "eventsLocal",
            fields: {
                432: "name",
                433: "fullname",
                434: "shortname",
                435: "text",
                436: "photo",

                437: "template",
                438: "lang",
                439: "meta",
                440: "active",
                441: "order",
            }
        },

        41: {
            name: "banners",
            fields: {
                448: "name",
                449: "photo",
                450: "link",
                451: "active",
                452: "order",
            }
        },

        42: {
            name: "menu",
            fields: {
                461: "name",
                462: "types",
                463: "orgtypes",
                464: "parent",
                465: "icon",

                466: "internal",
                467: "link",
                468: "active",
                469: "order"
            }
        },

        43: {
            name: "menuLocal",
            fields: {
                476: "text",
                477: "lang",
                478: "active",
                479: "order"
            }
        },

        44: {
            name: "defaultArticles",
            fields: {
                486: "alias",
                487: "name",
                488: "orgtypes",
                489: "parent",
                490: "description",

                491: "nok",
                492: "nameLocal",
                493: "text",
                494: "template",
                495: "active",

                496: "order",
            }
        },

        45: {
            name: "synchronizations",
            fields: {
                503: "organization",
                504: "admin",
                505: "orgOn",
                506: "orgPeriod",
                507: "orgStart",

                508: "orgEnd",
                509: "buiOn",
                510: "buiPeriod",
                511: "buiStart",
                512: "buiEnd",

                513: "subOn",
                514: "subPeriod",
                515: "subStart",
                516: "subEnd",
                517: "worOn",

                518: "worPeriod",
                519: "worStart",
                520: "worEnd",
                521: "active",
                522: "order",
            }
        },

    },

    //карта alias системных полей сущностей
    systemFields : {
        id:             "id",
        parent_id:      "parent_id",
        parent_item_id: "parent_item_id",
        linked_id:      "linked_id",
        date_added:     "date_added",
        date_updated:   "date_updated",
        created_by:     "created_by",
        sort_order:     "sort_order",
    },

    //карта alias полей списков
    listsFields : {
        id:             "id",
        name:           "name",
    },

    //карта alias полей значений списков
    listsChoicesFields : {
        id:             "id",
        parent_id:      "parent_id",
        lists_id:       "lists_id",
        name:           "name",
        is_default:     "is_default",
        bg_color:       "bg_color",
        sort_order:     "sort_order",
        users:          "users",
    },

    //карта alias списков (дополняет набор списков альтернативными алиасами)
    listsAlternativeAliases : {
        2: "langs",
        3: "menusTypes",
        4: "templatesTypes",
        5: "organizationsTypes",
    },

    //карта статических значений (для списков: ДА/НЕТ)
    staticValuesTrueFalse : {
        1: true,
        2: false,
    },

    //статические значения
    langsList   : '2',
    defaultLang : 'rus',
    filesFolder : 'uploads/attachments',
    orderType   : 'asc',
    orderIdType : 'asc',
    //orderType   : 'desc',
}