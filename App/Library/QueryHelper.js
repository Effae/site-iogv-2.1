module.exports = class extends Application.System.Model{

    // КОНСТАНТЫ
    defaultNameQH           = "QH";
    defaultNameDB           = "";

    tableEntities           = "app_entities";
    tableFields             = "app_fields";
    tableGlobalLists        = "app_global_lists";
    tableGlobalListsChoices = "app_global_lists_choices";
    prefixEntity            = "app_entity_";
    prefixField             = "field_";
    separator               = "*";
    postfixFilesLink        = "_link";
    postfixLat              = "_lat";
    postfixLon              = "_lon";

    nameGlobalLists         = "lists";
    nameGlobalListsChoices  = "listsChoices";
    prefixGlobalLists       = "list_";

    //ПАРАМЕТНЫ
    useAlias                = true;
    useJoinList             = false;
    useJoinEntity           = false;
    useJoinParent           = false;
    useJoinParentList       = false;
    useJoinParentEntity     = false;

    //конструктор
    constructor(_nameQH) {
        super();
        this.nameQH    = ( !global.empty(_nameQH) ) ? _nameQH : this.defaultNameQH;
        this.UseHelper = Application[this.nameQH];
    }

    //инициализация
    async initQueryHelper(_fileQHC, _nameDB){
        //создание глобального хранилища
        Application[this.nameQH]    = {};
        this.UseHelper              = Application[this.nameQH];

        this.UseHelper.nameQH       = this.nameQH;
        this.UseHelper.nameDB       = ( !global.empty(_nameDB) ) ? _nameDB : this.defaultNameDB;
        this.UseHelper.fileQHC      = _fileQHC;

        //---
        //ENTITIES
        //---

        //локальные обьекты
        let QHEntities                              = {};
        let QHEntitiesAliases                       = {};

        //получение данных
        //таблица сущностей
        let entitiesArray       = await this._getEntities();
        let entities            = {};
        entitiesArray.forEach   (function (entity) { entities[entity.id] = JSON.parse(JSON.stringify(entity));});
        //таблица полей
        let fieldsArray         = await this._getFields();
        let fields              = {};
        fieldsArray.forEach     (function (field) { fields[field.id] = JSON.parse(JSON.stringify(field));});

        //построение карт
        for(let indexEntity in entities) {
            //текущая сущность
            let entity                      = entities[indexEntity];

            //entities

            //создание обьекта сущности для карты
            let mapObject                   = {};

            mapObject["id"]                 = entity.id;
            mapObject["name"]               = this.getEntityNameById(entity.id);
            mapObject["alias"]              = this.getEntityAliasById(entity.id);
            mapObject["fields"]             = {
                id:             {id:null, name:"id", alias:this.UseHelper.fileQHC.systemFields.id},
                parent_id:      {id:null, name:"parent_id", alias:this.UseHelper.fileQHC.systemFields.parent_id},
                parent_item_id: {id:null, name:"parent_item_id", alias:this.UseHelper.fileQHC.systemFields.parent_item_id},
                linked_id:      {id:null, name:"linked_id", alias:this.UseHelper.fileQHC.systemFields.linked_id},
                date_added:     {id:null, name:"date_added", alias:this.UseHelper.fileQHC.systemFields.date_added},
                date_updated:   {id:null, name:"date_updated", alias:this.UseHelper.fileQHC.systemFields.date_updated},
                created_by:     {id:null, name:"created_by", alias:this.UseHelper.fileQHC.systemFields.created_by},
                sort_order:     {id:null, name:"sort_order", alias:this.UseHelper.fileQHC.systemFields.sort_order},
            };
            mapObject["join_parent"]        = {};
            mapObject["join_entities"]      = {};
            mapObject["join_lists"]         = {};

            //присоединение обьекта к карте
            QHEntities[mapObject["name"]]   = mapObject;

            //связь родителя
            if(entity.parent_id != 0){
                mapObject["join_parent"] = {
                    entity_id:          mapObject.id,
                    entity_name:        mapObject.name,
                    entity_alias:       mapObject.alias,
                    field_id:           null,
                    field_name:         "parent_item_id",
                    field_alias:        this.UseHelper.fileQHC.systemFields.parent_item_id,
                    join_entity_id:     entity.parent_id,
                    join_entity_name:   this.getEntityNameById(entity.parent_id),
                    join_entity_alias:  this.getEntityAliasById(entity.parent_id),
                    join_field_id:      null,
                    join_field_name:    "id",
                    join_field_alias:   this.UseHelper.fileQHC.systemFields.id,
                };
            }

            //обход полей
            for(let indexField in fields) {
                //текущее полей
                let field = fields[indexField];

                //проверка принадлежности поля к текущей сущности
                if(field.entities_id == entity.id){

                    //проверка на НЕ системное поле
                    //добавление в карту
                    let fieldObject = {};
                    if( ['fieldtype_action', 'fieldtype_id', 'fieldtype_date_added', 'fieldtype_date_updated', 'fieldtype_created_by', 'fieldtype_parent_item_id'].indexOf(field.type) == -1){
                        fieldObject = {
                            id:     field.id,
                            name:   this.getFieldNameById(field.id),
                            alias:  this.getFieldAliasById(entity.id, field.id),
                        };
                        mapObject["fields"][fieldObject.name] = fieldObject;
                    }


                    //проверка на наличие связи
                    //связь определена как список
                    if(field.type == "fieldtype_dropdown"){
                        let configuration = JSON.parse(field.configuration);
                        mapObject["join_lists"][fieldObject.name] = {
                            entity_id:          mapObject.id,
                            entity_name:        mapObject.name,
                            entity_alias:       mapObject.alias,
                            field_id:           fieldObject.id,
                            field_name:         fieldObject.name,
                            field_alias:        fieldObject.alias,
                            join_entity_id:     null,
                            join_entity_name:   this.tableGlobalListsChoices,
                            join_entity_alias:  this.tableGlobalListsChoices,
                            join_field_id:      null,
                            join_field_name:    "id",
                            join_field_alias:   "id",
                            join_listsTable:    this.tableGlobalLists,
                            join_listsTable_field: "id",
                            join_listsTable_value: configuration.use_global_list,
                        };
                    }

                    //проверка на наличие связи
                    //связь определена как сущность
                    if(field.type == "fieldtype_entity" || field.type == "fieldtype_entity_ajax"){
                        let configuration = JSON.parse(field.configuration);
                        if( configuration.display_as == "dropdown")
                        mapObject["join_entities"][fieldObject.name] = {
                            entity_id:          mapObject.id,
                            entity_name:        mapObject.name,
                            entity_alias:       mapObject.alias,
                            field_id:           fieldObject.id,
                            field_name:         fieldObject.name,
                            field_alias:        fieldObject.alias,
                            join_entity_id:     configuration.entity_id,
                            join_entity_name:   this.getEntityNameById(configuration.entity_id),
                            join_entity_alias:  this.getEntityAliasById(configuration.entity_id),
                            join_field_id:      null,
                            join_field_name:    "id",
                            join_field_alias:   this.UseHelper.fileQHC.systemFields.id,
                        };
                    }
                }
            }

            //entities alias

            //создание обьекта сущности для Alias карты
            let  mapAliasObject             = {}

            mapAliasObject["id"]            = mapObject["id"];
            mapAliasObject["name"]          = mapObject["name"];
            mapAliasObject["alias"]         = mapObject["alias"];
            mapAliasObject["fields"]        = {};
            mapAliasObject["join_parent"]   = mapObject["join_parent"];
            mapAliasObject["join_entities"] = {};
            mapAliasObject["join_lists"]    = {};

            //обход fields
            for(let index in  mapObject["fields"]) {
                mapAliasObject["fields"][mapObject["fields"][index].alias] = mapObject["fields"][index];
            }
            //обход join_entities
            for(let index in  mapObject["join_entities"]) {
                mapAliasObject["join_entities"][mapObject["join_entities"][index].field_alias] = mapObject["join_entities"][index];
            }
            //обход join_lists
            for(let index in  mapObject["join_lists"]) {
                mapAliasObject["join_lists"][mapObject["join_lists"][index].field_alias] = mapObject["join_lists"][index];
            }

            //присоединение обьекта к Alias карте
            QHEntitiesAliases[mapAliasObject["alias"]]    = mapAliasObject;
        }

        //загрузка карт сущностей в глобальные переменные
        this.UseHelper.Entities                     = QHEntities;
        this.UseHelper.EntitiesAliases              = QHEntitiesAliases;

        //---
        //LISTS
        //---

        //локальные обьекты
        let QHLists                                 = {};
        let QHListsAliases                          = {};

        //получение данных
        //таблица списков
        let listsArray          = await this._getLists();
        let lists               = {};
        listsArray.forEach      (function (list) { lists[list.id] = JSON.parse(JSON.stringify(list));});

        //построение карт
        for(let index in lists){
            QHLists[index] = this.prefixGlobalLists + index;
        }
        for(let index in QHLists) {
            QHListsAliases[QHLists[index]] = index;
        }
        for(let index in this.UseHelper.fileQHC.listsAlternativeAliases) {
            if( typeof(QHLists[index]) != "undefined" )
                QHListsAliases[this.UseHelper.fileQHC.listsAlternativeAliases[index]] = index;
        }

        //загрузка карт списков в глобальные переменные
        this.UseHelper.Lists                        = QHLists;
        this.UseHelper.ListsAliases                 = QHListsAliases;

        //---
        //LISTS FIELDS
        //---

        //локальные обьекты
        let QHListsFields                           = {};
        let QHListsFieldsAliases                    = {};

        //построение карт
        for(let index in this.UseHelper.fileQHC.listsFields) {
            QHListsFields[index] = this.UseHelper.fileQHC.listsFields[index];
        }
        for(let index in QHListsFields) {
            QHListsFieldsAliases[QHListsFields[index]] = index;
        }

        //загрузка карт полей списков в глобальные переменные
        this.UseHelper.ListsFields                  = QHListsFields;
        this.UseHelper.ListsFieldsAliases           = QHListsFieldsAliases;

        //---
        //LISTS CHOICES FIELDS
        //---

        //локальные обьекты
        let QHListChoicesFields                     = {};
        let QHListChoicesFieldsAliases              = {};

        //построение карт
        for(let index in this.UseHelper.fileQHC.listsChoicesFields) {
            QHListChoicesFields[index] = this.UseHelper.fileQHC.listsChoicesFields[index];
        }
        for(let index in QHListChoicesFields) {
            QHListChoicesFieldsAliases[QHListChoicesFields[index]] = index;
        }

        //загрузка карт полей значений списков в глобальные переменные
        this.UseHelper.ListsChoicesFields           = QHListChoicesFields;
        this.UseHelper.ListsChoicesFieldsAliases    = QHListChoicesFieldsAliases;

        //---
        //STATIC VALUES
        //---

        //true-false

        //локальные обьекты
        let QHStaticValuesTrueFalse                 = {};
        let QHStaticValuesTrueFalseAliases          = {};

        //построение значений
        for(let index in this.UseHelper.fileQHC.staticValuesTrueFalse) {
            QHStaticValuesTrueFalse[index] = this.UseHelper.fileQHC.staticValuesTrueFalse[index];
        }
        for(let index in QHStaticValuesTrueFalse) {
            QHStaticValuesTrueFalseAliases[QHStaticValuesTrueFalse[index]] = index;
        }

        //загрузка значений true-false в глобальные переменные
        this.UseHelper.StaticValuesTrueFalse         = QHStaticValuesTrueFalse;
        this.UseHelper.QHStaticValuesTrueFalseAliases= QHStaticValuesTrueFalseAliases;

        //langs

        //локальные обьекты
        let QHStaticValuesLangs                     = {};
        let QHStaticValuesLangsAliases              = {};

        //получение данных
        //список языков
        let langsArray          = await this._getListChoices(this.UseHelper.fileQHC.langsList);
        let langs               = {};
        langsArray.forEach      (function (lang) { langs[lang.id] = JSON.parse(JSON.stringify(lang));});

        //построение значений
        for(let index in langs) {
            QHStaticValuesLangs[index] = langs[index].name;
        }
        for(let index in QHStaticValuesLangs) {
            QHStaticValuesLangsAliases[QHStaticValuesLangs[index]] = index;
        }

        //загрузка значений м в глобальные переменные
        this.UseHelper.StaticValuesLangs            = QHStaticValuesLangs;
        this.UseHelper.StaticValuesLangsAliases     = QHStaticValuesLangsAliases;

        //---
        //VARIABLES and FUNCTIONS
        //---

        //тип сортировки по умочанию
        this.UseHelper.order            = this.UseHelper.fileQHC.orderType;
        //тип сортировки ID по умочанию
        this.UseHelper.orderId          = this.UseHelper.fileQHC.orderIdType;
        //язык по умочанию
        this.UseHelper.dl               = this.UseHelper.fileQHC.defaultLang;
        if( typeof(this.UseHelper.StaticValuesLangsAliases[this.UseHelper.dl]) == "undefined" ){
            this.UseHelper.dl = Object.keys(this.UseHelper.StaticValuesLangsAliases)[0];
        }
        //масив языков
        this.UseHelper.langs            = [];
        for(let index in this.UseHelper.StaticValuesLangs) {
            this.UseHelper.langs.push(this.UseHelper.StaticValuesLangs[index])
        }

        let thisUseHelper = this.UseHelper;

        //значение языка по алиасу
        this.UseHelper.lnv              = function (_value){ return thisUseHelper.StaticValuesLangsAliases[_value];};
        //значение списка по алиасу
        this.UseHelper.liv              = function (_value){ return thisUseHelper.ListsAliases[_value];};
        //значение булевы по алиасу
        this.UseHelper.tfv              = function (_value){ return thisUseHelper.QHStaticValuesTrueFalseAliases[_value];};

        //любое значение по алиасу
        let QHsvArray                   = Object.assign(this.UseHelper.StaticValuesLangsAliases,
                                                        this.UseHelper.ListsAliases,
                                                        this.UseHelper.QHStaticValuesTrueFalseAliases);
        this.UseHelper.sv               = function (_value){ return QHsvArray[_value];};
    }

    // SET функции
    setUseAlias(_useAlias) {
        this.useAlias = _useAlias;
        return this;
    }
    setUseJoinList(_useJoinList) {
        this.useJoinList            = _useJoinList;
        return this;
    }
    setUseJoinEntity(_useJoinEntity) {
        this.useJoinEntity          = _useJoinEntity;
        return this;
    }
    setUseJoinParent(_useJoinParent) {
        this.useJoinParent          = _useJoinParent;
        return this;
    }
    setUseJoinParentList(_useJoinParentList) {
        this.useJoinParentList      = _useJoinParentList;
        return this;
    }
    setUseJoinParentEntity(_useJoinParentEntity) {
        this.useJoinParentEntity    = _useJoinParentEntity;
        return this;
    }
    setUseJoin(_useJoinList, _useJoinEntity, _useJoinParent, _useJoinParentList, _useJoinParentEntity ) {
            this.useJoinList            = _useJoinList;
            if(typeof (_useJoinList)            == "undefined" ) this.useJoinList = false;
            this.useJoinEntity          = _useJoinEntity;
            if(typeof (_useJoinEntity)          == "undefined" ) this.useJoinEntity = false;
            this.useJoinParent          = _useJoinParent;
            if(typeof (_useJoinParent)          == "undefined" ) this.useJoinParent = false;
            this.useJoinParentList      = _useJoinParentList;
            if(typeof (_useJoinParentList)      == "undefined" ) this.useJoinParentList = false;
            this.useJoinParentEntity    = _useJoinParentEntity;
            if(typeof (_useJoinParentEntity)    == "undefined" ) this.useJoinParentEntity = false;
        return this;
    }
    setUseJoinAll(_useJoin) {
            this.useJoinList            = _useJoin;
            this.useJoinEntity          = _useJoin;
            this.useJoinParent          = _useJoin;
            this.useJoinParentList      = _useJoin;
            this.useJoinParentEntity    = _useJoin;
        return this;
    }

    // GET функции
    getEntityNameById(_entityId) {
        return this.prefixEntity + _entityId;
    }
    getFieldNameById(_fieldId) {
        return this.prefixField + _fieldId;
    }
    getEntityAliasById(_entityId) {
        let alias = this.getEntityNameById(_entityId);
        try {
            alias = this.UseHelper.fileQHC.entities[_entityId].name;
        }catch (e) {}
        return alias;
    }
    getFieldAliasById(_entityId, _fieldId) {
        let alias = this.getFieldNameById(_fieldId);
        try {
            alias = this.UseHelper.fileQHC.entities[_entityId].fields[_fieldId];
        }catch (e) {}
        return alias;
    }

    //полчение данных системных таблиц
    async _getEntities() {
        return await this.DB(this.UseHelper.nameBD).select().from(this.tableEntities);
    }
    async _getFields() {
        return await this.DB(this.UseHelper.nameBD).select().from(this.tableFields);
    }
    async _getLists() {
        return await this.DB(this.UseHelper.nameBD).select().from(this.tableGlobalLists);
    }
    async _getEntity(_entityId) {
        return await this.DB(this.UseHelper.nameBD).select().from(this.getEntityNameById(_entityId));
    }
    async _getListChoices(_listId) {
        return await this.DB(this.UseHelper.nameBD).select().from(this.tableGlobalListsChoices).where("lists_id", '=' , _listId);
    }

    //функции Join
    _joinList(request, baseEntityName, baseFieldName) {
        //соединение со списком

        //используемые обьекты
        let QHEntitiesObject        = this.UseHelper.Entities[baseEntityName];
        let QHEntitiesJoinBlock     = this.UseHelper.Entities[baseEntityName].join_lists[baseFieldName]

        //используемые имена
        let useBaseEntityName       = QHEntitiesJoinBlock.entity_name;
        let useBaseFieldName        = QHEntitiesJoinBlock.field_name;
        let useJoinEntityName       = QHEntitiesJoinBlock.join_entity_name;
        let useJoinFieldName        = QHEntitiesJoinBlock.join_field_name;
        let useBaseEntityAlias      = QHEntitiesJoinBlock.entity_alias;
        let useBaseFieldAlias       = QHEntitiesJoinBlock.field_alias;

        let joinName                = useBaseEntityName + this.separator + useBaseFieldName;

        //используемы alias
        if(this.useAlias){
            useBaseEntityName       = useBaseEntityAlias;
            joinName                = useBaseEntityAlias + this.separator + useBaseFieldAlias;

            for(var index in this.UseHelper.ListsChoicesFields) {
                request.column( joinName + "." + index + " AS " + this.UseHelper.ListsChoicesFields[index])
            }
        }

        //join
        request.leftOuterJoin(useJoinEntityName + " AS " + joinName, joinName + "." + useJoinFieldName , useBaseEntityName + "." + useBaseFieldName);

        return request;
    }
    _joinEntity(request, baseEntityName, baseFieldName) {
        //соединение с сущностью через поле

        //используемые обьекты
        let QHEntitiesObject        = this.UseHelper.Entities[baseEntityName];
        let QHEntitiesJoinBlock     = this.UseHelper.Entities[baseEntityName].join_entities[baseFieldName];
        let QHEntitiesObjectJoin    = this.UseHelper.Entities[QHEntitiesJoinBlock.join_entity_name];


        //используемые имена
        let useBaseEntityName       = QHEntitiesJoinBlock.entity_name;
        let useBaseFieldName        = QHEntitiesJoinBlock.field_name;
        let useJoinEntityName       = QHEntitiesJoinBlock.join_entity_name;
        let useJoinFieldName        = QHEntitiesJoinBlock.join_field_name;
        let useBaseEntityAlias      = QHEntitiesJoinBlock.entity_alias;
        let useBaseFieldAlias       = QHEntitiesJoinBlock.field_alias;

        let joinName                = useBaseEntityName + this.separator + useBaseFieldName;
        if(this.useAlias){
            useBaseEntityName       = useBaseEntityAlias;
            joinName                = useBaseEntityAlias + this.separator + useBaseFieldAlias;

            for(var index in QHEntitiesObjectJoin.fields) {
                request.column( joinName + "." + QHEntitiesObjectJoin.fields[index].name + " AS " +  QHEntitiesObjectJoin.fields[index].alias)
            }
        }

        //join
        request.leftOuterJoin(useJoinEntityName + " AS " + joinName, joinName + "." + useJoinFieldName , useBaseEntityName + "." + useBaseFieldName);

        //рекурсивные join
        // !!! ДАЛЬНЕЙШАЯ РЕКУРСИЯ ЗАПРЕЩЕНА !!!

        return request;
    }
    _joinParent(request, baseEntityName) {
        //соединение с родительской сущностью

        //используемые обьекты
        let QHEntitiesObject        = this.UseHelper.Entities[baseEntityName];
        let QHEntitiesJoinBlock     = this.UseHelper.Entities[baseEntityName].join_parent;
        let QHEntitiesObjectJoin    = this.UseHelper.Entities[QHEntitiesJoinBlock.join_entity_name];

        //используемые имена
        let useBaseEntityName       = QHEntitiesJoinBlock.entity_name;
        let useBaseFieldName        = QHEntitiesJoinBlock.field_name;
        let useJoinEntityName       = QHEntitiesJoinBlock.join_entity_name;
        let useJoinFieldName        = QHEntitiesJoinBlock.join_field_name;
        let useBaseEntityAlias      = QHEntitiesJoinBlock.entity_alias;
        let useJoinEntityAlias      = QHEntitiesJoinBlock.join_entity_alias;

        let joinName                = useJoinEntityName;
        if(this.useAlias){
            useBaseEntityName       = useBaseEntityAlias;
            joinName                = useJoinEntityAlias;

            for(var index in QHEntitiesObjectJoin.fields) {
                request.column( joinName + "." + QHEntitiesObjectJoin.fields[index].name + " AS " +  QHEntitiesObjectJoin.fields[index].alias)
            }
        }

        //join
        request.leftOuterJoin(useJoinEntityName + " AS " + joinName, joinName + "." + useJoinFieldName , useBaseEntityName + "." + useBaseFieldName );

        //рекурсивные join

        //наличие указания на join списков
        if(this.useJoinParentList && !global.empty(QHEntitiesObjectJoin['join_lists'])){
            for (let index in QHEntitiesObjectJoin['join_lists']) {
                this._joinList(request, QHEntitiesObjectJoin.name , QHEntitiesObjectJoin['join_lists'][index].field_name);
            }
        }
        //наличие указания на join сущностей
        if(this.useJoinParentEntity && !global.empty(QHEntitiesObjectJoin['join_entities'])){
            for (let index in QHEntitiesObjectJoin['join_entities']) {
                this._joinEntity(request, QHEntitiesObjectJoin.name , QHEntitiesObjectJoin['join_entities'][index].field_name);
            }
        }
        //наличие указания на join родительской сущности
        if(this.useJoinParent && !global.empty(QHEntitiesObjectJoin['join_parent'])){
            this._joinParent(request, QHEntitiesObjectJoin.name);
        }

        return request;
    }
    _joinThis(request, baseEntityName) {
        //соединение с текущей сущностью

        //используемые обьекты
        let QHEntitiesObject        = this.UseHelper.Entities[baseEntityName];

        //рекурсивные join

        //наличие указания на join списков
        if(this.useJoinList && !global.empty(QHEntitiesObject['join_lists'])){
            for (let index in QHEntitiesObject['join_lists']) {
                this._joinList(request, QHEntitiesObject.name , QHEntitiesObject['join_lists'][index].field_name);
            }
        }
        //наличие указания на join сущностей
        if(this.useJoinEntity && !global.empty(QHEntitiesObject['join_entities'])){
            for (let index in QHEntitiesObject['join_entities']) {
                this._joinEntity(request, QHEntitiesObject.name , QHEntitiesObject['join_entities'][index].field_name);
            }
        }

        return request;
    }
    _join(request, baseEntityName) {

        //join
        if(!global.empty(this.UseHelper.Entities[baseEntityName])){
            if(this.useJoinList || this.useJoinEntity){
                this._joinThis(request, baseEntityName);
            }
            if(this.useJoinParent && !global.empty(this.UseHelper.Entities[baseEntityName]['join_parent'])){
                this._joinParent(request, baseEntityName);
            }
        }

        return request;
    }

    //запросы
    _request(_table) {

        //используемые обьекты
        let entityName                  = _table
        if(this.useAlias){entityName    = this.UseHelper.EntitiesAliases[_table].name}
        let QHEntitiesObject            = this.UseHelper.Entities[entityName];
        let QHEntitiesAliasObject       = this.UseHelper.EntitiesAliases[QHEntitiesObject.alias];

        //используемые имена
        let useEntityName               = QHEntitiesObject.name;
        if(this.useAlias){useEntityName = QHEntitiesObject.alias;}

        //request
        let request = this.DB(this.UseHelper.nameBD).from(entityName + " AS " + useEntityName).options({nestTables:true});

        //columns
        if(this.useAlias){
            for(var index in QHEntitiesObject.fields) {
                request.column( QHEntitiesObject.alias + "." + QHEntitiesObject.fields[index].name + " AS " +  QHEntitiesObject.fields[index].alias)
            }
        }

        //join
        this._join(request, QHEntitiesObject.name);

        return request;
    }
    requestSelect(_table) {
        //base request
        let request = this._request(_table);

        return request.select();
    }
    requestUpdate(_table) {
        //base request
        let request = this._request(_table);

        //используемые обьекты
        let entityName                  = _table
        if(this.useAlias){entityName    = this.UseHelper.EntitiesAliases[_table].name}
        let QHEntitiesObject            = this.UseHelper.Entities[entityName];
        let QHEntitiesAliasObject       = this.UseHelper.EntitiesAliases[QHEntitiesObject.alias];

        //используемые имена
        let useEntityName               = QHEntitiesObject.name;
        if(this.useAlias){useEntityName = QHEntitiesObject.alias;}

        //автозаполение
        let timestamp = Math.floor(Date.now() / 1000);
        request.update(useEntityName + '.date_updated',  timestamp);

        return request;
    }
    requestInsert(_table, _insertObject) {
        //base request
        //не требуется

        //используемые обьекты
        let entityName                  = _table
        if(this.useAlias){entityName    = this.UseHelper.EntitiesAliases[_table].name}
        let QHEntitiesObject            = this.UseHelper.Entities[entityName];
        let QHEntitiesAliasObject       = this.UseHelper.EntitiesAliases[QHEntitiesObject.alias];

        //используемые имена
        //не требуется

        //обьект вставки
        let insertObject                = {};
        if(!this.useAlias){
            for(let index in _insertObject) {
                if(!global.empty(QHEntitiesObject.fields[index])) {
                    insertObject[QHEntitiesObject.fields[index].name] = _insertObject[index];
                }
            }
        }
        if(this.useAlias){
            for(let index in _insertObject) {
                if(!global.empty(QHEntitiesAliasObject.fields[index])){
                    insertObject[QHEntitiesAliasObject.fields[index].name] = _insertObject[index];
                }
            }
        }

        //автозаполение обьекта вставки
        for(let index in QHEntitiesObject.fields) {
            if(typeof (insertObject[index]) == "undefined" && global.empty(this.UseHelper.fileQHC.systemFields[index])){
                insertObject[index] = "";
            }
        }
        let timestamp = Math.floor(Date.now() / 1000);
        if( global.empty(insertObject.date_added) )     insertObject.date_added     = timestamp;
        if( global.empty(insertObject.date_updated) )   insertObject.date_updated   = timestamp;

        //request
        let request = this.DB(this.UseHelper.nameBD).insert(insertObject).into(QHEntitiesObject.name);

        return  request;
    }
    requestDeleteByIds(_table, _idArray) {
        //base request
        //не требуется

        //используемые обьекты
        let entityName                  = _table
        if(this.useAlias){entityName    = this.UseHelper.EntitiesAliases[_table].name}
        let QHEntitiesObject            = this.UseHelper.Entities[entityName];
        let QHEntitiesAliasObject       = this.UseHelper.EntitiesAliases[QHEntitiesObject.alias];

        //используемые имена
        //не требуется

        //request
        let request = this.DB(this.UseHelper.nameBD).from(QHEntitiesObject.name).options({nestTables:true}).where(QHEntitiesObject.fields['id'].name, "IN", _idArray).del();

        return request;
    }
    requestList() {

        //используемые имена
        let useListName                 = this.nameGlobalListsChoices;
        let field                       = "lists_id"
        if(this.useAlias){field         = this.UseHelper.ListsChoicesFields[field] }

        //request
        let request = this.DB(this.UseHelper.nameBD).from(this.tableGlobalListsChoices + " AS " + useListName).options({nestTables:true});

        //columns
        if(this.useAlias){
            for(var index in this.UseHelper.ListsChoicesFieldsAliases) {
                request.column( useListName + "." + this.UseHelper.ListsChoicesFieldsAliases[index] + " AS " +  index)
            }
        }

        //join
        if(this.useJoinParent){
            let joinName = this.nameGlobalLists;
            let useJoinFieldName = ( this.useAlias ) ? 'id' : 'id';
            let useBaseFieldName = ( this.useAlias ) ? this.UseHelper.ListsChoicesFields['lists_id'] : 'lists_id';

            request.leftOuterJoin(this.tableGlobalLists + " AS " + joinName, joinName + "." + useJoinFieldName , useListName + "." + useBaseFieldName);

            //columns
            if(this.useAlias){
                for(var index in this.UseHelper.ListsFieldsAliases) {
                    request.column( joinName + "." + this.UseHelper.ListsFieldsAliases[index] + " AS " +  index)
                }
            }
        }

        return request.select();
    }

    //указание имени поля
    requestField(_aliasName){
        let resultName      = _aliasName;

        if(this.useAlias){
            let allArray    = _aliasName.split('.');
            let fieldName   = "";

            let tableArray  = allArray[0].split(this.separator);
            if( tableArray.length == 1 ){
                //без SEPARATOR
                //сущность
                if( !global.empty(this.UseHelper.EntitiesAliases[allArray[0]]) ){
                    fieldName = this.UseHelper.EntitiesAliases[allArray[0]].fields[allArray[1]].name
                }
                //список
                else if( allArray[0] == this.nameGlobalLists ){
                    fieldName = this.UseHelper.ListsFieldsAliases[allArray[1]]
                }
                //значение списка
                else if( allArray[0] == this.nameGlobalListsChoices){
                    fieldName = this.UseHelper.ListsChoicesFieldsAliases[allArray[1]]
                }
            }else{
                //с SEPARATOR
                let startTableName = tableArray[0];
                let startFieldName = tableArray[1];

                if(!global.empty(this.UseHelper.EntitiesAliases[startTableName]["join_entities"][startFieldName])){
                    let tableName = this.UseHelper.EntitiesAliases[startTableName]["join_entities"][startFieldName].join_entity_alias;
                    fieldName = this.UseHelper.EntitiesAliases[tableName].fields[allArray[1]].name;
                }
                else if(!global.empty(this.UseHelper.EntitiesAliases[startTableName]["join_lists"][startFieldName])){
                    fieldName = this.UseHelper.ListsChoicesFieldsAliases[allArray[1]];
                }
            }

            resultName = allArray[0] + "." + fieldName;
        }

        return resultName;
    }
    //получение ссылки для имени фала
    fileLinkByName(_fileName){
        let result = "";
        try {
            if( !global.empty( _fileName ) ){
                let data        = new Date(Number.parseInt(_fileName.substring(0, 10) + "000"))
                //let filename    = _fileName.substring(11);

                let pathFolder  = this.UseHelper.fileQHC.filesFolder;
                let year        = data.getFullYear();
                let month       = data.getMonth() + 1;
                let date        = data.getDate();
                month           = month > 9 ? month : "0" + month;
                date            = date > 9 ? date : "0" + date;

                let pathDir     = year + "/" + month + "/" + date;
                let pathSha1    = sha1(_fileName);

                let path        = "/" + pathFolder + "/" + pathDir + "/" + pathSha1;

                result          = path;
            }
        }catch (e) {}
        return result;
    }
    //генерация алиас
    generationAlias(_string){
        let result = "";
        try {
            //соответствие сомволов
            let converter = {
                'а': 'a','б': 'b','в': 'v','г': 'g','д': 'd',
                'е': 'e','ё': 'e','ж': 'zh','з': 'z','и': 'i',
                'й': 'y','к': 'k','л': 'l','м': 'm','н': 'n',
                'о': 'o','п': 'p','р': 'r','с': 's','т': 't',
                'у': 'u','ф': 'f','х': 'h','ц': 'c','ч': 'ch',
                'ш': 'sh','щ': 'sch','ь': '','ы': 'y','ъ': '',
                'э': 'e','ю': 'yu','я': 'ya',

                'a': 'a','b': 'b','c': 'c','d': 'd','e': 'e',
                'f': 'f','g': 'g','h': 'h','i': 'i','j': 'j',
                'k': 'k','l': 'l','m': 'm','n': 'n','o': 'o',
                'p': 'p','q': 'q','r': 'r','s': 's','t': 't',
                'u': 'u','v': 'v','w': 'w','x': 'x','y': 'y',
                'z': 'z',

                '0': '0','1': '1','2': '2','3': '3','4': '4',
                '5': '5','6': '6','7': '7','8': '8','9': '9',

                '_': '-','-': '-',' ': '-'
            };

            let alias = "";
            for (let i = 0; i < _string.length; i++) {
                if( typeof(converter[_string[i]]) != "undefined")
                    alias = alias + converter[_string[i]];
            }

            result = alias;
        }catch (e) {}
        return result;
    }

    //ФУНКЦИИ СТАТИЧЕСКИХ ЗНАЧЕНИЙ
    //тип сортировки
    order(){
        return this.UseHelper.order;
    }
    //тип сортировки Id
    orderId(){
        return  this.UseHelper.orderId;
    }
    //получение языка по умолчанию
    dl(){
        return this.UseHelper.dl;
    }
    //массив всех языков
    langs(){
        return this.UseHelper.langs;
    }
    //получение id булева значения
    tfv(_value){
        return this.UseHelper.tfv(_value);
    }
    //получение id языка
    lnv(_value){
        return this.UseHelper.lnv(_value);
    }
    //получение id списка
    liv(_value){
        return this.UseHelper.liv(_value);
    }
    //получение id статического значения (обьединение прочих методов)
    sv(_value){
        return this.UseHelper.sv(_value);
    }

    //ФУНКЦИИ ВСПОМОГАТЕЛЬНОЙ ОБРАБОТКИ РЕЛУЛЬТАТОВ
    //обьединение результатов (поиск соответствий по полю)
    resultMerge(_resultArrayOld, _resultArrayNew, _fieldName){
        //приведение к масиву
        //if( !Array.isArray(_resultArrayOld) ) { _resultArrayOld = [_resultArrayOld]; }
        //if( !Array.isArray(_resultArrayNew) ) {_resultArrayNew = [_resultArrayNew]; }

        let result          = _resultArrayOld;
        let nаmeArray       = _fieldName.split('.');

        //получение нового результата как обьекта
        let resultObjectNew = {}
        if(nаmeArray.length == 1){
            for(let index in _resultArrayNew) {
                resultObjectNew[_resultArrayNew[index][nаmeArray[0]]] = _resultArrayNew[index];
            }
        }
        if(nаmeArray.length == 2){
            for(let index in _resultArrayNew) {
                resultObjectNew[_resultArrayNew[index][nаmeArray[0]][nаmeArray[1]]] = _resultArrayNew[index];
            }
        }

        //замещение обьектов старого результата
        if(nаmeArray.length == 1){
            for(let index in result) {
                if( typeof (resultObjectNew[result[index][nаmeArray[0]]]) != "undefined")
                result[index] = resultObjectNew[result[index][nаmeArray[0]]];
            }
        }
        if(nаmeArray.length == 2){
            for(let index in result) {
                if( typeof (resultObjectNew[result[index][nаmeArray[0]][nаmeArray[1]]]) != "undefined")
                    result[index] = resultObjectNew[result[index][nаmeArray[0]][nаmeArray[1]]];
            }
        }

        return result;
    }
    //оставить только указанные поля
    resultLeaveFields(_resultArray, _fieldNameArray) {
        //приведение к масиву
        //if( !Array.isArray(_resultArray) ) { _resultArray = [_resultArray]; }
        if( !Array.isArray(_fieldNameArray) ) { _fieldNameArray = [_fieldNameArray]; }

        let result = [];

        //перебор записей
        for(let index in _resultArray) {
            let finalItem = {};

            //перебор запрошенных полей
            for(let indexFieldName in _fieldNameArray) {
                let nаmeArray = _fieldNameArray[indexFieldName].split('.');

                //сохранение значения
                if(nаmeArray.length == 1){
                    finalItem[nаmeArray[0]] = _resultArray[index][nаmeArray[0]];
                }
                if(nаmeArray.length == 2){
                    if(typeof (finalItem[nаmeArray[0]]) == "undefined") finalItem[nаmeArray[0]] = {};
                    finalItem[nаmeArray[0]][nаmeArray[1]] = _resultArray[index][nаmeArray[0]][nаmeArray[1]];
                }
            }

            result.push(finalItem);
        }

        //замена исходного результата
        for(let index in _resultArray) {
            _resultArray[index] = result[index]
        }

        return result;
    }
    //удалить все указанные поля
    resultRemoveFields(_resultArray, _fieldNameArray) {
        //приведение к масиву
        //if( !Array.isArray(_resultArray) ) { _resultArray = [_resultArray]; }
        if( !Array.isArray(_fieldNameArray) ) { _fieldNameArray = [_fieldNameArray]; }

        let result = [];

        //перебор записей
        for(let index in result) {

            //добавление НЕ указаных полей
            for(let indexEntity in _resultArray[index]) {
                if(typeof ( _resultArray[index][indexEntity] ) != "object"){
                    if(_fieldNameArray.indexOf(indexEntity) == -1){
                        finalItem[indexEntity] =  _resultArray[index][indexEntity];
                    }
                }
                if(typeof ( _resultArray[index][indexEntity] ) == "object"){
                    for(let indexField in _resultArray[index][indexEntity]) {
                        if(_fieldNameArray.indexOf(indexEntity + "." + indexField) == -1){
                            if(typeof (finalItem[indexEntity]) == "undefined") finalItem[indexEntity] = {};
                            finalItem[indexEntity][indexField] = _resultArray[index][indexEntity][indexField];
                        }
                    }
                }
            }

            result.push(finalItem);
        }

        //замена исходного результата
        for(let index in _resultArray) {
            _resultArray[index] = result[index]
        }

        return result;
    }
    //добавление ссылки на файл (указанным полям)
    resultAddFilesLink(_resultArray, _fieldNameArray) {
        //приведение к масиву
        //if( !Array.isArray(_resultArray) ) { _resultArray = [_resultArray]; }
        if( !Array.isArray(_fieldNameArray) ) { _fieldNameArray = [_fieldNameArray]; }

        let result       = _resultArray;

        //перебор запрошенных полей
        for(let indexFieldName in _fieldNameArray) {
            let nаmeArray    = _fieldNameArray[indexFieldName].split('.');

            //замена значений
            if(nаmeArray.length == 1){
                for(let index in result) {
                    if(typeof (result[index][nаmeArray[0]]) != 'undefined')
                        result[index][nаmeArray[0] + this.postfixFilesLink] = this.fileLinkByName(result[index][nаmeArray[0]]);
                }
            }
            if(nаmeArray.length == 2){
                for(let index in result) {
                    if(typeof (result[index][nаmeArray[0]]) != 'undefined' && typeof (result[index][nаmeArray[0]][nаmeArray[1]]) != 'undefined')
                        result[index][nаmeArray[0]][nаmeArray[1] + this.postfixFilesLink] = this.fileLinkByName(result[index][nаmeArray[0]][nаmeArray[1]]);;
                }
            }
        }

        return result;
    }
    //добавление раздельных координат (указанным полям)
    resultAddLatLon(_resultArray, _fieldNameArray) {
        //приведение к масиву
        //if( !Array.isArray(_resultArray) ) { _resultArray = [_resultArray]; }
        if( !Array.isArray(_fieldNameArray) ) { _fieldNameArray = [_fieldNameArray]; }

        let result       = _resultArray;

        //поиск первого вхождения значения координат
        let getFirstEntry    = function (_string) {
            let result       = "";

            let resultFind   = String(_string).match(/[0-9]+.[0-9]+,[0-9]+.[0-9]+/);
            if(resultFind != null)
                result       = resultFind[0];

            return result;
        }

        //перебор запрошенных полей
        for(let indexFieldName in _fieldNameArray) {
            let nаmeArray    = _fieldNameArray[indexFieldName].split('.');

            //замена значений
            if(nаmeArray.length == 1){
                for(let index in result) {
                    if(typeof (result[index][nаmeArray[0]]) != 'undefined'){
                        let value = result[index][nаmeArray[0]];
                        let lat   = "";
                        let lon   = "";
                        if( !global.empty(value) ){
                            value = getFirstEntry(value).split(",");
                            if( !global.empty(value[0]) && !global.empty(value[1])){
                                lat = value[0].trim();
                                lon = value[1].trim();
                            }
                        }
                        result[index][nаmeArray[0] + this.postfixLat] = lat;
                        result[index][nаmeArray[0] + this.postfixLon] = lon;
                    }
                }
            }
            if(nаmeArray.length == 2){
                for(let index in result) {
                    if(typeof (result[index][nаmeArray[0]]) != 'undefined' && typeof (result[index][nаmeArray[0]][nаmeArray[1]]) != 'undefined') {
                        let value = result[index][nаmeArray[0]][nаmeArray[1]];
                        let lat   = "";
                        let lon   = "";
                        if( !global.empty(value) ){
                            value = getFirstEntry(value).split(",");
                            if( !global.empty(value[0]) &&  !global.empty(value[1])){
                                lat = value[0].trim();
                                lon = value[1].trim();
                            }
                        }
                        result[index][nаmeArray[0]][nаmeArray[1] + this.postfixLat] = lat;
                        result[index][nаmeArray[0]][nаmeArray[1] + this.postfixLon] = lon;
                    }
                }
            }
        }

        return result;
    }

    //получение результата как обьекта (именование по полю)
    fromResultGetObject(_resultArray, _fieldName ) {
        //приведение к масиву
        if( !Array.isArray(_resultArray) ) _resultArray = [_resultArray];

        let result       = {};
        let nаmeArray    = _fieldName.split('.');

        if(nаmeArray.length == 1){
            for(let index in _resultArray) {
                result[_resultArray[index][nаmeArray[0]]] = _resultArray[index];
            }
        }
        if(nаmeArray.length == 2){
            for(let index in _resultArray) {
                result[_resultArray[index][nаmeArray[0]][nаmeArray[1]]] = _resultArray[index];
            }
        }

        return result;
    }
    //получение массива значений поля
    fromResultGetFieldValues(_resultArray, _fieldName ) {
        //приведение к масиву
        if( !Array.isArray(_resultArray) ) _resultArray = [_resultArray];

        let result       = [];
        let nаmeArray    = _fieldName.split('.');

        if(nаmeArray.length == 1){
            for(let index in _resultArray) {
                result.push(_resultArray[index][nаmeArray[0]]);
            }
        }
        if(nаmeArray.length == 2){
            for(let index in _resultArray) {
                result.push(_resultArray[index][nаmeArray[0]][nаmeArray[1]]);
            }
        }

        return result;
    }
    //получение записей с полями сущностей сведенными в один обьект
    fromResultGetFlat(_resultArray) {
        let isObject = false;
        //приведение к масиву
        if( !Array.isArray(_resultArray) ) { _resultArray = [_resultArray]; isObject = true }

        let result       = [];

        //перебор записей
        for(let index in _resultArray) {
            let finalItem = {};

            for(let indexEntity in _resultArray[index]){
                if(typeof ( _resultArray[index][indexEntity] ) != "object"){
                    finalItem[indexEntity] = _resultArray[index][indexEntity];
                }
                if(typeof ( _resultArray[index][indexEntity] ) == "object"){
                    //проверка на вложенный массив
                    if( Array.isArray(_resultArray[index][indexEntity]) ){
                        finalItem[indexEntity] = this.fromResultGetFlat(_resultArray[index][indexEntity]);
                    }else{
                        for(let indexField in _resultArray[index][indexEntity]){
                            //проверка на вложенный массив
                            if( Array.isArray(_resultArray[index][indexEntity][indexField]) ){
                                finalItem[indexEntity + "." + indexField] = this.fromResultGetFlat(_resultArray[index][indexEntity][indexField]);
                            }else{
                                finalItem[indexEntity + "." + indexField] = _resultArray[index][indexEntity][indexField];
                            }
                        }
                    }
                }
            }

            result.push(finalItem);
        }

        if(isObject) result = result[0];

        return result;
    }
    //получение результата со свойством содержащим массив связанных записей (поиск связи по полю)
    fromResultGetExtendedResult (_resultArrayBase, _resultArrayAdded, _fieldNameBase, _fieldNameAdded, _fieldNameNew, _useEntry = false){
        let isObject = false;
        //приведение к масиву
        if( !Array.isArray(_resultArrayBase) ) {_resultArrayBase = [_resultArrayBase]; isObject = true}
        if( !Array.isArray(_resultArrayAdded) ) {_resultArrayAdded = [_resultArrayAdded];}

        let result          = [];

        //проверка вхождения значения
        let checkEntry      = function (_findValue, _string) {
            let result      = false;

            let regExp      = new RegExp( "(^|[^0-9])"+_findValue+"([^0-9]|$)" );
            let resultFind  = String(_string).search(regExp);

            if(resultFind != -1)
                result      = true;

            return result;
        }

        let nаmeNewArray    = _fieldNameNew.split('.');
        let nаmeBaseArray   = _fieldNameBase.split('.');
        let nаmeAddedArray  = _fieldNameAdded.split('.');

        //перебор записей
        for(let index in _resultArrayBase) {

            result[index]   = _resultArrayBase[index];
            let findValue   = null;
            let addArray    = [];

            if (nаmeBaseArray.length == 1) {
                findValue = result[index][nаmeBaseArray[0]];
            }
            if (nаmeBaseArray.length == 2) {
                findValue = result[index][nаmeBaseArray[0]][nаmeBaseArray[1]];
            }

            if (nаmeNewArray.length == 1) {
                result[index][nаmeNewArray[0]] = addArray;
            }
            if (nаmeNewArray.length == 2) {
                if( typeof(result[index][nаmeNewArray[0]]) == "undefined") result[index][nаmeNewArray[0]] = {};
                result[index][nаmeNewArray[0]][nаmeNewArray[1]] = addArray;
            }

            for(let indexAdded in _resultArrayAdded) {
                if (nаmeAddedArray.length == 1) {
                    if( !_useEntry && _resultArrayAdded[indexAdded][nаmeAddedArray[0]] == findValue )
                        addArray.push(_resultArrayAdded[indexAdded]);

                    if( _useEntry && (checkEntry(findValue, _resultArrayAdded[indexAdded][nаmeAddedArray[0]]) || checkEntry(_resultArrayAdded[indexAdded][nаmeAddedArray[0]]), findValue) )
                        addArray.push(_resultArrayAdded[indexAdded]);
                }
                if (nаmeAddedArray.length == 2) {
                    if( !_useEntry &&_resultArrayAdded[indexAdded][nаmeAddedArray[0]][nаmeAddedArray[1]] == findValue )
                        addArray.push(_resultArrayAdded[indexAdded]);

                    if( _useEntry && (checkEntry(findValue, _resultArrayAdded[indexAdded][nаmeAddedArray[0]][nаmeAddedArray[1]]) || checkEntry(_resultArrayAdded[indexAdded][nаmeAddedArray[0]][nаmeAddedArray[1]], findValue)) )
                        addArray.push(_resultArrayAdded[indexAdded]);
                }
            }

        }

        if(isObject) result = result[0];

        return result;
    }

}


