module.exports = {
    //карта шаблонов
    nameMap : {
        14 : 'full',
        15 : 'navigWithout',
        16 : 'full',
        17 : 'navigOnlyChildrens',
        18 : 'navigOnlyBrothers',
        19 : 'navigWithout',
        20 : 'navigWithout',
    },

    defaultTemplate : 'full',
    templatesFolder : 'contentTemplates',
}
