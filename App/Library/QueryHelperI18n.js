module.exports = class extends Application.System.Model{

    // ПАРАМЕТРЫ
    useAlias            = true;
    useFieldId          = "id";
    useFieldActive      = "active";
    useFieldLang        = "lang";
    useFieldOrder       = "order";
    useFieldKey         = "key";
    useFieldText        = "text";

    //конструктор
    constructor(_nameQH) {
        super();
        this.nameQH         = _nameQH;
    }

    //инициализация
    async initQueryHelperI18n(_keysTable, _textTable){
        try {
            this.keysTable  = _keysTable;
            this.textTable  = _textTable;

            let QH          = new Application.Library.QueryHelper(this.nameQH);
            let langs       = QH.langs();

            //комплекты языковых пакетов
            let oldI18n     = Application.i18n;
            let newI18n     = {};
            let finI18n     = {};

            //создание QH комплекта языковых пакетов
            for(let index in langs){
                let pack = await this._createLangPack(langs[index]);
                if( !global.empty(pack) )
                    newI18n[langs[index]] = pack;
            }

            //создание финального комплекта языковых пакетов
            for(let indexLang in oldI18n){
                finI18n[indexLang] = {}
                for(let indexString in oldI18n[indexLang]){
                    finI18n[indexLang][indexString] =  oldI18n[indexLang][indexString]
                }
            }
            for(let indexLang in newI18n){
                if( typeof(finI18n[indexLang]) == "undefined" ) finI18n[indexLang] = {}
                for(let indexString in newI18n[indexLang]){
                    finI18n[indexLang][indexString] =  newI18n[indexLang][indexString]
                }
            }

            //замена действующего комплекта языковых пакетов
            Application.i18n = finI18n;
        }catch (e) {}
    }

    //инициализация
    async importToQHFromI18n(_keysTable, _textTable){
        let result = [];

        try {
            this.keysTable  = _keysTable;
            this.textTable  = _textTable;

            let QH          = new Application.Library.QueryHelper(this.nameQH);
            let langs       = QH.langs();

            //комплекты языковых пакетов
            let i18n        = Application.i18n;

            for(let lang in i18n){
                if( langs.indexOf(lang) != -1 ){
                    result[lang] = await this._importLangPack(lang, i18n[lang])
                }
            }
        }catch (e) {}

        return result;
    }

    // SET функции
    setUseAlias(_useAlias) {
        this.useAlias           = _useAlias;
        return this;
    }
    setUseFieldId(_useFieldId) {
        this.useFieldId         = _useFieldId;
        return this;
    }
    setUseFieldActive(_useFieldActive) {
        this.useFieldActive     = _useFieldActive;
        return this;
    }
    setUseFieldLang(_useFieldLang) {
        this.useFieldLang       = _useFieldLang;
        return this;
    }
    setUseFieldId(_useFieldOrder) {
        this.useFieldOrder      = _useFieldOrder;
        return this;
    }
    setUseFieldKey(_useFieldKey) {
        this.useFieldKey        = _useFieldKey;
        return this;
    }
    setUseFieldText(_useFieldText) {
        this.useFieldText       = _useFieldText;
        return this;
    }

    //создание языкового пакета БД
    async _createLangPack(_lang){
        let result = null;

        try {
            let QH = new Application.Library.QueryHelper(this.nameQH);
            QH.setUseAlias(this.useAlias).setUseJoin(false, false, true);

            //язык
            let resultQH = QH.requestSelect( this.textTable )
                .where(QH.requestField(this.keysTable + '.' + this.useFieldActive ), '=' , QH.sv(true))
                .where(QH.requestField(this.textTable + '.' + this.useFieldActive), '=' , QH.sv(true))
                .where(QH.requestField(this.textTable + '.' + this.useFieldLang), '=' , QH.sv(_lang))
                .orderBy(QH.requestField(this.keysTable + '.' + this.useFieldOrder, QH.order()))
            resultQH = await resultQH;

            //создание пакета
            let pack = {};

            //локализации
            for(let index in resultQH){
                if( !global.empty(resultQH[index][this.keysTable][this.useFieldKey]) )
                pack[ resultQH[index][this.keysTable][this.useFieldKey] ] = resultQH[index][this.textTable][this.useFieldText];
            }

            if( Object.keys(pack).length > 0)
                result = pack;
        }catch (e) {}

        return result;
    }
    //импорт языкового пакета БД
    async _importLangPack(_lang, _pack){
        let result = [];

        try {
            let QH = new Application.Library.QueryHelper(this.nameQH);
            QH.setUseAlias(this.useAlias).setUseJoin(false, false, true);

            for(let key in _pack){
                let itemResult = {};

                //проверка наличия записи ключа
                let resultQHkey = QH.requestSelect( this.keysTable )
                    .where(QH.requestField(this.keysTable + '.' + this.useFieldKey), '=' , key)
                    .limit(1)
                resultQHkey = await resultQHkey;
                let keyId = null;

                if( !global.empty(resultQHkey[0]) ){
                    // наличия записи ключа подтверждено
                    keyId = resultQHkey[0][this.keysTable][this.useFieldId];
                }else{
                    // наличия записи ключа не подтверждено ( требуется создание )
                    let values = {}
                        values[this.useFieldKey]    = key;
                        values[this.useFieldActive] = QH.sv(true);
                        values[this.useFieldOrder]  = 50;

                    let resultCreateItemKey = await this._createItemKey(values);
                    if( !global.empty(resultCreateItemKey[0]) ){
                        keyId = resultCreateItemKey[0];
                        itemResult.key              = _pack[key];
                        itemResult.createKey        = resultCreateItemKey[0];
                    }else{
                        itemResult.key              = _pack[key];
                        itemResult.createKey        = false;
                    }
                }

                //проверка наличия записи локализации
                let resultQHtext = QH.requestSelect( this.textTable )
                    .where(QH.requestField(this.keysTable + '.' + this.useFieldKey), '=' , key)
                    .where(QH.requestField(this.textTable + '.' + this.useFieldLang), '=' , QH.sv(_lang))
                    .limit(1)
                resultQHtext = await resultQHtext;

                if( !global.empty(resultQHtext[0]) ){
                    // наличия записи локализации подтверждено
                    // изменения не требуются
                }else{
                    // наличия записи локализации не подтверждено ( требуется создание )
                    if( keyId ){
                        let parent_item_id = QH.UseHelper.fileQHC.systemFields['parent_item_id'];

                        let values = {}
                        values[parent_item_id]          = keyId;
                        values[this.useFieldText]       = _pack[key];
                        values[this.useFieldLang]       = QH.sv(_lang);
                        values[this.useFieldActive]     = QH.sv(true);
                        values[this.useFieldOrder]      = 50;

                        let resultCreateItemText = await this._createItemText(values);
                        if( !global.empty(resultCreateItemText[0]) ){
                            itemResult.key              = _pack[key];
                            itemResult.createText       = resultCreateItemText[0];
                        }else{
                            itemResult.key              = _pack[key];
                            itemResult.createText       = false;
                        }
                    }else{
                        itemResult.key              = _pack[key];
                        itemResult.createText       = false;
                    }
                }

                if( !global.empty(itemResult.key) ){
                    result.push(itemResult);
                }
            }

        }catch (e) {}

        return result;
    }


    async _createItemKey(_values) {
        let result = [];
        try {
            let QH = new Application.Library.QueryHelper(this.nameQH);
            QH.setUseAlias(this.useAlias).setUseJoin(false, false, false);

            //создание записи
            let requestInsert =  QH.requestInsert(this.keysTable, _values);
            requestInsert = await requestInsert;

            result = requestInsert;
        }catch (e) {}
        return result;
    }
    async _createItemText(_values) {
        let result = [];
        try {
            let QH = new Application.Library.QueryHelper(this.nameQH);
            QH.setUseAlias(this.useAlias).setUseJoin(false, false, false);

            //создание записи
            let requestInsert =  QH.requestInsert(this.textTable, _values);
            requestInsert = await requestInsert;

            result = requestInsert;
        }catch (e) {}
        return result;
    }

}


