/*
Routes configuration.
type: Object
keys of Object is domain names in regex.
values of Object keys are:
if type of value is Object, then it's structure is
 {
     name: <string> human readable name of route
     method: optional <string> (get|post|put|etc..) type of http method. If set, strict request to this method only
     uri: required <string> uri part of request, in ExpressJS-like format
     controller: <string> the name of Controller. default is "Default". if variable ":controller" is set in uri, it overrides this setting
     action: <string> the name of action in controller. default is "index". If variable ":action" is set in uri, it overrides this setting
 }
 and it works as "set up route function with this parameters"

 if type of value is String, it works as "set up route with any method, using /\/(.*)/ as uri"
 Inside this route, the value is used as root path to controller (RootC),
 and uri treat /part/subpart1/subpart../subpartN as Application.Controller.{RootC}.part.subpart1.{...}.subpartN(), where subpartN of uri is action of controller;
 if value is empty, the Application.Controller is RootC.
 if only /part in uri, it resolves as name of controller with action "index"
 */
routes = {
    //ПОДДОМЕНЫ
    'subdomain': [
        //сайтмап
        {
            name: 'Sitemap',
            method: 'get',
            uri: '/sitemap.xml',
            controller: 'Subdomain.Sitemap'
        },    
        //нок
        {
            name: 'Nok',
            method: 'get',
            uri: '/nok.txt',
            controller: 'Subdomain.Nok'
        },
        //главная
        {
            name: 'Welcome',
            method: 'get',
            uri: '/:lang?',
            controller: 'Subdomain.Welcome'
        },
        //поиск
        {
            name: 'Search',
            method: 'get',
            uri: '/:lang?/search/:arg?',
            controller: 'Subdomain.Search'
        },
        //руководство
        {
            name: 'Administrations',
            method: 'get',
            uri: '/:lang?/rukovodstvo',
            controller: 'Subdomain.Administrations'
        },
        //организаця
        {
            name: 'Organization',
            method: 'get',
            uri: '/:lang?/contact',
            controller: 'Subdomain.Organizations',
            action: 'browse'
        },
        //здания
        {
            name: 'Buildings',
            method: 'get',
            uri: '/:lang?/buildings',
            controller: 'Subdomain.Buildings',
        },
        {
            name: 'Building',
            method: 'get',
            uri: '/:lang?/building-:alias?',
            controller: 'Subdomain.Buildings',
            action: 'browse'
        },
        //подразделения
        {
            name: 'Subdivisions',
            method: 'get',
            uri: '/:lang?/subdivisions',
            controller: 'Subdomain.Subdivisions',
        },
        {
            name: 'Subdivision',
            method: 'get',
            uri: '/:lang?/subdivision-:alias?',
            controller: 'Subdomain.Subdivisions',
            action: 'browse'
        },
        //вакансии
        {
            name: 'Vacancys',
            method: 'get',
            uri: '/:lang?/vacancy',
            controller: 'Subdomain.Vacancys'
        },
        //работники
        {
            name: 'Workers',
            method: 'get',
            uri: '/:lang?/workers',
            controller: 'Subdomain.Workers'
        },
        //новости
        {
            name: 'News',
            method: 'get',
            uri: '/:lang?/news',
            controller: 'Subdomain.News'
        },
        {
            name: 'New',
            method: 'get',
            uri: '/:lang?/news-:alias?',
            controller: 'Subdomain.News',
            action: 'browse'
        },
        //события
        {
            name: 'Events',
            method: 'get',
            uri: '/:lang?/events',
            controller: 'Subdomain.Events'
        },
        {
            name: 'Event',
            method: 'get',
            uri: '/:lang?/events-:alias?',
            controller: 'Subdomain.Events',
            action: 'browse'
        },
        //статьи
        {
            name: 'Articles',
            method: 'get',
            uri: '/:lang?/articles',
            controller: 'Subdomain.Articles'
        },
        {
            name: 'ArticleDocument',
            method: 'get',
            uri: '/:lang?/document-:id?',
            controller: 'Subdomain.Ajax',
            action: 'fileForDocument'
        },
        {
            name: 'Article',
            method: 'get',
            uri: '/:lang?/:alias?',
            controller: 'Subdomain.Articles',
            action: 'browse'
        },
    ],
    //ДОМЕН
    'domain': [
        //файлы
        {
            name: 'File',
            method: 'get',
            uri: '/file-:filename?',
            controller: 'Ajax',
            action: 'file'
        },
        //сайтмап
        {
            name: 'Sitemap',
            method: 'get',
            uri: '/sitemap.xml',
            controller: 'Sitemap'
        },
        //главная
        {
            name: 'Welcome',
            method: 'get',
            uri: '/:lang?',
            controller: 'Welcome'
        },
        //поиск
        {
            name: 'Search',
            method: 'get',
            uri: '/:lang?/search/:arg?',
            controller: 'Search'
        },
        //руководство
        {
            name: 'Administrations',
            method: 'get',
            uri: '/:lang?/rukovodstvo',
            controller: 'Administrations'
        },
        //организации
        {
            name: 'OrganizationsTable',
            method: 'get',
            uri: '/:lang?/table-of-organisations',
            controller: 'Organizations'
        },
        {
            name: 'OrganizationsMap',
            method: 'get',
            uri: '/:lang?/map-of-organisations',
            controller: 'Organizations',
            action: 'map'
        },
        {
            name: 'OrganizationsMapOptions',
            method: 'get',
            uri: '/:lang?/ajax/organizationsForMap/:arg?',
            controller: 'Ajax',
            action: 'organizationsForMap'
        },
        //вакансии
        {
            name: 'Vacancys',
            method: 'get',
            uri: '/:lang?/vacancy',
            controller: 'Vacancys'
        },
        //новости
        {
            name: 'News',
            method: 'get',
            uri: '/:lang?/news',
            controller: 'News'
        },
        {
            name: 'New',
            method: 'get',
            uri: '/:lang?/news-:alias?',
            controller: 'News',
            action: 'browse'
        },
        //события
        {
            name: 'Events',
            method: 'get',
            uri: '/:lang?/events',
            controller: 'Events'
        },
        {
            name: 'Event',
            method: 'get',
            uri: '/:lang?/events-:alias?',
            controller: 'Events',
            action: 'browse'
        },
        //статьи
        {
            name: 'Articles',
            method: 'get',
            uri: '/:lang?/articles',
            controller: 'Articles'
        },
        {
            name: 'ArticleDocument',
            method: 'get',
            uri: '/:lang?/document-:id?',
            controller: 'Ajax',
            action: 'fileForDocument'
        },
        {
            name: 'Article',
            method: 'get',
            uri: '/:lang?/:alias?',
            controller: 'Articles',
            action: 'browse'
        },
    ],
    'localhost':""
};

module.exports = {
    '.*.iogv21.ru'  : routes['subdomain'],
    'iogv21.ru'     : routes['domain'],
};