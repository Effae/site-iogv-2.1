module.exports = class extends Controller_Template {

    async action_index(){
        let QH = new Application.Library.QueryHelper();

        let organization        = this.organization;
        let useLang             = this.useLang;
        let useHost             = this.useHost;

        //организации
        let ModelOrganizations  = new Application.Model.Organizations();
        let resultOrganizations = await ModelOrganizations.getAll([["organizations.parent","=",organization.id]]);

        let content = new this.ViewJS("organizations.organizations");
            content.organization    = organization;
            content.useLang         = useLang;
            content.useHost         = useHost;
            content.contentData     = resultOrganizations;
        this.result.content = await content.render();
    }

    async action_map(){
        let QH = new Application.Library.QueryHelper();

        let organization        = this.organization;
        let useLang             = this.useLang;
        let useHost             = this.useHost;

        //типы организаций
        let ModelLists          = new Application.Model.Lists();
        let resultLists         = await ModelLists.getOrgType();

        //организации
        let ModelOrganizations  = new Application.Model.Organizations();
        let resultOrganizations = await ModelOrganizations.getAll([["organizations.parent","=",organization.id]]);

        //отбор только используемых типов
        let resultListsChecked  = [];
        for(let index in resultLists) {
            for(let indexOrg in resultOrganizations) {
                if (resultOrganizations[indexOrg].organizations.type == resultLists[index].listsChoices.id){
                    resultListsChecked.push(resultLists[index]);
                    break;
                }
            }
        }

        let content = new this.ViewJS("organizations.organizationsMap");
            content.organization    = organization;
            content.useLang         = useLang;
            content.useHost         = useHost;
            content.orgTypes        = resultListsChecked;
            content.contentData     = resultOrganizations;
        this.result.content = await content.render();
    }

    async client_onload(){
    }
}
