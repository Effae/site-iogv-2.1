module.exports = class extends Controller_Template {

    async action_index(){
        let QH = new Application.Library.QueryHelper();

        let organization    = this.organization;
        let useLang         = this.useLang;
        let useHost         = this.useHost;

        //актуальные ссыки
        let ModelMenu       = new Application.Model.Menu();
        let resultMenu      = await ModelMenu.getMenu(organization.type, useLang,9);

        //последние события
        let ModelEvents     = new Application.Model.Events();
        let resultEvents    = await ModelEvents.getAll(organization.id, QH.dl(), null, 5);

        //последние новости
        let ModelNews       = new Application.Model.News();
        let resultNews      = await ModelNews.getAll(organization.id, useLang, null, 5);

        //баннеры
        let ModelBanners    = new Application.Model.Banners();
        let resultBanners   = await ModelBanners.getAll(1); //всегда показывать баннеры уровня министрества, баннеры оранизаций отключены

        let content = new this.ViewJS("welcomes.welcomeDomain");
            content.organization    = organization;
            content.useLang         = useLang;
            content.useHost         = useHost;
            content.currentLinks    = resultMenu;
            content.events          = resultEvents;
            content.news            = resultNews;
            content.banners         = resultBanners;
        this.result.content = await content.render();
    }

    async client_onload(){
    }
}
