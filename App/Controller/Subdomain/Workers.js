module.exports = class extends Controller_Subdomain_Template {

    async action_index(){
        let QH = new Application.Library.QueryHelper();

        let organization    = this.organization;
        let useLang         = this.useLang;
        let useHost         = this.useHost;
        let useSubhost      = this.useSubhost;

        //работники
        let ModelWorkers       = new Application.Model.Workers();
        let resultWorkers      = await ModelWorkers.getAllExtended(organization.id);

        let content = new this.ViewJS("workers.workers");
            content.organization    = organization;
            content.useLang         = useLang;
            content.useHost         = useHost;
            content.useSubhost      = useSubhost;
            content.count           = resultWorkers.length;
            content.contentData     = resultWorkers;
        this.result.content = await content.render();
    }

    async client_onload(){
    }
}
