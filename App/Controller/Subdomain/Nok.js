module.exports = class extends Application.System.Controller {
    async _before() {
    }

    async _after() {
        return this.result;
    }

    async action_index(){
        let QH                  = new Application.Library.QueryHelper();
        let lang                = QH.dl();

        let host                = this.req.headers.host;
        let useHost             = this.req.headers.host.substring(this.req.headers.host.indexOf('.')+1).split(":")[0];
        let useSubhost          = this.req.headers.host.substring(0, this.req.headers.host.indexOf('.'));

        //проверка организации
        let ModelOrganizations  = new Application.Model.Organizations();
        let resultOrganizations = await ModelOrganizations.getByAlias(useSubhost);
        if (global.empty(resultOrganizations[0])) {
            throw new Error(404);
        }

        let orgId               = resultOrganizations[0].organizations.id;

        //маршруты
        let routes              = {};
        for(let index in Application.routes[".*."+useHost]) {
            routes[Application.routes[".*."+useHost][index].name] = Application.routes[".*."+useHost][index];
        }

        //ссылки контроллеров
        let welcome             = host + routes["Welcome"].uri.replace(":lang?", lang)
        let workers             = host + routes["Workers"].uri.replace(":lang?", lang)
        let vacancys            = host + routes["Vacancys"].uri.replace(":lang?", lang)
        let administrations     = host + routes["Administrations"].uri.replace(":lang?", lang)
        let subdivisions        = host + routes["Subdivisions"].uri.replace(":lang?", lang)

        //подразделения
        let subdivisionsArray   = [];
        let ModelSubdivisions   = new Application.Model.Subdivisions();
        let resultSubdivisions  = await ModelSubdivisions.getAll(orgId);
        for(let index in resultSubdivisions) {
            let link = routes["Subdivision"].uri.replace(":lang?", lang).replace(":alias?", resultSubdivisions[index].subdivisions.alias)
            subdivisionsArray.push( {link: host + link} );
        }

        //статьи
        let articlesArray       = [];
        let ModelArticles       = new Application.Model.Articles();
        let resultArticles      = await ModelArticles.getAll(orgId, QH.dl(),[["articles.nok", "!=", ""]]);
        for(let index in resultArticles) {
            let noks = resultArticles[index].articles.nok.replace(/[^0-9-]/g, '').split("-");
            for(let indexNoks in noks) {
                if( !global.empty(noks[indexNoks]) ){
                    let link = routes["Article"].uri.replace(":lang?", lang).replace(":alias?", resultArticles[index].articles.alias)
                    articlesArray.push( {nok: noks[indexNoks], link: host + link} );
                }
            }
        }

        let content = new this.ViewJS("noks.nok");
            content.workers             = workers;
            content.vacancys            = vacancys;
            content.administrations     = administrations;
            content.welcome             = welcome;
            content.subdivisions        = subdivisions;
            content.subdivisionsArray   = subdivisionsArray;
            content.articlesArray       = articlesArray;
        this.result = await content.render();
    }

    async client_onload(){
    }
}
