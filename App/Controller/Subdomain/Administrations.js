module.exports = class extends Controller_Subdomain_Template {

    async action_index(){
        let QH = new Application.Library.QueryHelper();

        let organization    = this.organization;
        let useLang         = this.useLang;
        let useHost         = this.useHost;
        let useSubhost      = this.useSubhost;

        //администрация
        let ModelAdministrations    = new Application.Model.Administrations();
        let resulAdministrations    = await ModelAdministrations.getAll(organization.id);

        //подраделения с основным зданием
        let ModelBuildings          = new Application.Model.Buildings();
        let resulBuildings          = await ModelBuildings.getAllExtended(organization.id);

        //подраделения без основного здания
        let ModelSubdivisions       = new Application.Model.Subdivisions();
        let resulSubdivisions       = await ModelSubdivisions.getAll(organization.id, [["subdivisions.buildingMain","=",""]]);

        let content = new this.ViewJS("administrations.administrations");
            content.organization            = organization;
            content.useLang                 = useLang;
            content.useHost                 = useHost;
            content.useSubhost              = useSubhost;
            content.contentDataAdmin        = resulAdministrations;
            content.contentDataBuildings    = resulBuildings;
            content.contentDataSubdivisions = resulSubdivisions;
        this.result.content = await content.render();
    }

    async client_onload(){
    }
}
