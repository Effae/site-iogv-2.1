module.exports = class extends Controller_Subdomain_Template {
    async action_index() {
        let QH = new Application.Library.QueryHelper();

        let organization    = this.organization;
        let useLang         = this.useLang;
        let useHost         = this.useHost;
        let useSubhost      = this.useSubhost;

        //актуальные ссыки
        let ModelMenu           = new Application.Model.Menu();
        let resultMenu          = await ModelMenu.getMenu(organization.type, useLang, 13);

        //последние новости
        let ModelNews           = new Application.Model.News();
        let resultNews          = await ModelNews.getAll(organization.id, useLang, null, 5);

        //баннеры
        let ModelBanners        = new Application.Model.Banners();
        let resultBanners       = await ModelBanners.getAll(1); //всегда показывать баннеры уровня министрества, баннеры оранизаций отключены

        //подразделения
        let ModelBuildings      = new Application.Model.Buildings();
        let resultBuildings     = await ModelBuildings.getAll(organization.id);

        let content = new this.ViewJS('welcomes.welcomeSubdomain');
            content.organization    = organization;
            content.useLang         = useLang;
            content.useHost         = useHost;
            content.useSubhost      = useSubhost;
            content.currentLinks    = resultMenu;
            content.buildings       = resultBuildings;
            content.news            = resultNews;
            content.banners         = resultBanners;
        this.result.content = await content.render();
    }

    async client_onload() {}
};
