module.exports = class extends Application.System.Controller {
    async _before() {
    }

    async _after() {
        return this.result;
    }

    async action_file() {
        let QH = new Application.Library.QueryHelper();

        let fileName            = this.req.params['filename'];

        try {
            let filePath        = QH.fileLinkByName(fileName);

            let filePathFull    = Application.lib.path.join(Application.config.Directories.AppPublic, filePath);
            let file            = await Application.lib.fs_promises.readFile(filePathFull);

            this.res.write(file);
            this.res.end();
        }catch (e) {
            this.result = false;
        }
    }

    async action_fileForDocument() {
        let QH = new Application.Library.QueryHelper();

        let useHost                 = this.req.headers.host.substring(this.req.headers.host.indexOf('.')+1);
        let useSubhost              = this.req.headers.host.substring(0, this.req.headers.host.indexOf('.'));
        let useLang                 = this.req.params['lang'];

        //данные i18n
        this.i18n.lang          = useLang;

        let docId               = this.req.params['id'];

        //документы
        let ModelDocuments  = new Application.Model.Documents();
        let resultDocuments = await ModelDocuments.getAny([["documents.id", "=", docId],["organizations.alias", "=", useSubhost]], 1);

        if( !global.empty(resultDocuments) && !global.empty(resultDocuments[0]) ){
            //данныее QH
            let useFilePath         = QH.fileLinkByName(resultDocuments[0].documents.filename);
            let useFileExtension    = resultDocuments[0].documents.filename.slice(resultDocuments[0].documents.filename.lastIndexOf('.') + 1);
            let useFileName         = ( !global.empty(useFileExtension) ) ? resultDocuments[0].documents.name + "." + useFileExtension : resultDocuments[0].documents.name;

            //файл
            let filePathFull        = Application.lib.path.join(Application.config.Directories.AppPublic, useFilePath);
            let file                = await Application.lib.fs_promises.readFile(filePathFull);

            //данные заголовка ответа
            let filename            = encodeURIComponent(useFileName);
            let type                = ( !global.empty( Application.System.MimeTypes[useFileExtension]) ) ? Application.System.MimeTypes[useFileExtension] : Application.System.MimeTypes["txt"]
            let length2              = file.length;
            let method              = "attachment";
            if( type == "application/pdf" || type.indexOf('image/') != -1 )
                method              = "inline";

            this.res.writeHead(200, {
                "Content-Disposition": method + ";filename=" + filename,
                'Content-Type': type,
                'Content-Length': length2,
            });
            this.res.write(file);
            this.res.end();
        }else{
            this.result = false;
        }

    }

}