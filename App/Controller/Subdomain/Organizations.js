module.exports = class extends Controller_Subdomain_Template {
    
    async action_browse(){        
        let QH = new Application.Library.QueryHelper();

        let organization    = this.organization;
        let useLang         = this.useLang;
        let useHost         = this.useHost;
        let useSubhost      = this.useSubhost;

        //здания
        let ModelBuildings              = new Application.Model.Buildings();
        let resultBuildings             = await ModelBuildings.getAll(organization.id);
        
        //подразделения
        let ModelSubdivisions           = new Application.Model.Subdivisions();
        let resultSubdivisions          = await ModelSubdivisions.getAll(organization.id);

        let content = new this.ViewJS("organizations.organization");
            content.organization            = organization;
            content.useLang                 = useLang;
            content.useHost                 = useHost;
            content.useSubhost              = useSubhost;
            content.contentDataBuildings    = resultBuildings;
            content.contentDataSubdivisions = resultSubdivisions;
        this.result.content = await content.render();
    }

    async client_onload(){
    }
}
