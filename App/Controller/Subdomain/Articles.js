module.exports = class extends Controller_Subdomain_Template {

    async action_index(){
        let QH = new Application.Library.QueryHelper();


        let organization    = this.organization;
        let useLang         = this.useLang;
        let useHost         = this.useHost;
        let useSubhost      = this.useSubhost;

        //статьи
        let ModelArticles   = new Application.Model.Articles();
        let resultArticles  = await ModelArticles.getAll(organization.id, useLang);

        let content = new this.ViewJS("articles.articles");
            content.organization    = organization;
            content.useLang         = useLang;
            content.useHost         = useHost;
            content.useSubhost      = useSubhost;
            content.contentData     = resultArticles;
        this.result.content = await content.render();
    }

    async action_browse(){
        try {

            let QH = new Application.Library.QueryHelper();
            let TH = new Application.Library.TemplateHelper();

            let organization    = this.organization;
            let useLang         = this.useLang;
            let useHost         = this.useHost;
            let useSubhost      = this.useSubhost;
            let useIdAlias      = this.useIdAlias;

            let alias           = this.req.params['alias'];

            //условите запроса
            let fieldName       = "articles.alias"
            if( useIdAlias && new RegExp('^[0-9]*$').test(alias))
                fieldName       = "articles.id"

            //статья
            let ModelArticles   = new Application.Model.Articles();
            let resultArticles  = await ModelArticles.getAll(organization.id, useLang, [[fieldName,"=", alias]], 1);

            //проверка 404
            if( global.empty(resultArticles[0]) ){
                throw new Error(404);
            }

            //документы
            let ModelDocuments  = new Application.Model.Documents();
            let resultDocuments = await ModelDocuments.getAll(organization.id, resultArticles[0].articles.id);

            //навигация
            let navigParetn     = await ModelArticles.getById(organization.id, useLang, resultArticles[0].articles.parent);
            let navigBrothers   = await ModelArticles.getAll(organization.id, useLang,[["articles.parent", "=", resultArticles[0].articles.parent]]);
            let navigChildrens  = await ModelArticles.getAll(organization.id, useLang,[["articles.parent", "=", resultArticles[0].articles.id]]);
            let navigData = {
                parent:         navigParetn[0],
                brothers:       navigBrothers,
                childrens:      navigChildrens
            }

            let content         = new this.ViewJS(TH.getTemplateNameById(resultArticles[0].articlesLocal.template));
                content.organization    = organization;
                content.useLang         = useLang;
                content.useHost         = useHost;
                content.useSubhost      = useSubhost;
                content.contentData     = resultArticles[0].articlesLocal;
                content.documentData    = resultDocuments;
                content.navigationData  = navigData;
            this.result.content = await content.render();

            this.result.description     = resultArticles[0].articlesLocal.meta;
            this.result.keywords        = resultArticles[0].articlesLocal.meta;

            //канонический адрес
            if( useIdAlias )
                this.result.canonical   = await this.getUrl( {'alias':resultArticles[0].articles.id} );

        }catch (e) {
            await this.error404();
        }
    }

    async client_onload(){
    }
}
