module.exports = class extends Controller_Subdomain_Template {

    async action_index(){
        let QH = new Application.Library.QueryHelper();

        let organization    = this.organization;
        let useLang         = this.useLang;
        let useHost         = this.useHost;
        let useSubhost      = this.useSubhost;

        //вакансии
        let ModelVacancys       = new Application.Model.Vacancys();
        let resultVacancys      = await ModelVacancys.getAll(organization.id);

        let content = new this.ViewJS("vacancys.vacancysSubdomain");
            content.organization    = organization;
            content.useLang         = useLang;
            content.useHost         = useHost;
            content.useSubhost      = useSubhost;
            content.count           = resultVacancys.length;
            content.contentData     = resultVacancys;
        this.result.content = await content.render();
    }

    async client_onload(){
    }
}
