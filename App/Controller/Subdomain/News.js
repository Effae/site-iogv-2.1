module.exports = class extends Controller_Subdomain_Template {

    async action_index(){
        let QH = new Application.Library.QueryHelper();

        let organization    = this.organization;
        let useLang         = this.useLang;
        let useHost         = this.useHost;
        let useSubhost      = this.useSubhost;

        //новости
        let ModelNews       = new Application.Model.News();
        let resultNews      = await ModelNews.getAll(organization.id, useLang);

        let content = new this.ViewJS("news.news");
            content.organization    = organization;
            content.useLang         = useLang;
            content.useHost         = useHost;
            content.useSubhost      = useSubhost;
            content.contentData     = resultNews;
        this.result.content = await content.render();
    }

    async action_browse(){
        try {

            let QH = new Application.Library.QueryHelper();
            let TH = new Application.Library.TemplateHelper();

            let organization    = this.organization;
            let useLang         = this.useLang;
            let useHost         = this.useHost;
            let useSubhost      = this.useSubhost;
            let useIdAlias      = this.useIdAlias;

            let alias           = this.req.params['alias'];

            //условите запроса
            let fieldName       = "news.alias"
            if( useIdAlias && new RegExp('^[0-9]*$').test(alias))
                fieldName       = "news.id"

            //новость
            let ModelNews       = new Application.Model.News();
            let resultNews      = await ModelNews.getAll(organization.id, useLang, [[fieldName,"=", alias]], 1);

            //проверка 404
            if( global.empty(resultNews[0]) ){
                throw new Error(404);
            }

            let content         = new this.ViewJS(TH.getTemplateNameById());
                content.organization    = organization;
                content.useLang         = useLang;
                content.useHost         = useHost;
                content.useSubhost      = useSubhost;
                content.contentData     = resultNews[0].newsLocal;
                content.navigationData  = undefined;
                content.documentData    = undefined;
            this.result.content = await content.render();

            this.result.description     = resultNews[0].newsLocal.meta;
            this.result.keywords        = resultNews[0].newsLocal.meta;

            //канонический адрес
            if( useIdAlias )
                this.result.canonical   = await this.getUrl( {'alias':resultNews[0].news.id})

        }catch (e) {
            await this.error404();
        }
    }

    async client_onload(){
    }
}
