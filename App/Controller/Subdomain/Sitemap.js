module.exports = class extends Application.System.Controller {
    async _before() {
    }

    async _after() {
        return this.result.content;
    }

    async action_index(){
        let useIdAlias          = false;
        if( Application.config.AlternativeIdAlias.useIdAlias == "true" )
            useIdAlias          = true;

        let QH                  = new Application.Library.QueryHelper();
        let langs               = QH.langs();

        let host                = this.req.headers.host;
        let useHost             = this.req.headers.host.substring(this.req.headers.host.indexOf('.')+1).split(":")[0];
        let useSubhost          = this.req.headers.host.substring(0, this.req.headers.host.indexOf('.'));

        //проверка организации
        let ModelOrganizations  = new Application.Model.Organizations();
        let resultOrganizations = await ModelOrganizations.getByAlias(useSubhost);
        if (global.empty(resultOrganizations[0])) {
            throw new Error(404);
        }

        let orgId               = resultOrganizations[0].organizations.id;

        //маршруты
        let routes              = {};
        for(let index in Application.routes[".*."+useHost]) {
            routes[Application.routes[".*."+useHost][index].name] = Application.routes[".*."+useHost][index];
        }

        //массив ссылок
        let links               = [ {link: host} ];

        //sitemap
        links.push( {link: host + routes["Sitemap"].uri} );

        //welcome
        for(let index in langs) {
           let link         = routes["Welcome"].uri.replace(":lang?", langs[index])
            links.push( {link: host + link} );
        }

        //search
        for(let index in langs) {
            let link        = routes["Search"].uri.replace(":lang?", langs[index]).replace(":arg?", "")
            links.push( {link: host + link} );
        }

        //administrations
        for(let index in langs) {
            let link        = routes["Administrations"].uri.replace(":lang?", langs[index])
            links.push( {link: host + link} );
        }

        //organization
        for(let index in langs) {
            let link        = routes["Organization"].uri.replace(":lang?", langs[index])
            links.push( {link: host + link} );
        }

        //buildings
        for(let index in langs) {
            let link        = routes["Buildings"].uri.replace(":lang?", langs[index])
            links.push( {link: host + link} );
        }
        let ModelBuildings  = new Application.Model.Buildings();
        let resultBuildings = await ModelBuildings.getAll(orgId);
        for(let index in langs) {
            for(let indexResult in resultBuildings) {
                let link    = routes["Buildings"].uri.replace(":lang?", langs[index]).replace(":alias?", resultBuildings[indexResult].buildings.alias)
                links.push( {link: host + link} );
                if(useIdAlias) {
                    let linkId    = routes["Buildings"].uri.replace(":lang?", langs[index]).replace(":alias?", resultBuildings[indexResult].buildings.id)
                    links.push( {link: host + linkId} );
                }
            }
        }

        //subdivisions
        for(let index in langs) {
            let link        = routes["Subdivisions"].uri.replace(":lang?", langs[index])
            links.push( {link: host + link} );
        }
        let ModelSubdivisions  = new Application.Model.Subdivisions();
        let resultSubdivisions = await ModelSubdivisions.getAll(orgId);
        for(let index in langs) {
            for(let indexResult in resultSubdivisions) {
                let link    = routes["Subdivision"].uri.replace(":lang?", langs[index]).replace(":alias?", resultSubdivisions[indexResult].subdivisions.alias)
                links.push( {link: host + link} );
                if(useIdAlias) {
                    let linkId    = routes["Subdivision"].uri.replace(":lang?", langs[index]).replace(":alias?", resultSubdivisions[indexResult].subdivisions.id)
                    links.push( {link: host + linkId} );
                }
            }
        }

        //vacancys
        for(let index in langs) {
            let link        = routes["Vacancys"].uri.replace(":lang?", langs[index])
            links.push( {link: host + link} );
        }

        //workers
        for(let index in langs) {
            let link        = routes["Workers"].uri.replace(":lang?", langs[index])
            links.push( {link: host + link} );
        }

        //news
        for(let index in langs) {
            let link = routes["News"].uri.replace(":lang?", langs[index])
            links.push( {link: host + link} );
        }
        let ModelNews       = new Application.Model.News();
        let resultNews      = await ModelNews.getAll(orgId, QH.dl());
        for(let index in langs) {
            for(let indexResult in resultNews) {
                let link    = routes["New"].uri.replace(":lang?", langs[index]).replace(":alias?", resultNews[indexResult].news.alias)
                links.push( {link: host + link} );
                if(useIdAlias) {
                    let linkId    = routes["New"].uri.replace(":lang?", langs[index]).replace(":alias?", resultNews[indexResult].news.id)
                    links.push( {link: host + linkId} );
                }
            }
        }

        //events
        for(let index in langs) {
            let link = routes["Events"].uri.replace(":lang?", langs[index])
            links.push( {link: host + link} );
        }
        let ModelEvents     = new Application.Model.Events();
        let resultEvents    = await ModelEvents.getAll(orgId, QH.dl());
        for(let index in langs) {
            for(let indexResult in resultEvents) {
                let link    = routes["Event"].uri.replace(":lang?", langs[index]).replace(":alias?", resultEvents[indexResult].events.alias)
                links.push( {link: host + link} );
                if(useIdAlias) {
                    let linkId    = routes["Event"].uri.replace(":lang?", langs[index]).replace(":alias?", resultEvents[indexResult].events.id)
                    links.push( {link: host + linkId} );
                }
            }
        }

        //articles
        for(let index in langs) {
            let link = routes["Articles"].uri.replace(":lang?", langs[index])
            links.push( {link: host + link} );
        }
        let ModelArticles   = new Application.Model.Articles();
        let resultArticles  = await ModelArticles.getAll(orgId, QH.dl());
        for(let index in langs) {
            for(let indexResult in resultArticles) {
                let link    = routes["Article"].uri.replace(":lang?", langs[index]).replace(":alias?", resultArticles[indexResult].articles.alias)
                links.push( {link: host + link} );
                if(useIdAlias) {
                    let linkId = routes["Article"].uri.replace(":lang?", langs[index]).replace(":alias?", resultArticles[indexResult].articles.id)
                    links.push({link: host + linkId});
                }
            }
        }

        let content = new this.ViewJS("sitemaps.sitemap");
            content.contentData = links
        this.result.content = await content.render();
    }

    async client_onload(){
    }
}
