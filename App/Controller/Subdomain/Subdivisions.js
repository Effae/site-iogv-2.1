module.exports = class extends Controller_Subdomain_Template {

    async action_index(){
        let QH = new Application.Library.QueryHelper();

        let organization    = this.organization;
        let useLang         = this.useLang;
        let useHost         = this.useHost;
        let useSubhost      = this.useSubhost;

        //подразделения
        let ModelSubdivisions           = new Application.Model.Subdivisions();
        let resultSubdivisions          = await ModelSubdivisions.getAll(organization.id);

        let content = new this.ViewJS("subdivisions.subdivisions");
            content.organization        = organization;
            content.useLang             = useLang;
            content.useHost             = useHost;
            content.useSubhost          = useSubhost;
            content.count               = resultSubdivisions.length;
            content.contentData         = resultSubdivisions;
        this.result.content = await content.render();
    }

    async action_browse(){
        try {

            let QH = new Application.Library.QueryHelper();
  
            let organization    = this.organization;
            let useLang         = this.useLang;
            let useHost         = this.useHost;
            let useSubhost      = this.useSubhost;
            let useIdAlias      = this.useIdAlias;

            let alias           = this.req.params['alias'];

            //условите запроса
            let fieldName       = "subdivisions.alias"
            if( useIdAlias && new RegExp('^[0-9]*$').test(alias))
                fieldName       = "subdivisions.id"

            //подразделения
            let ModelSubdivisions       = new Application.Model.Subdivisions();
            let resultSubdivisions      = await ModelSubdivisions.getAllExtended(organization.id, [[fieldName,"=", alias]], 1);
    
            //проверка 404
            if( global.empty(resultSubdivisions[0]) ){
                throw new Error(404);
            }

            let content = new this.ViewJS("subdivisions.subdivision");
                content.organization        = organization;
                content.useLang             = useLang;
                content.useHost             = useHost;
                content.useSubhost          = useSubhost;
                content.contentData         = resultSubdivisions[0];
            this.result.content = await content.render();

            //канонический адрес
            if( useIdAlias )
                this.result.canonical       = await this.getUrl( {'alias':resultSubdivisions[0].subdivisions.id} );

        }catch (e) {
            console.log(e)
            
            await this.error404();
        }
    }

    async client_onload(){
    }
}
