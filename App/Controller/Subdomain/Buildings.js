module.exports = class extends Controller_Subdomain_Template {

    async action_index(){
        let QH = new Application.Library.QueryHelper();

        let organization    = this.organization;
        let useLang         = this.useLang;
        let useHost         = this.useHost;
        let useSubhost      = this.useSubhost;

        //подразделения
        let ModelBuildings              = new Application.Model.Buildings();
        let resultBuildings             = await ModelBuildings.getAll(organization.id);

        let content = new this.ViewJS("buildings.buildings");
            content.organization        = organization;
            content.useLang             = useLang;
            content.useHost             = useHost;
            content.useSubhost          = useSubhost;
            content.count               = resultBuildings.length;
            content.contentData         = resultBuildings;
        this.result.content = await content.render();
    }

    async action_browse(){
        try {

            let QH = new Application.Library.QueryHelper();

            let organization    = this.organization;
            let useLang         = this.useLang;
            let useHost         = this.useHost;
            let useSubhost      = this.useSubhost;
            let useIdAlias      = this.useIdAlias;

            let alias           = this.req.params['alias'];
    
            //условите запроса
            let fieldName       = "buildings.alias"
            if( useIdAlias && new RegExp('^[0-9]*$').test(alias))
                fieldName       = "buildings.id"

            //подразделения
            let ModelBuildings              = new Application.Model.Buildings();
            let resultBuildings             = await ModelBuildings.getAllExtended(organization.id, [[fieldName,"=", alias]], 1);

            //проверка 404
            if( global.empty(resultBuildings[0]) ){
                throw new Error(404);
            }
              
            let content = new this.ViewJS("buildings.building");
                content.organization        = organization;
                content.useLang             = useLang;
                content.useHost             = useHost;
                content.useSubhost          = useSubhost;
                content.contentData         = resultBuildings[0];
            this.result.content = await content.render();

            //канонический адрес
            if( useIdAlias )
                this.result.canonical       = await this.getUrl( {'alias':resultBuildings[0].buildings.id} );

        }catch (e) {
            await this.error404();
        }
    }

    async client_onload(){
    }
}
