module.exports = class extends Controller_Subdomain_Template {

    async action_index(){
        let QH = new Application.Library.QueryHelper();

        let organization    = this.organization;
        let useLang         = this.useLang;
        let useHost         = this.useHost;
        let useSubhost      = this.useSubhost;

        //события
        let ModelEvents     = new Application.Model.Events();
        let resultEvents    = await ModelEvents.getAll(organization.id, QH.dl());

        let content = new this.ViewJS("events.events");
            content.organization    = organization;
            content.useLang         = useLang;
            content.useHost         = useHost;
            content.useSubhost      = useSubhost;
            content.contentData     = resultEvents;
        this.result.content = await content.render();
    }

    async action_browse(){
        try {

            let QH = new Application.Library.QueryHelper();
            let TH = new Application.Library.TemplateHelper();

            let organization    = this.organization;
            let useLang         = this.useLang;
            let useHost         = this.useHost;
            let useSubhost      = this.useSubhost;
            let useIdAlias      = this.useIdAlias;

            let alias           = this.req.params['alias'];

            //условите запроса
            let fieldName       = "events.alias"
            if( useIdAlias && new RegExp('^[0-9]*$').test(alias))
                fieldName       = "events.id"

            //событие
            let ModelEvents     = new Application.Model.Events();
            let resultEvents    = await ModelEvents.getAll(organization.id, useLang, [[fieldName,"=", alias]], 1);

            //проверка 404
            if( global.empty(resultEvents[0]) ){
                throw new Error(404);
            }

            let content         = new this.ViewJS(TH.getTemplateNameById());
                content.organization    = organization;
                content.useLang         = useLang;
                content.useHost         = useHost;
                content.useSubhost      = useSubhost;
                content.contentData     = resultEvents[0].eventsLocal;
                content.navigationData  = undefined;
                content.documentData    = undefined;
            this.result.content = await content.render();

            this.result.description     = resultEvents[0].events.meta;
            this.result.keywords        = resultEvents[0].events.meta;

            //канонический адрес
            if( useIdAlias )
                this.result.canonical   = await this.getUrl( {'alias':resultEvents[0].events.id} );

        }catch (e) {
            await this.error404();
        }
    }

    async client_onload(){
    }
}
