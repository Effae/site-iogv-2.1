module.exports = class extends Application.System.Controller {
   
    async _before() {
        await super._before();
        let QH = new Application.Library.QueryHelper();

        let useHost = this.req.headers.host;
        let useLang = this.req.params['lang'];

        if (global.empty(useLang)) {
            useLang = QH.dl();
        }

        //проверка языка
        if (QH.langs().indexOf(useLang) == -1) {
            //ошибка наличия языка - устаноален дефолтный язык
            useLang = QH.dl();
            this._action = 'error404';
            this['action_' + this._action] = this.error404lang;
        }

        //проверка организации
        let ModelOrganizations      = new Application.Model.Organizations();
        let resultOrganizations     = await ModelOrganizations.getById(1);
        if (global.empty(resultOrganizations[0])) {
            //ошибка наличия организации - невозможна
            //добавить обработку в случае появления нескольких организаций уровня домена
        }

        //генерация меню
        let ModelMenu               = new Application.Model.Menu();
        let resultMenu              = await ModelMenu.getMenu(resultOrganizations[0].organizations.type, useLang, 6);

        //данные i18n
        this.i18n.lang              = useLang;
        let title                   = this._route.name;
        title = (Application.i18n && Application.i18n['eng'] && Application.i18n['eng'][title]) ? Application.i18n['eng'][title] : title;
        title = (Application.i18n && Application.i18n[useLang] && Application.i18n[useLang][title]) ? Application.i18n[useLang][title] : title;

        //использование альтернативных Id алиасов
        let useIdAlias              = ( Application.config.AlternativeIdAlias.useIdAlias == "true" ) ? true : false;

        //данные для контроллера
        this.useIdAlias             = useIdAlias;
        this.useHost                = useHost;
        this.useLang                = useLang;
        this.organization           = resultOrganizations[0].organizations;
        this.organization           = resultOrganizations[0].organizations;

        //данные для шаблона
        this.result.canonical       = this.req.headers.host + this.req.path;
        this.result.useHost         = useHost;
        this.result.useLang         = useLang;
        this.result.organization    = resultOrganizations[0].organizations;
        this.result.generalMenu     = resultMenu;
        this.result.title           = title;
        this.result.description     = "";
        this.result.keywords        = "";

        this.template  = 'templates.templateDomain';
    }

    async _after () {
        let View_Template = new this.ViewJS(this.template);
        for(let index in this.result) {
            View_Template[index] = this.result[index]
        }
        this.result = await View_Template.render();

        await super._after()
        return this.result;
    }

    async server_sendHelloMail(myMailAddress){
        //let Mailer = new Application.System.Mail();
        //return await Mailer.send(myMailAddress/*address where to send*/, "Hello from fresh install"/*mail subject*/, "This html part may be prepared via View()"/*html body of mail*/,false/* no attachments*/)
     }

    async getUrl (_params){
        let result = this.req.headers.host + this.req.path;
        try {
            //обновление параметов
            let newParams = this.req.params;
            for(let index in _params) {
                newParams[index] = _params[index];
            }
            //построение адреса
            let newPart = this._route.uri;
            for(let index in newParams) {
                newPart = newPart.replace(":"+index+"?", newParams[index]);
            }

            result = this.req.headers.host + newPart;
        }catch (e) {}
        return result;
    }

    async error404(){
        let content = new this.ViewJS('errors.error404');
            content.useHost = this.useHost;
        this.result.content = await content.render();
    }
    async error404lang(){
        let content = new this.ViewJS('errors.error404lang');
            content.useHost = this.useHost;
        this.result.content = await content.render();
    }
    async error404org(){
        let content = new this.ViewJS('errors.error404org');
        content.useHost = this.useHost;
        this.result.content = await content.render();
    }

}
