module.exports = class extends Controller_Template {

    async action_index(){
        let QH = new Application.Library.QueryHelper();

        let organization    = this.organization;
        let useLang         = this.useLang;
        let useHost         = this.useHost;

        let searchText                  = this.req.query.searchText;
        if(typeof (searchText) == "undefined") searchText = "";
            searchText                  = searchText.replace("+"," ")

        let useSearch                   = false;
        let countAll                    = 0;

        let resultOrganizations         = [];
        let resultBuildings             = [];
        let resultSubdivisions          = [];
        let resultAdministrations       = [];
        let resulArticles               = [];
        let resulNews                   = [];
        let resulEvents                 = [];
        let resulDocuments              = [];
        let resulWorkers                = [];

        let countOrg                    = 0;
        let countBui                    = 0;
        let countSub                    = 0;
        let countAdm                    = 0;
        let countArt                    = 0;
        let countNew                    = 0;
        let countEve                    = 0;
        let countDoc                    = 0;
        let countWor                    = 0;

        if (searchText.length > 2){
            useSearch                   = true;

            //организации
            let ModelOrganizations      = new Application.Model.Organizations();
            resultOrganizations         = await ModelOrganizations.search(searchText);
            countOrg                    = resultOrganizations.length;
            countAll                    = countAll + countOrg;

            //здания
            let ModelBuildings          = new Application.Model.Buildings();
            resultBuildings             = await ModelBuildings.search(searchText);
            countBui                    = resultBuildings.length;
            countAll                    = countAll + countBui;

            //подразделения
            let ModelSubdivisions       = new Application.Model.Subdivisions();
            resultSubdivisions          = await ModelSubdivisions.search(searchText);
            countSub                = resultSubdivisions.length;
            countAll                    = countAll + countSub;

            //администрация
            let ModelAdministrations    = new Application.Model.Administrations();
            resultAdministrations       = await ModelAdministrations.search(searchText);
            countAdm                    = resultAdministrations.length;
            countAll                    = countAll + countAdm;

            //статьи
            let ModelArticles           = new Application.Model.Articles();
            resulArticles               = await ModelArticles.search(searchText);
            countArt                    = resulArticles.length;
            countAll                    = countAll + countArt;

            //новости
            let ModelNews               = new Application.Model.News();
            resulNews                   = await ModelNews.search(searchText);
            countNew                    = resulNews.length;
            countAll                    = countAll + countNew;

            //события
            let ModelEvents             = new Application.Model.Events();
            resulEvents                 = await ModelEvents.search(searchText);
            countEve                    = resulEvents.length;
            countAll                    = countAll + countEve;
            console.log(countAll)
            //документы
            let ModelDocuments          = new Application.Model.Documents();
            resulDocuments              = await ModelDocuments.search(searchText);
            countDoc                    = resulDocuments.length;
            countAll                    = countAll + countDoc;

            //работники
            let ModelWorkers            = new Application.Model.Workers();
            resulWorkers                = await ModelWorkers.search(searchText);
            countWor                    = resulWorkers.length;
            countAll                    = countAll + countWor;
        }

        let content = new this.ViewJS("searchs.search");
            content.organization    = organization;
            content.useLang         = useLang;
            content.useHost         = useHost;
            content.useSearch       = useSearch;
            content.searchText      = searchText;
            content.count           = countAll;

            content.contentDataOrg  = resultOrganizations;
            content.contentDataBui  = resultBuildings;
            content.contentDataSub  = resultSubdivisions;
            content.contentDataAdm  = resultAdministrations;
            content.contentDataArt  = resulArticles;
            content.contentDataNew  = resulNews;
            content.contentDataEve  = resulEvents;
            content.contentDataDoc  = resulDocuments;
            content.contentDataWor  = resulWorkers;

            content.countOrg        = countOrg;
            content.countBui        = countBui;
            content.countSub        = countSub;
            content.countAdm        = countAdm;
            content.countArt        = countArt;
            content.countNew        = countNew;
            content.countEve        = countEve;
            content.countDoc        = countDoc;
            content.countWor        = countWor;
        this.result.content = await content.render();
    }

    async client_onload(){
    }
}
