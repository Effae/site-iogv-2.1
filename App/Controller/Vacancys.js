module.exports = class extends Controller_Template {

    async action_index(){
        let QH = new Application.Library.QueryHelper();

        let organization        = this.organization;
        let useLang             = this.useLang;
        let useHost             = this.useHost;

        let orgId               = this.req.query['organisation'];
        let orgTypeId           = this.req.query['type'];

        //типы организаций
        let ModelLists          = new Application.Model.Lists();
        let resultLists         = await ModelLists.getOrgType();

        //организаци
        let ModelOrganizations  = new Application.Model.Organizations();
        if( global.empty(orgTypeId) && !global.empty(orgId) ){
            let resultOrganization = await ModelOrganizations.getById(orgId);
            if( !global.empty(resultOrganization[0]) ) orgTypeId = resultOrganization[0].organizations.type;
        }
        let wheresOrganizations = [];
            if( !global.empty(orgTypeId) ) wheresOrganizations.push(["organizations.type","=",orgTypeId])
        let resultOrganizations = await ModelOrganizations.getAll(wheresOrganizations);

        //вакансии
        let ModelVacancys       = new Application.Model.Vacancys();
        let wheresVacancys      = [];
            if( !global.empty(orgTypeId) ) wheresVacancys.push(["organizations.type","=",orgTypeId])
            if( !global.empty(orgId) ) wheresVacancys.push(["organizations.id","=",orgId])
        let resultVacancys      = await ModelVacancys.getAllDomain(wheresVacancys);

        let content = new this.ViewJS("vacancys.vacancysDomain");
            content.organization    = organization;
            content.useLang         = useLang;
            content.useHost         = useHost;
            content.orgTypes        = resultLists;
            content.orgs            = resultOrganizations;
            content.selectedOrgType = orgTypeId;
            content.selectedOrg     = orgId;
            content.count           = resultVacancys.length;
            content.contentData     = resultVacancys;
        this.result.content = await content.render();
    }

    async client_onload(){
    }
}
