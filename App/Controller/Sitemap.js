module.exports = class extends Application.System.Controller {
    async _before() {
    }

    async _after() {
        return this.result.content;
    }

    async action_index(){
        let useIdAlias          = true;
        if( Application.config.AlternativeIdAlias.useIdAlias == "true" )
            useIdAlias          = true;

        let QH                  = new Application.Library.QueryHelper();
        let langs               = QH.langs();

        let host                = this.req.headers.host;
        let useHost             = this.req.headers.host.split(":")[0];

        let orgId               = 1;

        //маршруты
        let routes              = {};
        for(let index in Application.routes[useHost]) {
            routes[Application.routes[useHost][index].name] = Application.routes[useHost][index];
        }

        //массив ссылок
        let links               = [ {link: host} ];

        //sitemap
        links.push( {link: host + routes["Sitemap"].uri} );

        //welcome
        for(let index in langs) {
           let link         = routes["Welcome"].uri.replace(":lang?", langs[index])
            links.push( {link: host + link} );
        }

        //search
        for(let index in langs) {
            let link        = routes["Search"].uri.replace(":lang?", langs[index]).replace(":arg?", "")
            links.push( {link: host + link} );
        }

        //administrations
        for(let index in langs) {
            let link        = routes["Administrations"].uri.replace(":lang?", langs[index]).replace(":arg?", "")
            links.push( {link: host + link} );
        }

        //organisation
        for(let index in langs) {
            let link        = routes["OrganizationsTable"].uri.replace(":lang?", langs[index])
            links.push( {link: host + link} );
        }
        for(let index in langs) {
            let link        = routes["OrganizationsMap"].uri.replace(":lang?", langs[index]).replace(":arg?", "")
            links.push( {link: host + link} );
        }

        //vacancys
        for(let index in langs) {
            let link        = routes["Vacancys"].uri.replace(":lang?", langs[index])
            links.push( {link: host + link} );
        }

        //news
        for(let index in langs) {
            let link = routes["News"].uri.replace(":lang?", langs[index])
            links.push( {link: host + link} );
        }
        let ModelNews       = new Application.Model.News();
        let resultNews      = await ModelNews.getAll(orgId, QH.dl());
        for(let index in langs) {
            for(let indexResult in resultNews) {
                let link    = routes["New"].uri.replace(":lang?", langs[index]).replace(":alias?", resultNews[indexResult].news.alias)
                links.push( {link: host + link} );
                if (useIdAlias) {
                    let linkId = routes["New"].uri.replace(":lang?", langs[index]).replace(":alias?", resultNews[indexResult].news.id)
                    links.push({link: host + linkId});
                }
            }
        }

        //events
        for(let index in langs) {
            let link = routes["Events"].uri.replace(":lang?", langs[index])
            links.push( {link: host + link} );
        }
        let ModelEvents     = new Application.Model.Events();
        let resultEvents    = await ModelEvents.getAll(orgId, QH.dl());
        for(let index in langs) {
            for(let indexResult in resultEvents) {
                let link    = routes["Event"].uri.replace(":lang?", langs[index]).replace(":alias?", resultEvents[indexResult].events.alias)
                links.push( {link: host + link} );
                if (useIdAlias) {
                    let linkId = routes["Event"].uri.replace(":lang?", langs[index]).replace(":alias?", resultEvents[indexResult].events.id)
                    links.push({link: host + linkId});
                }
            }
        }

        //articles
        for(let index in langs) {
            let link = routes["Articles"].uri.replace(":lang?", langs[index])
            links.push( {link: host + link} );
        }
        let ModelArticles   = new Application.Model.Articles();
        let resultArticles  = await ModelArticles.getAll(orgId, QH.dl());
        for(let index in langs) {
            for(let indexResult in resultArticles) {
                let link = routes["Article"].uri.replace(":lang?", langs[index]).replace(":alias?", resultArticles[indexResult].articles.alias)
                links.push({link: host + link});
                if (useIdAlias) {
                    let linkId = routes["Article"].uri.replace(":lang?", langs[index]).replace(":alias?", resultArticles[indexResult].articles.id)
                    links.push({link: host + linkId});
                }
            }
        }

        let content = new this.ViewJS("sitemaps.sitemap");
            content.contentData = links
        this.result.content = await content.render();
    }

    async client_onload(){
    }
}
