module.exports = async function () {

    console.log('QueryHelper init start');
        await new Application.Library.QueryHelper().initQueryHelper(Library_QueryHelperСonfig);
    console.log('QueryHelper init end');

    console.log('QueryHelperI18n init start');
        //await new Application.Library.QueryHelperI18n().importToQHFromI18n("translations","translationsLocal");
        await new Application.Library.QueryHelperI18n().initQueryHelperI18n("translations","translationsLocal");
    console.log('QueryHelperI18n init end');

    console.log('DefaultArticles check start');
        //await new Application.Model.DefaultArticles().checkAndCreateAll();
    console.log('DefaultArticles check end');

}
