module.exports = {
    f:async function(){
        //обновление комплекта языковых пакетов
        try {
            if( Application.config.Sheduler.shedulerPeriodicOnOff == "true" ){
                await new Application.Library.QueryHelperI18n().initQueryHelperI18n("translations","translationsLocal");
            }
        }catch (e) {}
    },
    t: 60000 * Application.config.Sheduler.libraryQueryHelperI18n
}