module.exports = {
    f:async function(){
        //проверка и создание статей по умолчанию
        try {
            if( Application.config.Sheduler.shedulerPeriodicOnOff == "true" ){
                await new Application.Model.DefaultArticles().checkAndCreateAll();
            }
        }catch (e) {}
    },
    t: 60000 * Application.config.Sheduler.modelDefaultArticles
}