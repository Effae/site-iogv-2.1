module.exports = {
    f:async function(){
        //синхронизация данных ПУРС
        try {
            if( Application.config.Sheduler.shedulerPeriodicOnOff == "true" ){
                await new Application.Model.Synchronizations().syncAll();
            }
        }catch (e) {}
    },
    t: 60000 * Application.config.Sheduler.modelSynchronizations
}