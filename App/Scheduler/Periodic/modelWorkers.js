module.exports = {
    f:async function(){
        //удаление дублей Positions / Certificats / Increases для работников
        try {
            if( Application.config.Sheduler.shedulerPeriodicOnOff == "true" ){
                await new Application.Model.Workers().checkPCIAndDeleteDoublesAll();
            }
        }catch (e) {}
    },
    t: 60000 * Application.config.Sheduler.modelWorkers
}