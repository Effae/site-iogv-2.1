module.exports = class extends Application.System.Model {

    async getAll(_orgId, _lang, _wheres, _count) {
        let result = [];
        try {

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);

            //дефолтный язык
            let resultQH = QH.requestSelect("eventsLocal")
                .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                .where(QH.requestField('organizations.id'), '=' , _orgId)
                .where(QH.requestField('events.active'), '=' , QH.sv(true))
                .where(QH.requestField('eventsLocal.active'), '=' , QH.sv(true))
                .where(QH.requestField('eventsLocal.lang'), '=' , QH.sv(QH.dl()))
                .orderBy(QH.requestField('events.order', QH.order()))
                .orderBy(QH.requestField('events.id', QH.orderId()))
            if(typeof (_count) == "number") { resultQH.limit(_count) }
            if(typeof (_wheres) == "object") { for(let index in _wheres) {
                resultQH.where(QH.requestField(_wheres[index][0]), _wheres[index][1] , _wheres[index][2])
            }}
            resultQH = await resultQH;

            //локализации
            if(_lang != QH.dl()){

                let resultQHlocal = QH.requestSelect("eventsLocal")
                    .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                    .where(QH.requestField('organizations.id'), '=' , _orgId)
                    .where(QH.requestField('events.active'), '=' , QH.sv(true))
                    .where(QH.requestField('eventsLocal.active'), '=' , QH.sv(true))
                    .where(QH.requestField('eventsLocal.lang'), '=' ,QH.sv(_lang))
                if(typeof (_count) == "number"){
                    let idArray =  QH.fromResultGetFieldValues(resultQH, 'events.id' );
                    resultQHlocal.where(QH.requestField('events.id'), 'IN' , idArray);
                }
                if(typeof (_wheres) == "object") { for(let index in _wheres) {
                    resultQHlocal.where(QH.requestField(_wheres[index][0]), _wheres[index][1] , _wheres[index][2])
                }}
                resultQHlocal = await resultQHlocal;

                //слитие результатов
                QH.resultMerge(resultQH, resultQHlocal, 'events.id');
            }

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, 'eventsLocal.photo');

            result = resultQH;

        }catch (e) {}
        return result;
    }
    async getById(_orgId, _lang, _artId) {
        let result = [];
        try {

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);

            //дефолтный язык
            let resultQH = QH.requestSelect("eventsLocal")
                .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                .where(QH.requestField('organizations.id'), '=' , _orgId)
                .where(QH.requestField('events.active'), '=' , QH.sv(true))
                .where(QH.requestField('events.id'), '=' , _artId)
                .where(QH.requestField('eventsLocal.active'), '=' , QH.sv(true))
                .where(QH.requestField('eventsLocal.lang'), '=' , QH.sv(QH.dl()))
                .limit(1)
            resultQH = await resultQH;

            //локализации
            if(_lang != QH.dl()){

                let resultQHlocal = QH.requestSelect("eventsLocal")
                    .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                    .where(QH.requestField('organizations.id'), '=' , _orgId)
                    .where(QH.requestField('events.active'), '=' , QH.sv(true))
                    .where(QH.requestField('events.id'), '=' , _artId)
                    .where(QH.requestField('eventsLocal.active'), '=' , QH.sv(true))
                    .where(QH.requestField('eventsLocal.lang'), '=' ,QH.sv(_lang))
                    .limit(1)
                resultQHlocal = await resultQHlocal;

                //слитие результатов
                QH.resultMerge(resultQH, resultQHlocal, 'events.id');
            }

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, 'eventsLocal.photo');

            result = resultQH;;

        }catch (e) {}
        return result;
    }
    async getByAlias(_orgId, _lang, _artAlias) {
        let result = [];
        try {

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);

            //дефолтный язык
            let resultQH = QH.requestSelect("eventsLocal")
                .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                .where(QH.requestField('organizations.id'), '=' , _orgId)
                .where(QH.requestField('events.active'), '=' , QH.sv(true))
                .where(QH.requestField('events.alias'), '=' , _artAlias)
                .where(QH.requestField('eventsLocal.active'), '=' , QH.sv(true))
                .where(QH.requestField('eventsLocal.lang'), '=' , QH.sv(QH.dl()))
                .limit(1)
            resultQH = await resultQH;

            //локализации
            if(_lang != QH.dl()){

                let resultQHlocal = QH.requestSelect("eventsLocal")
                    .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                    .where(QH.requestField('organizations.id'), '=' , _orgId)
                    .where(QH.requestField('events.active'), '=' , QH.sv(true))
                    .where(QH.requestField('events.alias'), '=' , _artAlias)
                    .where(QH.requestField('eventsLocal.active'), '=' , QH.sv(true))
                    .where(QH.requestField('eventsLocal.lang'), '=' ,QH.sv(_lang))
                    .limit(1)
                resultQHlocal = await resultQHlocal;

                //слитие результатов
                QH.resultMerge(resultQH, resultQHlocal, 'events.id');
            }

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, 'eventsLocal.photo');

            result = resultQH;

        }catch (e) {}
        return result;
    }

    async search(_value, _wheres, _count) {
        let result = [];
        try {
            let value = "%"+_value+"%" ;

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);

            //статьи
            let resultQH = QH.requestSelect("eventsLocal")
                .where((builder) => builder
                    .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                    .where(QH.requestField('events.active'), '=' , QH.sv(true))
                    .where(QH.requestField('eventsLocal.active'), '=' , QH.sv(true))
                )
                .andWhere((builder) => builder
                    .where(QH.requestField('events.alias'), 'LIKE' , value)
                    .orWhere(QH.requestField('eventsLocal.name'), 'LIKE' , value)
                    .orWhere(QH.requestField('eventsLocal.fullname'), 'LIKE' , value)
                    .orWhere(QH.requestField('eventsLocal.shortname'), 'LIKE' , value)
                    .orWhere(QH.requestField('eventsLocal.text'), 'LIKE' , value)
                    .orWhere(QH.requestField('eventsLocal.meta'), 'LIKE' , value)
                )
                .orderBy(QH.requestField('organizations.order', QH.order()))
                .orderBy(QH.requestField('organizations.id', QH.orderId()))
                .orderBy(QH.requestField('events.order', QH.order()))
                .orderBy(QH.requestField('events.id', QH.orderId()))
                .orderBy(QH.requestField('eventsLocal.order', QH.order()))
                .orderBy(QH.requestField('eventsLocal.id', QH.orderId()))
            if(typeof (_count) == "number") { resultQH.limit(_count) }
            if(typeof (_wheres) == "object") { for(let index in _wheres) {
                resultQH.where(QH.requestField(_wheres[index][0]), _wheres[index][1] , _wheres[index][2])
            }}
            resultQH = await resultQH;

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, 'eventsLocal.photo');

            result = resultQH;

        }catch (e) {}
        return result;
    }

}
