module.exports = class extends Application.System.Model {

    async getCoicesByLisId(_lisId, _wheres, _count) {
        let result = [];
        try {

            let QH = new Application.Library.QueryHelper();
                QH.setUseAlias(true).setUseJoin(false, false, true);

            let resultQH = QH.requestList()
                .where(QH.requestField("lists.id"), '=' , _lisId)
                .orderBy(QH.requestField('lists.sort_order', QH.order()))
                .orderBy(QH.requestField('lists.id', QH.orderId()));
            if(typeof (_count) == "number") { resultQH.limit(_count) }
            if(typeof (_wheres) == "object") { for(let index in _wheres) {
                resultQH.where(QH.requestField(_wheres[index][0]), _wheres[index][1] , _wheres[index][2])
            }}
            resultQH = await resultQH;

            result = resultQH;

        }catch (e) {}
        return result;
    }
    async getCoicesByLisAlias(_lisAlias, _wheres, _count) {
        let result = [];
        try {

            let QH = new Application.Library.QueryHelper();
                QH.setUseAlias(true).setUseJoin(false, false, true);

            let resultQH = QH.requestList()
                .where(QH.requestField("lists.id"), '=' , QH.liv(_lisAlias))
                .orderBy(QH.requestField('listsChoices.sort_order', QH.order()))
                .orderBy(QH.requestField('listsChoices.id', QH.orderId()));
            if(typeof (_count) == "number") { resultQH.limit(_count) }
            if(typeof (_wheres) == "object") { for(let index in _wheres) {
                resultQH.where(QH.requestField(_wheres[index][0]), _wheres[index][1] , _wheres[index][2])
            }}
            resultQH = await resultQH;

            result = resultQH;

        }catch (e) {}
        return result;
    }

    async getOrgType() {
        return this.getCoicesByLisAlias("organizationsTypes");
    }
    async getTemType() {
        return this.getCoicesByLisAlias("templatesTypes");
    }
    async getMenType() {
        return this.getCoicesByLisAlias("menusTypes");
    }
    async getLangs() {
        return this.getCoicesByLisAlias("langs");
    }
}
