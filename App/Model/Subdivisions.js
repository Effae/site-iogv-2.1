module.exports = class extends Application.System.Model {

    async getAll(_orgId, _wheres, _count) {
        let result = [];
        try {

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, true, true);

            //подразделения
            let resultQH = QH.requestSelect("subdivisions")
                .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                .where(QH.requestField('organizations.id'), '=' , _orgId)
                .where(QH.requestField('subdivisions.active'), '=' , QH.sv(true))
                .orderBy(QH.requestField('subdivisions.order', QH.order()))
                .orderBy(QH.requestField('subdivisions.id', QH.orderId()))
            if(typeof (_count) == "number") { resultQH.limit(_count) }
            if(typeof (_wheres) == "object") { for(let index in _wheres) {
                resultQH.where(QH.requestField(_wheres[index][0]), _wheres[index][1] , _wheres[index][2])
            }}
            resultQH = await resultQH;

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, 'subdivisions.photo');
            QH.resultAddFilesLink(resultQH, 'subdivisions.managerPhoto');
            //добавление координат
            QH.resultAddLatLon(resultQH, 'subdivisions.coordinates');

            result = resultQH;

        }catch (e) {}
        return result;
    }
    async getById(_orgId, _subId) {
        let result = [];
        try {

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);

            //подразделение
            let resultQH = QH.requestSelect("subdivisions")
                .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                .where(QH.requestField('organizations.id'), '=' , _orgId)
                .where(QH.requestField('subdivisions.active'), '=' , QH.sv(true))
                .where(QH.requestField('subdivisions.id'), '=' , _subId)
                .limit(1)
            resultQH = await resultQH;

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, 'subdivisions.photo');
            QH.resultAddFilesLink(resultQH, 'subdivisions.managerPhoto');
            //добавление координат
            QH.resultAddLatLon(resultQH, 'subdivisions.coordinates');

            result = resultQH;;

        }catch (e) {}
        return result;
    }
    async getByAlias(_orgId, _subAlias) {
        let result = [];
        try {

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);

            //подразделение
            let resultQH = QH.requestSelect("subdivisions")
                .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                .where(QH.requestField('organizations.id'), '=' , _orgId)
                .where(QH.requestField('subdivisions.active'), '=' , QH.sv(true))
                .where(QH.requestField('subdivisions.alias'), '=' , _subAlias)
                .limit(1)
            resultQH = await resultQH;

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, 'subdivisions.photo');
            QH.resultAddFilesLink(resultQH, 'subdivisions.managerPhoto');
            //добавление координат
            QH.resultAddLatLon(resultQH, 'subdivisions.coordinates');

            result = resultQH;

        }catch (e) {}
        return result;
    }
    async getAllExtended(_orgId, _wheres, _count) {
        let result = [];
        try {

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, true, true);

            //подразделения
            let resultQH = QH.requestSelect("subdivisions")
                .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                .where(QH.requestField('organizations.id'), '=' , _orgId)
                .where(QH.requestField('subdivisions.active'), '=' , QH.sv(true))
                .orderBy(QH.requestField('subdivisions.order', QH.order()))
                .orderBy(QH.requestField('subdivisions.id', QH.orderId()))
            if(typeof (_count) == "number") { resultQH.limit(_count) }
            if(typeof (_wheres) == "object") { for(let index in _wheres) {
                resultQH.where(QH.requestField(_wheres[index][0]), _wheres[index][1] , _wheres[index][2])
            }}
            resultQH = await resultQH;

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, 'subdivisions.photo');
            QH.resultAddFilesLink(resultQH, 'subdivisions.managerPhoto');
            //добавление координат
            QH.resultAddLatLon(resultQH, 'subdivisions.coordinates');

            QH.setUseJoin(false, false, true);

            //здания
            let resultQHbui = QH.requestSelect("buildings")
                .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                .where(QH.requestField('organizations.id'), '=' , _orgId)
                .where(QH.requestField('buildings.active'), '=' , QH.sv(true))
            resultQHbui = await resultQHbui;

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQHbui, 'buildings.photo');
            //добавление координат
            QH.resultAddLatLon(resultQHbui, 'buildings.coordinates');

            //добавление массивов связей
            resultQH = QH.fromResultGetExtendedResult(resultQH, resultQHbui, "subdivisions.buildings", "buildings.id", "subdivisions.buildingsArray", true);

            result = resultQH;

        }catch (e) {
            console.log(e)

        }
        return result;
    }

    async search(_value, _wheres, _count) {
        let result = [];
        try {
            let value = "%"+_value+"%" ;

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);

            //здания
            let resultQH = QH.requestSelect("subdivisions")
                .where((builder) => builder
                    .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                    .where(QH.requestField('subdivisions.active'), '=' , QH.sv(true))
                )
                .andWhere((builder) => builder
                    .where(QH.requestField('subdivisions.name'), 'LIKE' , value)
                    .orWhere(QH.requestField('subdivisions.alias'), 'LIKE' , value)
                    .orWhere(QH.requestField('subdivisions.fullname'), 'LIKE' , value)
                    .orWhere(QH.requestField('subdivisions.shortname'), 'LIKE' , value)
                    .orWhere(QH.requestField('subdivisions.phone'), 'LIKE' , value)
                    .orWhere(QH.requestField('subdivisions.site'), 'LIKE' , value)
                    .orWhere(QH.requestField('subdivisions.email'), 'LIKE' , value)
                    .orWhere(QH.requestField('subdivisions.address'), 'LIKE' , value)
                    .orWhere(QH.requestField('subdivisions.coordinates'), 'LIKE' , value)
                    .orWhere(QH.requestField('subdivisions.managerPost'), 'LIKE' , value)
                    .orWhere(QH.requestField('subdivisions.managerFIO'), 'LIKE' , value)
                )
                .orderBy(QH.requestField('organizations.order', QH.order()))
                .orderBy(QH.requestField('organizations.id', QH.orderId()))
                .orderBy(QH.requestField('subdivisions.order', QH.order()))
                .orderBy(QH.requestField('subdivisions.id', QH.orderId()))
            if(typeof (_count) == "number") { resultQH.limit(_count) }
            if(typeof (_wheres) == "object") { for(let index in _wheres) {
                resultQH.where(QH.requestField(_wheres[index][0]), _wheres[index][1] , _wheres[index][2])
            }}
            resultQH = await resultQH;

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, 'subdivisions.photo');
            QH.resultAddFilesLink(resultQH, 'subdivisions.managerPhoto');
            //добавление координат
            QH.resultAddLatLon(resultQH, 'subdivisions.coordinates')

            result = resultQH;

        }catch (e) {}
        return result;
    }

    async syncByOid(_orgOid, _buiOid, _subOid) {
        let result      = {oid: _subOid, result: "error"};
        let checkChang  = function(_newObj, _oldObj){
            let result  = {};
            for(var index in _newObj) {
                if( index != "coordinates" && index != "phone" && !global.empty(_newObj[index]) && _newObj[index] != _oldObj[index] ){
                    result[index] = _newObj[index];
                }
                if( index == "coordinates" && !global.empty(_newObj[index]) && "[map]"+_newObj[index]+"[/map]" != _oldObj[index] ){
                    result[index] = "[map]"+_newObj[index]+"[/map]";
                }
                if( index == "phone" && !global.empty(_newObj[index]) ){
                    let phone = _newObj[index];
                    if(Array.isArray(_newObj[index])){
                        phone = "";
                        for(let indexPhone in _newObj[index]) {
                            phone = phone + " " + _newObj[index][indexPhone].phone;
                        }
                    }
                    if( phone != _oldObj[index] ){
                        result[index] = phone;
                    }
                }
            }
            return result;
        }
        let checkCreate = function(_newObj){
            let result  = {};
            for(var index in _newObj) {
                result[index] = _newObj[index];
                if(index != "coordinates"){
                    result[index] = "[map]"+_newObj[index]+"[/map]";
                }
                if(index != "alias"){
                    result[index] = QH.generationAlias(_newObj[index]);
                }
                if(index != "phone"){
                    let phone = _newObj[index];
                    if(Array.isArray(_newObj[index])){
                        phone = "";
                        for(let indexPhone in _newObj[index]) {
                            phone = phone + " " + _newObj[index][indexPhone].phone;
                        }
                    }
                    result[index] = phone;
                }
            }
            return result;
        }

        try {
            let RH = new Application.Library.RequestHelper();
            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, false);

            //подразделение QH
            let resultQH = QH.requestSelect("subdivisions")
                .where(QH.requestField('subdivisions.oid'), '=' , _buiOid)
                .limit(1)
            resultQH = await resultQH;

            //подразделение RH
            let link        = RH.map['purs'].paths.mo + "/" + _orgOid + "/" + RH.map['purs'].paths.sub + "/" + RH.map['purs'].paths.build_ + _buiOid +
                                                                        "/" + RH.map['purs'].paths.sub + "/" + RH.map['purs'].paths.sub_ + _subOid;
            let resultRH    = await RH.request( link );

            //синхронизация существующей записи
            if( !global.empty(resultQH) && !global.empty(resultQH[0]) ){
                //подразделение RH - не пусто
                if( resultRH.status && !global.empty(resultRH.response) ){
                    //проверка наличия изменений
                    let resObj = {
                        name:           resultRH.response.name,
                        address:        resultRH.response.address,
                        coordinates:    resultRH.response.coordinates,
                        phone:          resultRH.response.phones,
                        email:          resultRH.response.email,
                        managerPost:    resultRH.response.position,
                        managerFIO:     resultRH.response.manager,
                    }

                    let resultCheck = checkChang(resObj, resultQH[0].subdivisions);

                    //изменение
                    if( Object.keys(resultCheck).length > 0 ){
                        let resultEdit = await this.editItem(resultQH[0].subdivisions.id, resultCheck);
                        if( !global.empty(resultEdit) )
                            result = {oid: _buiOid, result: "edit"};
                    }else{  result = {oid: _buiOid, result: null};}
                }

                //подразделение RH - пусто
                if( !resultRH.status && global.empty(resultRH.response) ){
                    //удаление
                    let resultDelete = await this.deleteItem(resultQH[0].subdivisions.id);
                    if( !global.empty(resultDelete) )
                        result = {oid: _buiOid, result: "delete"};
                }
            }

            //синхронизация новой записи
            if( global.empty(resultQH) ){
                //подразделение RH - не пусто
                if(resultRH.status && !global.empty(resultRH.response) ){
                    //родительская запись
                    QH.setUseJoin(false, false, false);
                    let resultQHparent = QH.requestSelect("organizations")
                        .where(QH.requestField('organizations.oid'), '=' , _orgOid)
                        .limit(1)
                    resultQHparent = await resultQHparent;

                    //связанные записи
                    QH.setUseJoin(false, false, true);
                    let resultQHbuilding = QH.requestSelect("buildings")
                        .where(QH.requestField('organizations.oid'), '=' , _orgOid)
                        .where(QH.requestField('buildings.oid'), '=' , _buiOid)
                        .limit(1)
                    resultQHbuilding = await resultQHbuilding;

                    let building = "";
                    if( !global.empty(resultQHbuilding) && !global.empty(resultQHbuilding[0]) ){
                        building = resultQHbuilding[0].buildings.id;
                    }

                    //принятие изменения
                    if( !global.empty(resultQHparent) && !global.empty(resultQHparent[0]) ){
                        let resObj = {
                            parent_item_id: resultQHparent[0].organizations.id,
                            oid:            _subOid,
                            alias:          resultRH.response.name,
                            name:           resultRH.response.name,
                            fullname:       resultRH.response.name,
                            shortname:      resultRH.response.name,
                            phone:          resultRH.response.phones,
                            managerPost:    resultRH.response.position,
                            managerFIO:     resultRH.response.manager,
                            address:        resultRH.response.address,
                            coordinates:    resultRH.response.coordinates,
                            active:         QH.sv(true),
                            order:          50,
                            buildingMain:   building,
                            buildings:      building,
                        }

                        let resultCheck = checkCreate(resObj);

                        let resultCrete = await this.createItem(resultCheck);
                        if( !global.empty(resultCrete) )
                            result = {oid: _subOid, result: "create"};
                    }
                }
            }

        }catch (e) {}

        return result;
    }

    async editItem(_subId, _values) {
        let result = [];
        try {
            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, false);

            //изменение записи
            let requestUpdate =  QH.requestUpdate("subdivisions");
                requestUpdate.where(QH.requestField('subdivisions.id'), '=' ,  _subId);
            for(var index in _values) {
                requestUpdate.update(QH.requestField('subdivisions.'+index), _values[index]);
            }
            requestUpdate = await requestUpdate;

            result = requestUpdate;
        }catch (e) {}
        return result;
    }
    async deleteItem(_subId) {
        let result = [];
        try {
            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, false);

            //TODO добавить удаление связанностей в работниках

            //удаление записи
            let requestDelete = QH.requestDeleteByIds("subdivisions", [_subId]);
            requestDelete = await requestDelete;

            result = requestDelete;
        }catch (e) {}
        return result;
    }
    async createItem(_values) {
        let result = [];
        try {
            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, false);

            //создание записи
            let requestInsert =  QH.requestInsert("subdivisions", _values);
            requestInsert = await requestInsert;

            result = requestInsert;
        }catch (e) {}
        return result;
    }

    async deleteJoinArrayItem(_fieldName, _values) {
        let result = [];
        try {
            //приведение к масиву строковых значений
            if( !Array.isArray(_values) ) { _values = [_values]; }
            for(let index in _values) { _values[index] = String(_values[index]); }

            if( !global.empty(_values) ) {

                let QH = new Application.Library.QueryHelper();
                QH.setUseAlias(true).setUseJoin(false, false, false);

                //записи
                let resultQH = QH.requestSelect("subdivisions")
                    for(let index in _values) {
                        resultQH.orWhere(QH.requestField( "subdivisions."+_fieldName), 'REGEXP' , "(^|[^0-9])"+_values[index]+"([^0-9]|$)")
                    }
                resultQH = await resultQH;

                //очиска от искомых значений
                for(let index in resultQH) {
                    //создание очищенного значения
                    let oldValueArray = String(resultQH[index].subdivisions[_fieldName]).split(",");
                    let newValueArray = [];
                    let delValueArray = [];
                    for(let indexOldValue in oldValueArray) {
                        if( _values.indexOf(oldValueArray[indexOldValue]) == -1 ){
                            newValueArray.push(oldValueArray[indexOldValue])
                        }else{
                            delValueArray.push(oldValueArray[indexOldValue])
                        }
                    }

                    let resObj = {}
                        resObj[_fieldName] = newValueArray.join(",");

                    //изменение записи
                    let resultEdit = await this.editItem( resultQH[index].subdivisions.id, resObj );
                    if( global.empty(resultEdit) )
                        result.push(resultQH[index].subdivisions.id)
                }
            }
        }catch (e) {}

        return result;
    }
}
