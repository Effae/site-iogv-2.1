module.exports = class extends Application.System.Model {

    async getAll(_wheres, _count) {
        let result = [];
        try {

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, false);

            //статьи по умолчанию
            let resultQH = QH.requestSelect("defaultArticles")
                .where(QH.requestField('defaultArticles.active'), '=' , QH.sv(true))
                .orderBy(QH.requestField('defaultArticles.order', QH.order()))
                .orderBy(QH.requestField('defaultArticles.id', QH.orderId()))
            if(typeof (_count) == "number") { resultQH.limit(_count) }
            if(typeof (_wheres) == "object") { for(let index in _wheres) {
                resultQH.where(QH.requestField(_wheres[index][0]), _wheres[index][1] , _wheres[index][2])
            }}
            resultQH = await resultQH;

            result = resultQH;

        }catch (e) {}
        return result;
    }
    async getById(_defId) {
        let result = [];
        try {

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, false);

            //дефолтный язык
            let resultQH = QH.requestSelect("defaultArticles")
                .where(QH.requestField('defaultArticles.active'), '=' , QH.sv(true))
                .where(QH.requestField('defaultArticles.id'), '=' , _defId)
                .limit(1)
            resultQH = await resultQH;

            result = resultQH;;

        }catch (e) {}
        return result;
    }

    // !!! специальной метод - проверяет наличие и создает недостающие статьи для организации !!!
    async checkAndCreateAll() {
        let result = [];
        try {
            let artIDs = [];

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);

            //организации
            let resultQH = QH.requestSelect("organizations")
                .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                .orderBy(QH.requestField('organizations.order', QH.order()))
                .orderBy(QH.requestField('organizations.id', QH.orderId()))
            resultQH = await resultQH;

            //проверка и создание статей
            for(let index in resultQH) {
                let itemResult = await this.checkAndCreateByOrgId(resultQH[index].organizations.id);
                artIDs = [...artIDs, ...itemResult];
            }

            result = artIDs;

        }catch (e) {}
        return result;
    }
    async checkAndCreateByOrgId(_orgId) {
        let result = [];
        try {
            let artIDs = [];

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, true, true);

            //организация
            let resultQHorg = QH.requestSelect("organizations")
                .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                .where(QH.requestField('organizations.id'), '=' , _orgId)
                .limit(1)
            resultQHorg = await resultQHorg;

            //проверка существования организации
            if( !global.empty(resultQHorg) && !global.empty(resultQHorg[0]) ){
                let organization = resultQHorg[0].organizations;

                //статьи по умолчанию
                let resultQHdef = QH.requestSelect("defaultArticles")
                    .where((builder) => builder
                        .where(QH.requestField('defaultArticles.active'), '=' , QH.sv(true))
                    )
                    .andWhere((builder) => builder
                        .where(QH.requestField('defaultArticles.orgtypes'), '=' , "" + organization.type)
                        .orWhere(QH.requestField('defaultArticles.orgtypes'), '=' , 0)
                        .orWhere(QH.requestField('defaultArticles.orgtypes'), '=' , "")
                        .orWhere(QH.requestField('defaultArticles.orgtypes'), 'LIKE' , organization.type+",%")
                        .orWhere(QH.requestField('defaultArticles.orgtypes'), 'LIKE' , "%,"+organization.type+",%")
                        .orWhere(QH.requestField('defaultArticles.orgtypes'), 'LIKE' , "%,"+organization.type)
                    )
                    .orderBy(QH.requestField('defaultArticles.order', QH.order()))
                    .orderBy(QH.requestField('defaultArticles.id', QH.orderId()))
                resultQHdef = await resultQHdef;

                //статьи
                let resultQHart = QH.requestSelect("articles")
                        .where(QH.requestField('organizations.id'), '=' , organization.id)
                resultQHart = await resultQHart;

                //обход статей по умолчанию
                for(let indexDef in resultQHdef) {

                    //проверка существования статьи
                    let check               = false;
                    for(let indexArt in resultQHart) {
                        if(resultQHart[indexArt].articles.alias == resultQHdef[indexDef].defaultArticles.alias){
                            check = true;
                            break;
                        }
                    }

                    //статья не найдена, требуется создание
                    if(!check){

                        //ID родительской статьи
                        let parentArticlesId    = "";
                        if( !global.empty(resultQHdef[indexDef]["defaultArticles*parent"].alias) ){
                            for(let indexArt in resultQHart) {
                                if(resultQHart[indexArt].articles.alias == resultQHdef[indexDef]["defaultArticles*parent"].alias){
                                    parentArticlesId = resultQHart[indexArt].articles.id;
                                    break;
                                }
                            }
                        }

                        let ModelArticles   = new Application.Model.Articles();

                        //создание статьи
                        let valuesArt = {
                            parent_item_id: organization.id,
                            alias:          resultQHdef[indexDef].defaultArticles.alias,
                            name:           resultQHdef[indexDef].defaultArticles.name,
                            description:    resultQHdef[indexDef].defaultArticles.description,
                            parent:         parentArticlesId,
                            nok:            resultQHdef[indexDef].defaultArticles.nok,
                        }
                        let resultArt       = await ModelArticles.setArticles(valuesArt);

                        //обработка созданной статьи
                        if(resultArt && resultArt[0]){

                            let artId       = resultArt[0];

                            //статья
                            let resultQHnewArt = QH.requestSelect("articles")
                                .where(QH.requestField('organizations.id'), '=' , organization.id)
                                .where(QH.requestField('articles.id'), '=' , artId)
                                .limit(1)
                            resultQHnewArt = await resultQHnewArt;


                            //проверка существования созданной статьи
                            if( !global.empty(resultQHnewArt) && !global.empty(resultQHnewArt[0]) ){

                                //добавление новой статьи к resultQHart
                                resultQHart.push(resultQHnewArt[0]);

                                //создание локализации
                                let valuesLoc   = {
                                    parent_item_id: artId,
                                    name:           resultQHdef[indexDef].defaultArticles.nameLocal,
                                    text:           resultQHdef[indexDef].defaultArticles.text,
                                    template:       resultQHdef[indexDef].defaultArticles.template,
                                    meta:           resultQHdef[indexDef].defaultArticles.meta,
                                }
                                let resultLoc       = await ModelArticles.setArticlesLocal(valuesLoc);

                                //обработка созданной локализации
                                if(resultLoc && resultLoc[0]){
                                }

                                //добавление результата работы
                                artIDs.push(artId);

                            }

                        }

                    }

                }

            }

            result = artIDs;

        }catch (e) {}
        return result;
    }
}
