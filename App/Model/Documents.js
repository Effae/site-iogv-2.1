module.exports = class extends Application.System.Model {

    async getAll(_orgId, _artId, _wheres, _count) {
        let result = [];
        try {

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);

            let resultQH = QH.requestSelect("documents")
                .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                .where(QH.requestField('organizations.id'), '=' , _orgId)
                .where(QH.requestField('articles.active'), '=' , QH.sv(true))
                .where(QH.requestField('articles.id'), '=' , _artId)
                .where(QH.requestField('documents.active'), '=' , QH.sv(true))
                .orderBy(QH.requestField('documents.order', QH.order()))
                .orderBy(QH.requestField('documents.id', QH.orderId()))
            if(typeof (_count) == "number") { resultQH.limit(_count) }
            if(typeof (_wheres) == "object") { for(let index in _wheres) {
                resultQH.where(QH.requestField(_wheres[index][0]), _wheres[index][1] , _wheres[index][2])
            }}
            resultQH = await resultQH;

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, ["documents.filename"]);

            result = resultQH;

        }catch (e) {}
        return result;
    }
    async getById(_orgId, _docId) {
        let result = [];
        try {

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);

            let resultQH = QH.requestSelect("subdivisions")
                .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                .where(QH.requestField('organizations.id'), '=' , _orgId)
                .where(QH.requestField('documents.active'), '=' , QH.sv(true))
                .where(QH.requestField('documents.id'), '=' , _docId)
                .limit(1)
            resultQH = await resultQH;

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, ["documents.filename"]);

            result = resultQH;;

        }catch (e) {}
        return result;
    }

    async search(_value, _wheres, _count) {
        let result = [];
        try {
            let value = "%"+_value+"%" ;

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);

            //документы
            let resultQH = QH.requestSelect("documents")
                .where((builder) => builder
                    .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                    .where(QH.requestField('articles.active'), '=' , QH.sv(true))
                    .where(QH.requestField('documents.active'), '=' , QH.sv(true))
                )
                .andWhere((builder) => builder
                    .where(QH.requestField('documents.name'), 'LIKE' , value)
                    .orWhere(QH.requestField('documents.fullname'), 'LIKE' , value)
                    .orWhere(QH.requestField('documents.shortname'), 'LIKE' , value)
                    .orWhere(QH.requestField('documents.number'), 'LIKE' , value)
                    .orWhere(QH.requestField('documents.filename'), 'LIKE' , value)
                )
                .orderBy(QH.requestField('organizations.order', QH.order()))
                .orderBy(QH.requestField('organizations.id', QH.orderId()))
                .orderBy(QH.requestField('articles.order', QH.order()))
                .orderBy(QH.requestField('articles.id', QH.orderId()))
                .orderBy(QH.requestField('documents.order', QH.order()))
                .orderBy(QH.requestField('documents.id', QH.orderId()))
            if(typeof (_count) == "number") { resultQH.limit(_count) }
            if(typeof (_wheres) == "object") { for(let index in _wheres) {
                resultQH.where(QH.requestField(_wheres[index][0]), _wheres[index][1] , _wheres[index][2])
            }}
            resultQH = await resultQH;

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, 'documents.filename');

            result = resultQH;

        }catch (e) {}
        return result;
    }

    // !!! специальной метод - возвращает документ без ограничений !!!
    async getAny(_wheres, _count) {
        let result = [];
        try {

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);
            let resultQH = QH.requestSelect("documents")
                .orderBy(QH.requestField('documents.order', QH.order()))
                .orderBy(QH.requestField('documents.id', QH.orderId()))
            if(typeof (_count) == "number") { resultQH.limit(_count) }
            if(typeof (_wheres) == "object") { for(let index in _wheres) {
                resultQH.where(QH.requestField(_wheres[index][0]), _wheres[index][1] , _wheres[index][2])
            }}
            resultQH = await resultQH;

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, ["documents.filename"]);

            result = resultQH;

        }catch (e) {}
        return result;
    }

}
