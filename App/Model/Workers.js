module.exports = class extends Application.System.Model {

    async getAll(_orgId, _wheres, _count) {
        let result = [];
        try {

            let QH      = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);

            //работники
            let resultQH = QH.requestSelect("workers")
                .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                .where(QH.requestField('organizations.id'), '=' , _orgId)
                .where(QH.requestField('workers.active'), '=' , QH.sv(true))
                .orderBy(QH.requestField('workers.order', QH.order()))
                .orderBy(QH.requestField('workers.id', QH.orderId()))
            if(typeof (_count) == "number") { resultQH.limit(_count) }
            if(typeof (_wheres) == "object") { for(let index in _wheres) {
                resultQH.where(QH.requestField(_wheres[index][0]), _wheres[index][1] , _wheres[index][2])
            }}
            resultQH = await resultQH;

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, 'workers.photo');

            result = resultQH;

        }catch (e) {}
        return result;
    }
    async getById(_orgId, _worId) {
        let result = [];
        try {

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);

            let resultQH = QH.requestSelect("workers")
                .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                .where(QH.requestField('organizations.id'), '=' , _orgId)
                .where(QH.requestField('workers.active'), '=' , QH.sv(true))
                .where(QH.requestField('workers.id'), '=' , _worId)
                .limit(1)
            resultQH = await resultQH;

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, 'workers.photo');

            result = resultQH;

        }catch (e) {}
        return result;
    }
    async getAllExtended(_orgId, _wheres, _count) {
        let result = [];
        try {

            let QH      = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);

            //работники
            let resultQH = QH.requestSelect("workers")
                .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                .where(QH.requestField('organizations.id'), '=' , _orgId)
                .where(QH.requestField('workers.active'), '=' , QH.sv(true))
                .orderBy(QH.requestField('workers.order', QH.order()))
                .orderBy(QH.requestField('workers.id', QH.orderId()))
            if(typeof (_count) == "number") { resultQH.limit(_count) }
            if(typeof (_wheres) == "object") { for(let index in _wheres) {
                resultQH.where(QH.requestField(_wheres[index][0]), _wheres[index][1] , _wheres[index][2])
            }}
            resultQH = await resultQH;

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, 'workers.photo');

            QH.setUseAlias(true).setUseJoin(false, false, false);
            let idArray =  QH.fromResultGetFieldValues(resultQH, 'workers.id' );

            //выборка должностей
            let resultQHpositions = QH.requestSelect("positions")
                .where(QH.requestField('positions.parent_item_id'), 'IN' , idArray)
                .where(QH.requestField('positions.active'), '=' , QH.sv(true))
                .orderBy(QH.requestField('positions.order', QH.order()))
                .orderBy(QH.requestField('positions.id', QH.orderId()))
            resultQHpositions = await resultQHpositions;

            //выборка сертификатов
            let resultQHcertificates = QH.requestSelect("certificates")
                .where(QH.requestField('certificates.parent_item_id'), 'IN' , idArray)
                .where(QH.requestField('certificates.active'), '=' , QH.sv(true))
                .orderBy(QH.requestField('certificates.order', QH.order()))
                .orderBy(QH.requestField('certificates.id', QH.orderId()))
            resultQHcertificates = await resultQHcertificates;

            //выборка повышений
            let resultQHincreases = QH.requestSelect("increases")
                .where(QH.requestField('increases.parent_item_id'), 'IN' , idArray)
                .where(QH.requestField('increases.active'), '=' , QH.sv(true))
                .orderBy(QH.requestField('increases.order', QH.order()))
                .orderBy(QH.requestField('increases.id', QH.orderId()))
            resultQHincreases = await resultQHincreases;

            //добавление массивов связей
            resultQH = QH.fromResultGetExtendedResult(resultQH, resultQHpositions, "workers.id", "positions.parent_item_id", "workers.positions")
            resultQH = QH.fromResultGetExtendedResult(resultQH, resultQHcertificates, "workers.id", "certificates.parent_item_id", "workers.certificates")
            resultQH = QH.fromResultGetExtendedResult(resultQH, resultQHincreases, "workers.id", "increases.parent_item_id", "workers.increases")

            result = resultQH;

        }catch (e) {}
        return result;
    }

    async search(_value, _wheres, _count) {
        let result = [];
        try {
            let value = "%"+_value+"%" ;

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);

            //работники
            let resultQH = QH.requestSelect("workers")
                .where((builder) => builder
                    .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                    .where(QH.requestField('workers.active'), '=' , QH.sv(true))
                )
                .andWhere((builder) => builder
                    .where(QH.requestField('workers.fio'), 'LIKE' , value)
                    .orWhere(QH.requestField('workers.specialty'), 'LIKE' , value)
                    .orWhere(QH.requestField('workers.qualification'), 'LIKE' , value)
                    .orWhere(QH.requestField('workers.contacts'), 'LIKE' , value)
                    .orWhere(QH.requestField('workers.level'), 'LIKE' , value)
                    .orWhere(QH.requestField('workers.institution'), 'LIKE' , value)
                    .orWhere(QH.requestField('workers.year'), 'LIKE' , value)
                )
                .orderBy(QH.requestField('organizations.order', QH.order()))
                .orderBy(QH.requestField('organizations.id', QH.orderId()))
                .orderBy(QH.requestField('workers.order', QH.order()))
                .orderBy(QH.requestField('workers.id', QH.orderId()))
            if(typeof (_count) == "number") { resultQH.limit(_count) }
            if(typeof (_wheres) == "object") { for(let index in _wheres) {
                resultQH.where(QH.requestField(_wheres[index][0]), _wheres[index][1] , _wheres[index][2])
            }}
            resultQH = await resultQH;

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, 'workers.photo');

            result = resultQH;

        }catch (e) {}
        return result;
    }

    async syncByOid(_orgOid, _worOid) {
        let result      = {oid: _worOid, result: "error"};
        let checkChang  = function(_newObj, _oldObj){
            let result  = {};
            for(var index in _newObj) {
                if(!global.empty(_newObj[index]) && _newObj[index] != _oldObj[index]){
                    result[index] = _newObj[index];
                }
            }
            return result;
        }
        let checkCreate = function(_newObj){
            let result  = {};
            for(var index in _newObj) {
                result[index] = _newObj[index];
            }
            return result;
        }

        try {
            let RH = new Application.Library.RequestHelper();
            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, false);

            //работник QH
            let resultQH = QH.requestSelect("workers")
                .where(QH.requestField('workers.oid'), '=' , _worOid)
                .limit(1)
            resultQH = await resultQH;

            //работник RH
            let link        = RH.map['purs'].paths.mo + "/" + _orgOid + "/" + RH.map['purs'].paths.worker + "/" + _worOid;
            let resultRH    = await RH.request( link );

            //синхронизация существующей записи
            if( !global.empty(resultQH) && !global.empty(resultQH[0]) ){
                //работник RH - не пусто
                if( resultRH.status && !global.empty(resultRH.response) ){
                    //проверка наличия изменений
                    let resObj = {
                        fio:            resultRH.response.name,
                        contacts:       resultRH.response.phone,
                        level:          resultRH.response.level,
                        institution:    resultRH.response.institution,
                        year:           resultRH.response.year,
                        specialty:      resultRH.response.specialty,
                        qualification:  resultRH.response.qualification,
                    }
                    let resultCheck = checkChang(resObj, resultQH[0].workers);

                    //изменение
                    if( Object.keys(resultCheck).length > 0 ){
                        let resultEdit = await this.editItemWorker(resultQH[0].workers.id, resultCheck);
                        if( !global.empty(resultEdit) )
                            result = {oid: _worOid, result: "edit"};
                    }else{  result = {oid: _worOid, result: null};}

                    //---
                    //изменение связанных записей
                    //---

                    QH.setUseJoin(false, false, true);

                    //Positions
                    if( Array.isArray(resultRH.response.positions) ){
                        //должности
                        let resultQHpositions = QH.requestSelect("positions")
                            .where(QH.requestField('workers.id'), '=' , resultQH[0].workers.id)
                        resultQHpositions = await resultQHpositions;

                        //добавление
                        for(let resultRHindex in resultRH.response.positions) {
                            let resObj = {
                                text:           resultRH.response.positions[resultRHindex].position_text,
                                schedule:       resultRH.response.positions[resultRHindex].workingHours,
                            }

                            //проверка необходимости
                            let check = true;
                            for(let resultQHindex in resultQHpositions) {
                                let resultCheck = checkChang(resObj, resultQHpositions[resultQHindex].positions);
                                if( Object.keys(resultCheck).length == 0 ){
                                    check = false;
                                    break;
                                }
                            }

                            //безусловное игнорирование пустых записей (!!! опционально !!!)
                            if( resObj.text == "" && resObj.schedule == ""){
                                check = false;
                            }

                            //необходимость подтверждена (требуется добавление)
                            if(check){
                                resObj = {
                                    parent_item_id: resultQH[0].workers.id,
                                    subdivisionOID: "",
                                    text:           resultRH.response.positions[resultRHindex].position_text,
                                    schedule:       resultRH.response.positions[resultRHindex].workingHours,
                                    active:         QH.sv(true),
                                    order:          50,
                                }

                                let resultCheck = checkCreate(resObj);

                                let resultCretePosition = await this.createItemPosition(resultCheck);
                                if(resultCretePosition[0]) result["position_" + resultCretePosition[0]] = "create";
                            }
                        }

                        //удаление
                        for(let resultQHindex in resultQHpositions) {
                            //проверка необходимости
                            let check = true;
                            for(let resultRHindex in resultRH.response.positions) {
                                let resObj = {
                                    text:           resultRH.response.positions[resultRHindex].position_text,
                                    schedule:       resultRH.response.positions[resultRHindex].workingHours,
                                }

                                if( resObj.text == "" && resObj.schedule == ""){
                                    //безусловное игнорирование пустых записей (!!! опционально !!!)
                                }else{
                                    let resultCheck = checkChang(resObj, resultQHpositions[resultQHindex].positions);
                                    if( Object.keys(resultCheck).length == 0 ){
                                        check = false;
                                        break;
                                    }
                                }
                            }

                            //безусловное удаление пустых записей (!!! опционально !!!)
                            if( resultQHpositions[resultQHindex].positions.text == "" &&
                                resultQHpositions[resultQHindex].positions.schedule == ""){
                                check = true;
                            }

                            //необходимость подтверждена (требуется удаление)
                            if(check){
                                let resultDeletePosition = await this.deleteItemPosition(resultQHpositions[resultQHindex].positions.id);
                                if(resultDeletePosition) result["position_" + resultQHpositions[resultQHindex].positions.id] = "delete";
                            }
                        }
                    }

                    //Certificats
                    if( Array.isArray(resultRH.response.certificates) ){
                        //сертификаты
                        let resultQHcertificates = QH.requestSelect("certificates")
                            .where(QH.requestField('workers.id'), '=' , resultQH[0].workers.id)
                        resultQHcertificates = await resultQHcertificates;

                        //добавление
                        for(let resultRHindex in resultRH.response.certificates) {
                            let resObj = {
                                text:           resultRH.response.certificates[resultRHindex].speciality,
                                dateEnd:        resultRH.response.certificates[resultRHindex].deadline,
                            }

                            //проверка необходимости
                            let check = true;
                            for(let resultQHindex in resultQHcertificates) {
                                let resultCheck = checkChang(resObj, resultQHcertificates[resultQHindex].certificates);
                                if( Object.keys(resultCheck).length == 0 ){
                                    check = false;
                                    break;
                                }
                            }

                            //безусловное игнорирование пустых записей (!!! опционально !!!)
                            if( resObj.text == "" && resObj.dateEnd == ""){
                                check = false;
                            }

                            //необходимость подтверждена (требуется добавление)
                            if(check){
                                resObj = {
                                    parent_item_id: resultQH[0].workers.id,
                                    text:           resultRH.response.certificates[resultRHindex].speciality,
                                    dateStart:      "",
                                    dateEnd:        resultRH.response.certificates[resultRHindex].deadline,
                                    active:         QH.sv(true),
                                    order:          50,
                                }

                                let resultCheck = checkCreate(resObj);

                                let resultCreteCertificat = await this.createItemCertificat(resultCheck);
                                if(resultCreteCertificat[0]) result["certificat_" + resultCreteCertificat[0]] = "create";
                            }
                        }

                        //удаление
                        for(let resultQHindex in resultQHcertificates) {
                            //проверка необходимости
                            let check = true;
                            for(let resultRHindex in resultRH.response.certificates) {
                                let resObj = {
                                    text:           resultRH.response.certificates[resultRHindex].speciality,
                                    dateEnd:        resultRH.response.certificates[resultRHindex].deadline,
                                }

                                if( resObj.text == "" && resObj.dateEnd == ""){
                                    //безусловное игнорирование пустых записей (!!! опционально !!!)
                                }else{
                                    let resultCheck = checkChang(resObj, resultQHcertificates[resultQHindex].certificates);
                                    if (Object.keys(resultCheck).length == 0) {
                                        check = false;
                                        break;
                                    }
                                }
                            }

                            //безусловное удаление пустых записей (!!! опционально !!!)
                            if( resultQHcertificates[resultQHindex].certificates.text == "" &&
                                resultQHcertificates[resultQHindex].certificates.dateStart == "" &&
                                resultQHcertificates[resultQHindex].certificates.dateEnd == ""){
                                check = true;
                            }

                            //необходимость подтверждена (требуется удаление)
                            if(check){
                                let resultDeleteCertificat = await this.deleteItemCertificat(resultQHcertificates[resultQHindex].certificates.id);
                                if(resultDeleteCertificat) result["certificat_" + resultQHcertificates[resultQHindex].certificates.id] = "delete";
                            }
                        }
                    }

                    //Increases
                    if( Array.isArray(resultRH.response.levelUps) ){
                        //повышения
                        let resultQHincreases = QH.requestSelect("increases")
                            .where(QH.requestField('workers.id'), '=' , resultQH[0].workers.id)
                        resultQHincreases = await resultQHincreases;

                        //добавление
                        for(let resultRHindex in resultRH.response.levelUps) {
                            let resObj = {
                                text:           resultRH.response.levelUps[resultRHindex].speciality,
                                dateEnd:        resultRH.response.levelUps[resultRHindex].deadline,
                            }

                            //проверка необходимости
                            let check = true;
                            for(let resultQHindex in resultQHincreases) {
                                let changes = checkChang(resObj, resultQHincreases[resultQHindex].increases);
                                if( Object.keys(changes).length == 0 ){
                                    check = false;
                                    break;
                                }
                            }

                            //безусловное игнорирование пустых записей (!!! опционально !!!)
                            if( resObj.text == "" && resObj.dateEnd == ""){
                                check = false;
                            }

                            //необходимость подтверждена (требуется добавление)
                            if(check){
                                resObj = {
                                    parent_item_id: resultQH[0].workers.id,
                                    text:           resultRH.response.levelUps[resultRHindex].speciality,
                                    dateStart:      "",
                                    dateEnd:        resultRH.response.levelUps[resultRHindex].deadline,
                                    active:         QH.sv(true),
                                    order:          50,
                                }

                                let resultCheck = checkCreate(resObj);

                                let resultCreteIncreas = await this.createItemIncreas(resultCheck);
                                if(resultCreteIncreas[0]) result["increas" + resultCreteIncreas[0]] = "create";
                            }
                        }

                        //удаление
                        for(let resultQHindex in resultQHincreases) {
                            //проверка необходимости
                            let check = true;
                            for(let resultRHindex in resultRH.response.levelUps) {
                                let resObj = {
                                    text:           resultRH.response.levelUps[resultRHindex].speciality,
                                    dateEnd:        resultRH.response.levelUps[resultRHindex].deadline,
                                }

                                if( resObj.text == "" && resObj.dateEnd == ""){
                                    //безусловное игнорирование пустых записей (!!! опционально !!!)
                                }else{
                                    let resultCheck = checkChang(resObj, resultQHincreases[resultQHindex].increases);
                                    if( Object.keys(resultCheck).length == 0 ){
                                        check = false;
                                        break;
                                    }
                                }
                            }

                            //безусловное удаление пустых записей (!!! опционально !!!)
                            if( resultQHincreases[resultQHindex].increases.text == "" &&
                                resultQHincreases[resultQHindex].increases.dateStart == "" &&
                                resultQHincreases[resultQHindex].increases.dateEnd == ""){
                                check = true;
                            }

                            //необходимость подтверждена (требуется удаление)
                            if(check){
                                let resultDeleteIncreas = await this.deleteItemIncreas(resultQHincreases[resultQHindex].increases.id);
                                if(resultDeleteIncreas) result["increas_" + resultQHincreases[resultQHindex].increases.id] = "delete";
                            }
                        }
                    }

                    //---
                    //изменение связанных записей окончено
                    //---

                }

                //работник RH - пусто
                if( !resultRH.status && global.empty(resultRH.response) ){
                    //удаление
                    let resultDelete = await this.deleteItemWorker(resultQH[0].workers.id);
                    if( !global.empty(resultDelete) )
                        result = {oid: _worOid, result: "delete"};
                }
            }

            //синхронизация новой записи
            if( global.empty(resultQH) ){
                //подразделение RH - не пусто
                if(resultRH.status && !global.empty(resultRH.response) ){
                    //родительская запись
                    QH.setUseAlias(true).setUseJoin(false, false, false);
                    let resultQHparent = QH.requestSelect("organizations")
                        .where(QH.requestField('organizations.oid'), '=' , _orgOid)
                        .limit(1)
                    resultQHparent = await resultQHparent;

                    //принятие изменения
                    if( !global.empty(resultQHparent) && !global.empty(resultQHparent[0]) ){
                        let resObj = {
                            parent_item_id: resultQHparent[0].organizations.id,
                            oid:            _worOid,
                            fio:            resultRH.response.name,
                            contacts:       resultRH.response.phone,
                            level:          resultRH.response.level,
                            institution:    resultRH.response.institution,
                            year:           resultRH.response.year,
                            specialty:      resultRH.response.specialty,
                            qualification:  resultRH.response.qualification,
                            active:         QH.sv(true),
                            order:          50,
                        }

                        let resultCheck = checkCreate(resObj);

                        let resultCrete = await this.createItemWorker(resultCheck);
                        if( !global.empty(resultCrete) && !global.empty(resultCrete[0]) ) {
                            result = {oid: _worOid, result: "create"};

                            //---
                            //создание связанных записей
                            //---

                            //Positions
                            if( Array.isArray(resultRH.response.positions) ){
                                for(let index in resultRH.response.positions) {
                                    let resObj = {
                                        parent_item_id: resultCrete[0],
                                        subdivisionOID: "",
                                        text:           resultRH.response.positions[index].position_text,
                                        schedule:       resultRH.response.positions[index].workingHours,
                                        active:         QH.sv(true),
                                        order:          50,
                                    }
                                    if( resObj.text != "" || resObj.schedule != ""){
                                        let resultCheck = checkCreate(resObj);

                                        let resultCretePosition = await this.createItemPosition(resultCheck);
                                        if(resultCretePosition[0]) result["position_" + resultCretePosition[0]] = "create";
                                    }
                                }
                            }

                            //Certificats
                            if( Array.isArray(resultRH.response.certificates) ){
                                for(let index in resultRH.response.certificates) {
                                    let resObj = {
                                        parent_item_id: resultCrete[0],
                                        text:           resultRH.response.certificates[index].speciality,
                                        dateStart:      "",
                                        dateEnd:        resultRH.response.certificates[index].deadline,
                                        active:         QH.sv(true),
                                        order:          50,
                                    }
                                    if( resObj.text != "" || resObj.dateEnd != "" ){
                                        let resultCheck = checkCreate(resObj);

                                        let resultCreteCertificat = await this.createItemCertificat(resultCheck);
                                        if(resultCreteCertificat[0]) result["certificat_" + resultCreteCertificat[0]] = "create";
                                    }
                                }
                            }

                            //Increases
                            if( Array.isArray(resultRH.response.levelUps) ){
                                for(let index in resultRH.response.levelUps) {
                                    let resObj = {
                                        parent_item_id: resultCrete[0],
                                        text:           resultRH.response.levelUps[index].speciality,
                                        dateStart:      "",
                                        dateEnd:        resultRH.response.levelUps[index].deadline,
                                        active:         QH.sv(true),
                                        order:          50,
                                    }
                                    if( resObj.text != "" || resObj.dateEnd != "" ){
                                        let resultCheck = checkCreate(resObj);

                                        let resultCreteIncreas = await this.createItemIncreas(resultCheck);
                                        if(resultCreteIncreas[0]) result["increas_" + resultCreteIncreas[0]] = "create";
                                    }
                                }
                            }

                            //---
                            //создание связанных записей окончено
                            //---

                        }
                    }
                }
            }

        }catch (e) {}

        return result;
    }

    async checkPCIAndDeleteDoublesAll() {
        let result = [];
        try {

            let QH      = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);

            //организации
            let resultQH = QH.requestSelect("organizations")
                .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                .orderBy(QH.requestField('organizations.order', QH.order()))
                .orderBy(QH.requestField('organizations.id', QH.orderId()))
            resultQH = await resultQH;

            for(let index in resultQH) {
                let item = { orgId:resultQH[index].organizations.id, result:[] }
                item.result = await this.checkPCIAndDeleteDoublesByOrgId(resultQH[index].organizations.id);
                result.push(item);
            }

        }catch (e) {}
        return result;
    }
    async checkPCIAndDeleteDoublesByOrgId(_orgId) {
        let result = [];
        try {

            let QH      = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);

            //работники
            let resultQH = QH.requestSelect("workers")
                .where(QH.requestField('organizations.id'), '=' , _orgId)
                .where(QH.requestField('workers.active'), '=' , QH.sv(true))
                .orderBy(QH.requestField('workers.order', QH.order()))
                .orderBy(QH.requestField('workers.id', QH.orderId()))
            resultQH = await resultQH;

            for(let index in resultQH) {
                let item = { worId:resultQH[index].workers.id, result:[] }
                    item.result = await this.checkPCIAndDeleteDoublesByWorId(resultQH[index].workers.id);
                result.push(item);

            }

        }catch (e) {}
        return result;
    }
    async checkPCIAndDeleteDoublesByWorId(_worId) {
        let result          = [];
        let checkDoubles    = function(_resultQH, _fields){
            let result  = [];
            for(let indexBase in _resultQH) {
                for(let indexCheck in _resultQH) {
                    if(indexCheck > indexBase){
                        let isDoubl = true;
                        for(let indexField in _fields) {
                            if(_resultQH[indexBase][_fields[indexField]] != _resultQH[indexCheck][_fields[indexField]]){
                                isDoubl = false;
                                break;
                            }
                        }
                        if(isDoubl) result.push(_resultQH[indexCheck])
                    }
                }
            }
            return result.filter((item, index) => {
                return result.indexOf(item) === index
            });
        }

        try {
            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, false);

            //работник
            let resultQH = QH.requestSelect("workers")
                .where(QH.requestField('workers.id'), '=' , _worId)
                .limit(1)
            resultQH = await resultQH;

            if( !global.empty(resultQH[0]) ){
                let doubles         = [];

                //выборка должностей
                let resultQHpositions = QH.requestSelect("positions")
                    .where(QH.requestField('positions.parent_item_id'), '=' , resultQH[0].workers.id)
                    .where(QH.requestField('positions.active'), '=' , QH.sv(true))
                    .orderBy(QH.requestField('positions.order', QH.order()))
                    .orderBy(QH.requestField('positions.id', QH.orderId()))
                resultQHpositions = await resultQHpositions;
                //удаление дублей должностей
                doubles = checkDoubles(resultQHpositions, ["positions.subdivisionOID", "positions.text", "positions.schedule"])
                for(let index in doubles) {
                    let resultDelete = await this.deleteItemPosition(doubles[index].positions.id);
                    if(resultDelete) result["position_" + doubles[index].positions.id] = "delete";
                }

                //выборка сертификатов
                let resultQHcertificates = QH.requestSelect("certificates")
                    .where(QH.requestField('certificates.parent_item_id'), '=' , resultQH[0].workers.id)
                    .where(QH.requestField('certificates.active'), '=' , QH.sv(true))
                    .orderBy(QH.requestField('certificates.order', QH.order()))
                    .orderBy(QH.requestField('certificates.id', QH.orderId()))
                resultQHcertificates = await resultQHcertificates;
                //удаление дублей сертификатов
                doubles = checkDoublesn(resultQHcertificates, ["certificates.speciality", "certificates.dateStart", "certificates.dateEnd"])
                for(let index in doubles) {
                    let resultDelete = await this.deleteItemCertificat(doubles[index].certificates.id);
                    if(resultDelete) result["certificat_" + doubles[index].certificates.id] = "delete";
                }

                //выборка повышений
                let resultQHincreases = QH.requestSelect("increases")
                    .where(QH.requestField('increases.parent_item_id'), '=' , resultQH[0].workers.id)
                    .where(QH.requestField('increases.active'), '=' , QH.sv(true))
                    .orderBy(QH.requestField('increases.order', QH.order()))
                    .orderBy(QH.requestField('increases.id', QH.orderId()))
                resultQHincreases = await resultQHincreases;
                //удаление дублей повышений
                doubles = checkDoublesn(resultQHincreases, ["increases.increases", "increases.dateStart", "increases.dateEnd"])
                for(let index in doubles) {
                    let resultDelete = await this.deleteItemIncreas(doubles[index].increases.id);
                    if(resultDelete) result["increases_" + doubles[index].increases.id] = "delete";
                }
            }
        }catch (e) {}

        return result;
    }

    async editItemWorker(_worId, _values) {
        let result = [];
        try {
            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, false);

            //изменение записи
            let requestUpdate = QH.requestUpdate("workers");
            requestUpdate.where(QH.requestField('workers.id'), '=' ,  _worId);
            for(var index in _values) {
                requestUpdate.update(QH.requestField('workers.'+index), _values[index]);
            }
            requestUpdate = await requestUpdate;

            result = requestUpdate;
        }catch (e) {}
        return result;
    }
    async deleteItemWorker(_worId) {
        let result = [];
        try {
            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, false);

            //удаление дочерних записей
            let resultQHpositions = QH.requestSelect("positions")
                .where(QH.requestField('positions.parent_item_id'), '=' , _worId)
            resultQHpositions = await resultQHpositions;
            for(var index in resultQHpositions) { this.deleteItemPosition(resultQHpositions[index].positions.id) }

            let resultQHcertificates = QH.requestSelect("certificates")
                .where(QH.requestField('certificates.parent_item_id'), '=' , _worId)
            resultQHcertificates = await resultQHcertificates;
            for(var index in resultQHcertificates) { this.deleteItemCertificat(resultQHcertificates[index].certificates.id) }

            let resultQHincreases = QH.requestSelect("increases")
                .where(QH.requestField('increases.parent_item_id'), '=' , _worId)
            resultQHincreases = await resultQHincreases;
            for(var index in resultQHincreases) { this.deleteItemIncreases(resultQHincreases[index].increases.id) }

            //удаление записи
            let requestDelete = QH.requestDeleteByIds("workers", [_worId]);
            requestDelete = await requestDelete;

            result = requestDelete;
        }catch (e) {}
        return result;
    }
    async createItemWorker(_values) {
        let result = [];
        try {
            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, false);

            //создание записи
            let requestInsert =  QH.requestInsert("workers", _values);
            requestInsert = await requestInsert;

            result = requestInsert;
        }catch (e) {}
        return result;
    }

    async deleteItemPosition(_posId) {
        let result = [];
        try {
            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, false);

            //удаление записи
            let requestDelete = QH.requestDeleteByIds("positions", [_posId]);
            requestDelete = await requestDelete;

            result = requestDelete;
        }catch (e) {}
        return result;
    }
    async createItemPosition(_values) {
        let result = [];
        try {
            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, false);

            //создание записи
            let requestInsert =  QH.requestInsert("positions", _values);
            requestInsert = await requestInsert;

            result = requestInsert;
        }catch (e) {}
        return result;
    }
    async deleteItemCertificat(_cerId) {
        let result = [];
        try {
            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, false);

            //удаление записи
            let requestDelete = QH.requestDeleteByIds("certificates", [_cerId]);
            requestDelete = await requestDelete;

            result = requestDelete;
        }catch (e) {}
        return result;
    }
    async createItemCertificat(_values) {
        let result = [];
        try {
            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, false);

            //создание записи
            let requestInsert =  QH.requestInsert("certificates", _values);
            requestInsert = await requestInsert;

            result = requestInsert;
        }catch (e) {}
        return result;
    }
    async deleteItemIncreas(_incId) {
        let result = [];
        try {
            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, false);

            //удаление записи
            let requestDelete = QH.requestDeleteByIds("increases", [_incId]);
            requestDelete = await requestDelete;

            result = requestDelete;
        }catch (e) {}
        return result;
    }
    async createItemIncreas(_values) {
        let result = [];
        try {
            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, false);

            //создание записи
            let requestInsert =  QH.requestInsert("increases", _values);
            requestInsert = await requestInsert;

            result = requestInsert;
        }catch (e) {}
        return result;
    }

}
