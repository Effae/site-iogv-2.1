module.exports = class extends Application.System.Model {

    async getAll(_wheres, _count) {
        let result = [];
        try {

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, false);

            //синхронизации
            let resultQH = QH.requestSelect("synchronizations")
                .orderBy(QH.requestField('synchronizations.organization',QH.order()))
            if(typeof (_count) == "number") { resultQH.limit(_count) }
            if(typeof (_wheres) == "object") { for(let index in _wheres) {
                resultQH.where(QH.requestField(_wheres[index][0]), _wheres[index][1] , _wheres[index][2])
            }}
            resultQH = await resultQH;

            result = resultQH;

        }catch (e) {}
        return result;
    }
    async getById(_synId) {
        let result = [];
        try {

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, false);

            //синхронизация
            let resultQH = QH.requestSelect("synchronizations")
                .where(QH.requestField('synchronizations.id'), '=' , _synId)
                .limit(1)
            resultQH = await resultQH;

            result = resultQH;

        }catch (e) {}
        return result;
    }

    async syncAll() {
        let result = [];
        try {

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, true, true);

            //синхронизации
            let resultQH = QH.requestSelect("synchronizations")
                .orderBy(QH.requestField('synchronizations.order', QH.order()))
                .orderBy(QH.requestField('synchronizations.id', QH.orderId()))
            resultQH = await resultQH;

            for(let index in resultQH) {
                let itemResult          = {orgId: resultQH[index]["synchronizations*organization"].id, orgOid: resultQH[index]["synchronizations*organization"].oid}

                itemResult.startOrg     = new Date();
                itemResult.resultOrg    = ( !global.empty(await this.syncByIdForOrg(resultQH[index].synchronizations.id)) ) ? true : false;
                itemResult.endOrg       = new Date();

                itemResult.startBui     = new Date();
                itemResult.resultBui    = ( !global.empty(await this.syncByIdForBui(resultQH[index].synchronizations.id)) ) ? true : false;
                itemResult.endBui       = new Date();

                itemResult.startSub     = new Date();
                itemResult.resultSub    = ( !global.empty(await this.syncByIdForSub(resultQH[index].synchronizations.id)) ) ? true : false;
                itemResult.endSub       = new Date();

                itemResult.startWor     = new Date();
                itemResult.resultWor    = ( !global.empty(await this.syncByIdForWor(resultQH[index].synchronizations.id)) ) ? true : false;
                itemResult.endWor       = new Date();

                result.push(itemResult);
                //console.log(itemResult);
            }

        }catch (e) {}
        //console.log(result);
        return result;
    }
    async syncByIdForOrg(_synId) {
        let result = [];
        try {
            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, true, true);

            //синхронизация
            let resultQH = QH.requestSelect("synchronizations")
                .where(QH.requestField('synchronizations.id'), '=' , _synId)
                .where(QH.requestField('synchronizations.active'), '=' , QH.sv(true))
                .where(QH.requestField('synchronizations*organization.active'), '=' , QH.sv(true))
                .limit(1)
            resultQH = await resultQH;

            //проверка записи
            if( !global.empty(resultQH) && !global.empty(resultQH[0]) && !global.empty(resultQH[0]["synchronizations*organization"].oid) && resultQH[0].synchronizations.orgOn == QH.sv(true) ){

                let period          = resultQH[0].synchronizations.orgPeriod;
                let timestamp       = Math.floor(Date.now() / 1000);

                //проверка периода
                if( timestamp > (period * 3600 + resultQH[0].synchronizations.orgStart )){

                    //изменение времени старта последней синхронизации
                    timestamp           = Math.floor(Date.now() / 1000);
                    let editResultStart = await this.editItem(_synId, {orgStart: timestamp});

                    if( !global.empty(editResultStart) ){
                        //проведение синхронизации
                        let ModelOrganizations = new Application.Model.Organizations();
                        result.push( await ModelOrganizations.syncByOid(resultQH[0]["synchronizations*organization"].oid) );
                        }

                    //изменение времени окончания последней синхронизации
                    timestamp           = Math.floor(Date.now() / 1000);
                    let editResultEnd   = await this.editItem(_synId, {orgEnd: timestamp});
                }
            }

        }catch (e) {}
        return result;
    }
    async syncByIdForBui(_synId) {
        let result = [];
        try {
            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, true, true);

            //синхронизация
            let resultQH = QH.requestSelect("synchronizations")
                .where(QH.requestField('synchronizations.id'), '=' , _synId)
                .where(QH.requestField('synchronizations.active'), '=' , QH.sv(true))
                .where(QH.requestField('synchronizations*organization.active'), '=' , QH.sv(true))
                .limit(1)
            resultQH = await resultQH;

            //проверка записи
            if( !global.empty(resultQH) && !global.empty(resultQH[0]) && !global.empty(resultQH[0]["synchronizations*organization"].oid) && resultQH[0].synchronizations.buiOn == QH.sv(true)){

                let period          = resultQH[0].synchronizations.buiPeriod;
                let timestamp       = Math.floor(Date.now() / 1000);

                //проверка периода
                if( timestamp > (period * 3600 + resultQH[0].synchronizations.buiStart )){

                    //изменение времени старта последней синхронизации
                    timestamp           = Math.floor(Date.now() / 1000);
                    let editResultStart = await this.editItem(_synId, {buiStart: timestamp});

                    if( !global.empty(editResultStart) ){
                        let RH                  = new Application.Library.RequestHelper();
                        let ModelBuildings      = new Application.Model.Buildings();

                        //данные QH
                        let resultQHBuildings   = await ModelBuildings.getAll(resultQH[0].synchronizations.organization, [["buildings.oid", "<>", ""]]);
                        //данные RH
                        let link                = RH.map['purs'].paths.mo + "/" + resultQH[0]["synchronizations*organization"].oid +
                                                                            "/" + RH.map['purs'].paths.sub + "/";
                        let resultRH            = await RH.request( link );

                        //список oid
                        let oids                = QH.fromResultGetFieldValues(resultQHBuildings, "buildings.oid" );
                        if ( resultRH.status ) {
                            for(let index in resultRH.response) {
                                if(!oids.includes(resultRH.response[index].replace(RH.map['purs'].paths.build_,"")))
                                    oids.push(resultRH.response[index].replace(RH.map['purs'].paths.build_,""))
                            }
                        }

                        //проведение синхронизации зданий
                        for(let indexOids in oids) {
                            result.push( await ModelBuildings.syncByOid(resultQH[0]["synchronizations*organization"].oid, oids[indexOids] ) );
                        }
                    }

                    //изменение времени окончания последней синхронизации
                    timestamp           = Math.floor(Date.now() / 1000);
                    let editResultEnd   = await this.editItem(_synId, {buiEnd: timestamp})

                }
            }

        }catch (e) {  console.log(e) }
        return result;
    }
    async syncByIdForSub(_synId) {
        let result = [];
        try {
            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, true, true);

            //синхронизация
            let resultQH = QH.requestSelect("synchronizations")
                .where(QH.requestField('synchronizations.id'), '=' , _synId)
                .where(QH.requestField('synchronizations.active'), '=' , QH.sv(true))
                .where(QH.requestField('synchronizations*organization.active'), '=' , QH.sv(true))
                .limit(1)
            resultQH = await resultQH;

            //проверка записи
            if( !global.empty(resultQH) && !global.empty(resultQH[0]) && !global.empty(resultQH[0]["synchronizations*organization"].oid) && resultQH[0].synchronizations.subOn == QH.sv(true)){

                let period          = resultQH[0].synchronizations.subPeriod;
                let timestamp       = Math.floor(Date.now() / 1000);

                //проверка периода
                if( timestamp > (period * 3600 + resultQH[0].synchronizations.subStart )){

                    //изменение времени старта последней синхронизации
                    timestamp           = Math.floor(Date.now() / 1000);
                    let editResultStart = await this.editItem(_synId, {subStart: timestamp});

                    if( !global.empty(editResultStart) ){
                        let RH                  = new Application.Library.RequestHelper();
                        let ModelBuildings      = new Application.Model.Buildings();
                        let ModelSubdivisions   = new Application.Model.Subdivisions();

                        //данные QH
                        let resultQHBuildings   = await ModelBuildings.getAll(resultQH[0].synchronizations.organization, [["buildings.oid", "<>", ""]]);

                        //обход зданий
                        for(let indexBuildings in resultQHBuildings) {
                            //данные QH
                            let resultQHSubdivisions= await ModelSubdivisions.getAll(resultQH[0].synchronizations.organization, [["subdivisions.oid", "<>", ""],["buildings.buildingMain", "=", resultQHBuildings[indexBuildings].buildings.id]]);
                            //данные RH
                            let link                = RH.map['purs'].paths.mo + "/" + resultQH[0]["synchronizations*organization"].oid +
                                                                                "/" + RH.map['purs'].paths.sub +
                                                                                "/" + RH.map['purs'].paths.build_ + resultQHBuildings[indexBuildings].buildings.oid +
                                                                                "/" + RH.map['purs'].paths.sub + "/";
                            let resultRH            = await RH.request( link );

                            //список oid
                            let oids                = QH.fromResultGetFieldValues(resultQHSubdivisions, "subdivisions.oid" );
                            if ( resultRH.status ) {
                                for(let index in resultRH.response) {
                                    if(!oids.includes(resultRH.response[index].replace(RH.map['purs'].paths.sub_,"")))
                                        oids.push(resultRH.response[index].replace(RH.map['purs'].paths.sub_,""))
                                }
                            }

                            //проведение синхронизации филиалов
                            for(let indexOids in oids) {
                                result.push( await ModelSubdivisions.syncByOid(resultQH[0]["synchronizations*organization"].oid, resultQHBuildings[indexBuildings].buildings.oid, oids[indexOids]) );
                            }
                        }
                    }

                    //изменение времени окончания последней синхронизации
                    timestamp           = Math.floor(Date.now() / 1000);
                    let editResultEnd   = await this.editItem(_synId, {subEnd: timestamp})

                }
            }

        }catch (e) {  console.log(e) }
        return result;
    }
    async syncByIdForWor(_synId) {
        let result = [];
        try {
            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, true, false);

            //синхронизация
            let resultQH = QH.requestSelect("synchronizations")
                .where(QH.requestField('synchronizations.id'), '=' , _synId)
                .where(QH.requestField('synchronizations*organization.active'), '=' , QH.sv(true))
                .limit(1)
            resultQH = await resultQH;

            //проверка записи
            if( !global.empty(resultQH) && !global.empty(resultQH[0]) && !global.empty(resultQH[0]["synchronizations*organization"].oid) && resultQH[0].synchronizations.worOn == QH.sv(true) ){

                let period          = resultQH[0].synchronizations.worPeriod;
                let timestamp       = Math.floor(Date.now() / 1000);

                //проверка периода
                if( timestamp > (period * 3600 + resultQH[0].synchronizations.worStart )){

                    //изменение времени старта последней синхронизации
                    timestamp           = Math.floor(Date.now() / 1000);
                    let editResultStart = await this.editItem(_synId, {worStart: timestamp});

                    if( !global.empty(editResultStart) ){
                        let RH                  = new Application.Library.RequestHelper();
                        let ModelWorkers        = new Application.Model.Workers();

                        //данные QH
                        let resultQHWorker      = await ModelWorkers.getAll(resultQH[0].synchronizations.organization);
                        //данные RH
                        let link                = RH.map['purs'].paths.mo + "/" + resultQH[0]["synchronizations*organization"].oid +
                                                                            "/" + RH.map['purs'].paths.worker + "/";
                        let resultRH            = await RH.request( link );

                        //список oid
                        let oids                = QH.fromResultGetFieldValues(resultQHWorker, "workers.oid" );
                        if ( resultRH.status ){
                            for(let index in resultRH.response) {
                                if(!oids.includes(resultRH.response[index]))
                                    oids.push(resultRH.response[index])
                            }
                        }

                        //проведение синхронизации
                        for(let indexOids in oids) {
                            result.push( await ModelWorkers.syncByOid(resultQH[0]["synchronizations*organization"].oid, oids[indexOids] ) );
                        }
                    }

                    //изменение времени окончания последней синхронизации
                    timestamp           = Math.floor(Date.now() / 1000);
                    let editResultEnd   = await this.editItem(_synId, {worEnd: timestamp});
                }
            }

        }catch (e) {}
        return result;
    }

    async editItem(_synId, _values) {
        let result = [];
        try {
            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, false);

            //изменение записи
            let requestUpdate = QH.requestUpdate("synchronizations");
            requestUpdate.where(QH.requestField('synchronizations.id'), '=' ,  _synId);
            for(var index in _values) {
                requestUpdate.update(QH.requestField('synchronizations.'+index), _values[index]);
            }
            requestUpdate = await requestUpdate;

            result = requestUpdate;
        }catch (e) {}
        return result;
    }
}

