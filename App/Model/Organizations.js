module.exports = class extends Application.System.Model {

    async getAll(_wheres, _count) {
        let result = [];
        try {

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, false);

            //организации
            let resultQH = QH.requestSelect("organizations")
                .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                .orderBy(QH.requestField('organizations.order', QH.order()))
                .orderBy(QH.requestField('organizations.id', QH.orderId()))
            if(typeof (_count) == "number") { resultQH.limit(_count) }
            if(typeof (_wheres) == "object") { for(let index in _wheres) {
                resultQH.where(QH.requestField(_wheres[index][0]), _wheres[index][1] , _wheres[index][2])
            }}
            resultQH = await resultQH;

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, 'organizations.photo');
            QH.resultAddFilesLink(resultQH, 'organizations.managerPhoto');
            //добавление координат
            QH.resultAddLatLon(resultQH, 'organizations.coordinates')

            result = resultQH;

        }catch (e) {}
        return result;
    }
    async getById(_orgId) {
        let result = [];
        try {

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, false);

            //организация
            let resultQH = QH.requestSelect("organizations")
                .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                .where(QH.requestField('organizations.id'), '=' , _orgId)
                .limit(1)
            resultQH = await resultQH;

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, 'organizations.photo');
            QH.resultAddFilesLink(resultQH, 'organizations.managerPhoto');
            //добавление координат
            QH.resultAddLatLon(resultQH, 'organizations.coordinates')

            result = resultQH;

        }catch (e) {}
        return result;
    }
    async getByAlias(_orgAlias) {
        let result = [];
        try {

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, false);

            //организация
            let resultQH = QH.requestSelect("organizations")
                .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                .where(QH.requestField('organizations.alias'), '=' , _orgAlias)
                .limit(1)
            resultQH = await resultQH;

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, 'organizations.photo');
            QH.resultAddFilesLink(resultQH, 'organizations.managerPhoto');
            //добавление координат
            QH.resultAddLatLon(resultQH, 'organizations.coordinates')

            result = resultQH;

        }catch (e) {}
        return result;
    }

    async search(_value, _wheres, _count) {
        let result = [];
        try {
            let value = "%"+_value+"%" ;

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);

            //организации
            let resultQH = QH.requestSelect("organizations")
                .where((builder) => builder
                    .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                )
                .andWhere((builder) => builder
                    .where(QH.requestField('organizations.name'), 'LIKE' , value)
                    .orWhere(QH.requestField('organizations.alias'), 'LIKE' , value)
                    .orWhere(QH.requestField('organizations.fullname'), 'LIKE' , value)
                    .orWhere(QH.requestField('organizations.shortname'), 'LIKE' , value)
                    .orWhere(QH.requestField('organizations.address'), 'LIKE' , value)
                    .orWhere(QH.requestField('organizations.phone'), 'LIKE' , value)
                    .orWhere(QH.requestField('organizations.site'), 'LIKE' , value)
                    .orWhere(QH.requestField('organizations.email'), 'LIKE' , value)
                    .orWhere(QH.requestField('organizations.coordinates'), 'LIKE' , value)
                    .orWhere(QH.requestField('organizations.managerPost'), 'LIKE' , value)
                    .orWhere(QH.requestField('organizations.managerFIO'), 'LIKE' , value)
                )
                .orderBy(QH.requestField('organizations.order', QH.order()))
                .orderBy(QH.requestField('organizations.id', QH.orderId()))
            if(typeof (_count) == "number") { resultQH.limit(_count) }
            if(typeof (_wheres) == "object") { for(let index in _wheres) {
                resultQH.where(QH.requestField(_wheres[index][0]), _wheres[index][1] , _wheres[index][2])
            }}
            resultQH = await resultQH;

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, 'organizations.photo');
            QH.resultAddFilesLink(resultQH, 'organizations.managerPhoto');
            //добавление координат
            QH.resultAddLatLon(resultQH, 'organizations.coordinates')

            result = resultQH;

        }catch (e) {}
        return result;
    }

    async syncByOid(_orgOid) {
        let result      = {oid: _orgOid, result: "error"};
        let checkChang  = function(_oldObj, _newObj){
            let result  = {};
            for(var index in _newObj) {
                if(index != "coordinates" && !global.empty(_newObj[index]) && _newObj[index] != _oldObj[index]){
                    result[index] = _newObj[index];
                }
                if(index == "coordinates" && !global.empty(_newObj[index]) && "[map]"+_newObj[index]+"[/map]" != _oldObj[index]){
                    result[index] = "[map]"+_newObj[index]+"[/map]";
                }
            }
            return result;
        }

        try {
            let RH = new Application.Library.RequestHelper();
            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, false);

            //организация QH
            let resultQH    = QH.requestSelect("organizations")
                .where(QH.requestField('organizations.oid'), '=' , _orgOid)
                .limit(1)
            resultQH        = await resultQH;

            //организация RH
            let link        = RH.map['purs'].paths.mo + "/" + _orgOid;
            let resultRH    = await RH.request( link );

            //синхронизация существующей записи
            if( !global.empty(resultQH) && !global.empty(resultQH[0]) ){
                //организация RH - не пусто
                if( resultRH.status && !global.empty(resultRH.response) ){
                    //проверка наличия изменений
                    let resObj = {
                        name:           resultRH.response.name,
                        fullname:       resultRH.response.fullname,
                        shortname:      resultRH.response.shortname,
                        address:        resultRH.response.address,
                        coordinates:    resultRH.response.coordinates,
                        phone:          resultRH.response.phone,
                        site:           resultRH.response.site,
                        managerPost:    resultRH.response.position,
                        managerFIO:     resultRH.response.manager,
                    }
                    let changes = checkChang(resultQH[0].organizations, resObj);

                    //изменение
                    if( Object.keys(changes).length > 0 ){
                        let resultEdit = await this.editItem(resultQH[0].organizations.id, changes);
                        if( !global.empty(resultEdit) )
                            result = {oid: _orgOid, result: "edit"};
                    }else{  result = {oid: _orgOid, result: null};}
                }
            }

        }catch (e) {


            console.log(e);
        }
        return result;
    }

    async editItem(_orgId, _values) {
        let result = [];
        try {
            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, false);

            let requestUpdate =  QH.requestUpdate("organizations");
                requestUpdate.where(QH.requestField('organizations.id'), '=' ,  _orgId);
            for(var index in _values) {
                requestUpdate.update(QH.requestField('organizations.'+index), _values[index]);
            }
            requestUpdate = await requestUpdate;

            result = requestUpdate;
        }catch (e) {}
        return result;
    }

}
