module.exports = class extends Application.System.Model {

    async getAll(_orgId, _wheres, _count) {
        let result = [];
        try {

            let QH      = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);

            //работники
            let resultQH = QH.requestSelect("administrations")
                .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                .where(QH.requestField('organizations.id'), '=' , _orgId)
                .where(QH.requestField('administrations.active'), '=' , QH.sv(true))
                .orderBy(QH.requestField('administrations.order', QH.order()))
                .orderBy(QH.requestField('administrations.id', QH.orderId()))
            if(typeof (_count) == "number") { resultQH.limit(_count) }
            if(typeof (_wheres) == "object") { for(let index in _wheres) {
                resultQH.where(QH.requestField(_wheres[index][0]), _wheres[index][1] , _wheres[index][2])
            }}
            resultQH = await resultQH;

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, 'administrations.photo');

            result = resultQH;

        }catch (e) {}
        return result;
    }
    async getById(_orgId, _admId) {
        let result = [];
        try {

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);

            let resultQH = QH.requestSelect("administrations")
                .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                .where(QH.requestField('organizations.id'), '=' , _orgId)
                .where(QH.requestField('administrations.active'), '=' , QH.sv(true))
                .where(QH.requestField('administrations.id'), '=' , _admId)
                .limit(1)
            resultQH = await resultQH;

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, 'administrations.photo');

            result = resultQH;

        }catch (e) {}
        return result;
    }

    async search(_value, _wheres, _count) {
        let result = [];
        try {
            let value = "%"+_value+"%" ;

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);

            //администрация
            let resultQH = QH.requestSelect("administrations")
                .where((builder) => builder
                    .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                    .where(QH.requestField('administrations.active'), '=' , QH.sv(true))
                )
                .andWhere((builder) => builder
                    .where(QH.requestField('administrations.fio'), 'LIKE' , value)
                    .orWhere(QH.requestField('administrations.post'), 'LIKE' , value)
                )
                .orderBy(QH.requestField('administrations.order', QH.order()))
                .orderBy(QH.requestField('administrations.id', QH.orderId()))
            if(typeof (_count) == "number") { resultQH.limit(_count) }
            if(typeof (_wheres) == "object") { for(let index in _wheres) {
                resultQH.where(QH.requestField(_wheres[index][0]), _wheres[index][1] , _wheres[index][2])
            }}
            resultQH = await resultQH;

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, 'administrations.photo');

            result = resultQH;

        }catch (e) {}
        return result;
    }

}
