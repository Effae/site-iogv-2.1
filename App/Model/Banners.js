module.exports = class extends Application.System.Model {

    async getAll(_orgId, _wheres, _count) {
        let result = [];
        try {

            let QH      = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);

            let resultQH = QH.requestSelect("banners")
                .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                .where(QH.requestField('organizations.id'), '=' , _orgId)
                .where(QH.requestField('banners.active'), '=' , QH.sv(true))
                .orderBy(QH.requestField('banners.order', QH.order()))
                .orderBy(QH.requestField('banners.id', QH.orderId()))
            if(typeof (_count) == "number") { resultQH.limit(_count) }
            if(typeof (_wheres) == "object") { for(let index in _wheres) {
                resultQH.where(QH.requestField(_wheres[index][0]), _wheres[index][1] , _wheres[index][2])
            }}
            resultQH = await resultQH;

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, 'banners.photo');

            result = resultQH;

        }catch (e) {}
        return result;
    }
    async getById(_orgId, _banId) {
        let result = [];
        try {

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);

            let resultQH = QH.requestSelect("banners")
                .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                .where(QH.requestField('organizations.id'), '=' , _orgId)
                .where(QH.requestField('banners.active'), '=' , QH.sv(true))
                .where(QH.requestField('banners.id'), '=' , _banId)
                .limit(1)
            resultQH = await resultQH;

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, 'banners.photo');

            result = resultQH;

        }catch (e) {}
        return result;
    }

}
