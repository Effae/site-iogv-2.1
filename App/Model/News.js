module.exports = class extends Application.System.Model {

    async getAll(_orgId, _lang, _wheres, _count) {
        let result = [];
        try {

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);

            //дефолтный язык
            let resultQH = QH.requestSelect("newsLocal")
                .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                .where(QH.requestField('organizations.id'), '=' , _orgId)
                .where(QH.requestField('news.active'), '=' , QH.sv(true))
                .where(QH.requestField('newsLocal.active'), '=' , QH.sv(true))
                .where(QH.requestField('newsLocal.lang'), '=' , QH.sv(QH.dl()))
                .orderBy(QH.requestField('news.order', QH.order()))
                .orderBy(QH.requestField('news.id', QH.orderId()))
            if(typeof (_count) == "number") { resultQH.limit(_count) }
            if(typeof (_wheres) == "object") { for(let index in _wheres) {
                resultQH.where(QH.requestField(_wheres[index][0]), _wheres[index][1] , _wheres[index][2])
            }}
            resultQH = await resultQH;

            //локализации
            if(_lang != QH.dl()){

                let resultQHlocal = QH.requestSelect("newsLocal")
                    .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                    .where(QH.requestField('organizations.id'), '=' , _orgId)
                    .where(QH.requestField('news.active'), '=' , QH.sv(true))
                    .where(QH.requestField('newsLocal.active'), '=' , QH.sv(true))
                    .where(QH.requestField('newsLocal.lang'), '=' ,QH.sv(_lang))
                if(typeof (_count) == "number"){
                    let idArray =  QH.fromResultGetFieldValues(resultQH, 'news.id' );
                    resultQHlocal.where(QH.requestField('news.id'), 'IN' , idArray);
                }
                if(typeof (_wheres) == "object") { for(let index in _wheres) {
                    resultQHlocal.where(QH.requestField(_wheres[index][0]), _wheres[index][1] , _wheres[index][2])
                }}
                resultQHlocal = await resultQHlocal;

                //слитие результатов
                QH.resultMerge(resultQH, resultQHlocal, 'news.id');
            }

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, 'newsLocal.photo');

            result = resultQH;

        }catch (e) {}
        return result;
    }
    async getById(_orgId, _lang, _artId) {
        let result = [];
        try {

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);

            //дефолтный язык
            let resultQH = QH.requestSelect("newsLocal")
                .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                .where(QH.requestField('organizations.id'), '=' , _orgId)
                .where(QH.requestField('news.active'), '=' , QH.sv(true))
                .where(QH.requestField('news.id'), '=' , _artId)
                .where(QH.requestField('newsLocal.active'), '=' , QH.sv(true))
                .where(QH.requestField('newsLocal.lang'), '=' , QH.sv(QH.dl()))
                .limit(1)
            resultQH = await resultQH;

            //локализации
            if(_lang != QH.dl()){

                let resultQHlocal = QH.requestSelect("newsLocal")
                    .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                    .where(QH.requestField('organizations.id'), '=' , _orgId)
                    .where(QH.requestField('news.active'), '=' , QH.sv(true))
                    .where(QH.requestField('news.id'), '=' , _artId)
                    .where(QH.requestField('newsLocal.active'), '=' , QH.sv(true))
                    .where(QH.requestField('newsLocal.lang'), '=' ,QH.sv(_lang))
                    .limit(1)
                resultQHlocal = await resultQHlocal;

                //слитие результатов
                QH.resultMerge(resultQH, resultQHlocal, 'news.id');
            }

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, 'newsLocal.photo');

            result = resultQH;;

        }catch (e) {}
        return result;
    }
    async getByAlias(_orgId, _lang, _artAlias) {
        let result = [];
        try {

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);

            //дефолтный язык
            let resultQH = QH.requestSelect("newsLocal")
                .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                .where(QH.requestField('organizations.id'), '=' , _orgId)
                .where(QH.requestField('news.active'), '=' , QH.sv(true))
                .where(QH.requestField('news.alias'), '=' , _artAlias)
                .where(QH.requestField('newsLocal.active'), '=' , QH.sv(true))
                .where(QH.requestField('newsLocal.lang'), '=' , QH.sv(QH.dl()))
                .limit(1)
            resultQH = await resultQH;

            //локализации
            if(_lang != QH.dl()){

                let resultQHlocal = QH.requestSelect("newsLocal")
                    .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                    .where(QH.requestField('organizations.id'), '=' , _orgId)
                    .where(QH.requestField('news.active'), '=' , QH.sv(true))
                    .where(QH.requestField('news.alias'), '=' , _artAlias)
                    .where(QH.requestField('newsLocal.active'), '=' , QH.sv(true))
                    .where(QH.requestField('newsLocal.lang'), '=' ,QH.sv(_lang))
                    .limit(1)
                resultQHlocal = await resultQHlocal;

                //слитие результатов
                QH.resultMerge(resultQH, resultQHlocal, 'news.id');
            }

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, 'newsLocal.photo');

            result = resultQH;

        }catch (e) {}
        return result;
    }

    async search(_value, _wheres, _count) {
        let result = [];
        try {
            let value = "%"+_value+"%" ;

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);

            //статьи
            let resultQH = QH.requestSelect("newsLocal")
                .where((builder) => builder
                    .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                    .where(QH.requestField('news.active'), '=' , QH.sv(true))
                    .where(QH.requestField('newsLocal.active'), '=' , QH.sv(true))
                )
                .andWhere((builder) => builder
                    .where(QH.requestField('news.alias'), 'LIKE' , value)
                    .orWhere(QH.requestField('newsLocal.name'), 'LIKE' , value)
                    .orWhere(QH.requestField('newsLocal.fullname'), 'LIKE' , value)
                    .orWhere(QH.requestField('newsLocal.shortname'), 'LIKE' , value)
                    .orWhere(QH.requestField('newsLocal.text'), 'LIKE' , value)
                    .orWhere(QH.requestField('newsLocal.meta'), 'LIKE' , value)
                )
                .orderBy(QH.requestField('organizations.order', QH.order()))
                .orderBy(QH.requestField('organizations.id', QH.orderId()))
                .orderBy(QH.requestField('news.order', QH.order()))
                .orderBy(QH.requestField('news.id', QH.orderId()))
                .orderBy(QH.requestField('newsLocal.order', QH.order()))
                .orderBy(QH.requestField('newsLocal.id', QH.orderId()))
            if(typeof (_count) == "number") { resultQH.limit(_count) }
            if(typeof (_wheres) == "object") { for(let index in _wheres) {
                resultQH.where(QH.requestField(_wheres[index][0]), _wheres[index][1] , _wheres[index][2])
            }}
            resultQH = await resultQH;

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, 'newsLocal.photo');

            result = resultQH;

        }catch (e) {}
        return result;
    }

}
