module.exports = class extends Application.System.Model {

    async getAll(_orgId, _lang, _wheres, _count) {
        let result = [];
        try {

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);

            //дефолтный язык
            let resultQH = QH.requestSelect("articlesLocal")
                .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                .where(QH.requestField('organizations.id'), '=' , _orgId)
                .where(QH.requestField('articles.active'), '=' , QH.sv(true))
                .where(QH.requestField('articlesLocal.active'), '=' , QH.sv(true))
                .where(QH.requestField('articlesLocal.lang'), '=' , QH.sv(QH.dl()))
                .orderBy(QH.requestField('articles.order', QH.order()))
                .orderBy(QH.requestField('articles.id', QH.orderId()))
            if(typeof (_count) == "number") { resultQH.limit(_count) }
            if(typeof (_wheres) == "object") { for(let index in _wheres) {
                resultQH.where(QH.requestField(_wheres[index][0]), _wheres[index][1] , _wheres[index][2])
            }}
            resultQH = await resultQH;

            //локализации
            if(_lang != QH.dl()){

                let resultQHlocal = QH.requestSelect("articlesLocal")
                    .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                    .where(QH.requestField('organizations.id'), '=' , _orgId)
                    .where(QH.requestField('articles.active'), '=' , QH.sv(true))
                    .where(QH.requestField('articlesLocal.active'), '=' , QH.sv(true))
                    .where(QH.requestField('articlesLocal.lang'), '=' ,QH.sv(_lang))
                if(typeof (_count) == "number"){
                    let idArray =  QH.fromResultGetFieldValues(resultQH, 'articles.id' );
                    resultQHlocal.where(QH.requestField('articles.id'), 'IN' , idArray);
                }
                if(typeof (_wheres) == "object") { for(let index in _wheres) {
                    resultQHlocal.where(QH.requestField(_wheres[index][0]), _wheres[index][1] , _wheres[index][2])
                }}
                resultQHlocal = await resultQHlocal;

                //слитие результатов
                QH.resultMerge(resultQH, resultQHlocal, 'articles.id');
            }

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, 'articlesLocal.photo');

            result = resultQH;

        }catch (e) {}
        return result;
    }
    async getById(_orgId, _lang, _artId) {
        let result = [];
        try {

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);

            //дефолтный язык
            let resultQH = QH.requestSelect("articlesLocal")
                .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                .where(QH.requestField('organizations.id'), '=' , _orgId)
                .where(QH.requestField('articles.active'), '=' , QH.sv(true))
                .where(QH.requestField('articles.id'), '=' , _artId)
                .where(QH.requestField('articlesLocal.active'), '=' , QH.sv(true))
                .where(QH.requestField('articlesLocal.lang'), '=' , QH.sv(QH.dl()))
                .limit(1)
            resultQH = await resultQH;

            //локализации
            if(_lang != QH.dl()){

                let resultQHlocal = QH.requestSelect("articlesLocal")
                    .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                    .where(QH.requestField('organizations.id'), '=' , _orgId)
                    .where(QH.requestField('articles.active'), '=' , QH.sv(true))
                    .where(QH.requestField('articles.id'), '=' , _artId)
                    .where(QH.requestField('articlesLocal.active'), '=' , QH.sv(true))
                    .where(QH.requestField('articlesLocal.lang'), '=' ,QH.sv(_lang))
                    .limit(1)
                resultQHlocal = await resultQHlocal;

                //слитие результатов
                QH.resultMerge(resultQH, resultQHlocal, 'articles.id');
            }

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, 'articlesLocal.photo');

            result = resultQH;;

        }catch (e) {}
        return result;
    }
    async getByAlias(_orgId, _lang, _artAlias) {
        let result = [];
        try {

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);

            //дефолтный язык
            let resultQH = QH.requestSelect("articlesLocal")
                .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                .where(QH.requestField('organizations.id'), '=' , _orgId)
                .where(QH.requestField('articles.active'), '=' , QH.sv(true))
                .where(QH.requestField('articles.alias'), '=' , _artAlias)
                .where(QH.requestField('articlesLocal.active'), '=' , QH.sv(true))
                .where(QH.requestField('articlesLocal.lang'), '=' , QH.sv(QH.dl()))
                .limit(1)
            resultQH = await resultQH;

            //локализации
            if(_lang != QH.dl()){

                let resultQHlocal = QH.requestSelect("articlesLocal")
                    .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                    .where(QH.requestField('organizations.id'), '=' , _orgId)
                    .where(QH.requestField('articles.active'), '=' , QH.sv(true))
                    .where(QH.requestField('articles.alias'), '=' , _artAlias)
                    .where(QH.requestField('articlesLocal.active'), '=' , QH.sv(true))
                    .where(QH.requestField('articlesLocal.lang'), '=' ,QH.sv(_lang))
                    .limit(1)
                resultQHlocal = await resultQHlocal;

                //слитие результатов
                QH.resultMerge(resultQH, resultQHlocal, 'articles.id');
            }

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, 'articlesLocal.photo');

            result = resultQH;

        }catch (e) {}
        return result;
    }

    async search(_value, _wheres, _count) {
        let result = [];
        try {
            let value = "%"+_value+"%" ;

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);

            //статьи
            let resultQH = QH.requestSelect("articlesLocal")
                .where((builder) => builder
                    .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                    .where(QH.requestField('articles.active'), '=' , QH.sv(true))
                    .where(QH.requestField('articlesLocal.active'), '=' , QH.sv(true))
                )
                .andWhere((builder) => builder
                    .where(QH.requestField('articles.alias'), 'LIKE' , value)
                    .orWhere(QH.requestField('articlesLocal.name'), 'LIKE' , value)
                    .orWhere(QH.requestField('articlesLocal.fullname'), 'LIKE' , value)
                    .orWhere(QH.requestField('articlesLocal.shortname'), 'LIKE' , value)
                    .orWhere(QH.requestField('articlesLocal.text'), 'LIKE' , value)
                    .orWhere(QH.requestField('articlesLocal.meta'), 'LIKE' , value)
                )
                .orderBy(QH.requestField('organizations.order', QH.order()))
                .orderBy(QH.requestField('organizations.id', QH.orderId()))
                .orderBy(QH.requestField('articles.order', QH.order()))
                .orderBy(QH.requestField('articles.id', QH.orderId()))
                .orderBy(QH.requestField('articlesLocal.order', QH.order()))
                .orderBy(QH.requestField('articlesLocal.id', QH.orderId()))
            if(typeof (_count) == "number") { resultQH.limit(_count) }
            if(typeof (_wheres) == "object") { for(let index in _wheres) {
                resultQH.where(QH.requestField(_wheres[index][0]), _wheres[index][1] , _wheres[index][2])
            }}
            resultQH = await resultQH;

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, 'articlesLocal.photo');

            result = resultQH;

        }catch (e) {

            console.log("eeeeeee")

            console.log(e)
        }
        return result;
    }

    async setArticles(_values) {
        let result = null;
        try {

            let QH = new Application.Library.QueryHelper();

            let values = {}
            values.parent_item_id   = _values.parent_item_id;

            values.alias            = _values.alias;
            values.name             = _values.name;
            values.description      = ( typeof(_values.description) != "undefined" )        ? _values.description : "";
            values.parent           = ( typeof(_values.parent) != "undefined" )             ? _values.parent : "";
            values.nok              = ( typeof(_values.nok) != "undefined" )                ? _values.nok : "";

            values.active           = ( typeof(_values.active) != "undefined" )             ? _values.active : QH.sv(true);
            values.order            = ( typeof(_values.order) != "undefined" )              ? _values.order : 50;

            let requestInsert =  QH.requestInsert("articles", values);
            result = await requestInsert;

        }catch (e) {}
        return result;
    }
    async setArticlesLocal(_values) {
        let result = null;
        try {

            let QH = new Application.Library.QueryHelper();

            let values = {}
                values.parent_item_id   = _values.parent_item_id;

                values.name             = _values.name;
                values.fullname         = ( typeof(_values.fullname) != "undefined" )       ? _values.fullname : "";
                values.shortname        = ( typeof(_values.shortname) != "undefined" )      ? _values.shortname : "";
                values.text             = ( typeof(_values.text) != "undefined" )           ? _values.text : "";
                values.photo            = ( typeof(_values.photo) != "undefined" )          ? _values.photo : "";

                values.template         = ( typeof(_values.template) != "undefined" )       ? _values.template : "";
                values.lang             = ( typeof(_values.lang) != "undefined" )           ? _values.lang : QH.sv(QH.dl());
                values.meta             = ( typeof(_values.meta) != "undefined" )           ? _values.meta : "";
                values.active           = ( typeof(_values.active) != "undefined" )         ? _values.active : QH.sv(true);
                values.order            = ( typeof(_values.order) != "undefined" )          ? _values.order : 50;

            let requestInsert =  QH.requestInsert("articlesLocal", values);
            result = await requestInsert;

        }catch (e) {}
        return result;
    }

}
