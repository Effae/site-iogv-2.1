module.exports = class extends Application.System.Model {

    async getAll(_orgId, _wheres, _count) {
        let result = [];
        try {

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);

            //здания
            let resultQH = QH.requestSelect("buildings")
                .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                .where(QH.requestField('organizations.id'), '=' , _orgId)
                .where(QH.requestField('buildings.active'), '=' , QH.sv(true))
                .orderBy(QH.requestField('buildings.order', QH.order()))
                .orderBy(QH.requestField('buildings.id', QH.orderId()))
            if(typeof (_count) == "number") { resultQH.limit(_count) }
            if(typeof (_wheres) == "object") { for(let index in _wheres) {
                resultQH.where(QH.requestField(_wheres[index][0]), _wheres[index][1] , _wheres[index][2])
            }}
            resultQH = await resultQH;

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, 'buildings.photo');

            //добавление координат
            QH.resultAddLatLon(resultQH, 'buildings.coordinates');

            result = resultQH;

        }catch (e) {}
        return result;
    }
    async getById(_orgId, _buiId) {
        let result = [];
        try {

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);

            //здание
            let resultQH = QH.requestSelect("buildings")
                .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                .where(QH.requestField('organizations.id'), '=' , _orgId)
                .where(QH.requestField('buildings.active'), '=' , QH.sv(true))
                .where(QH.requestField('buildings.id'), '=' , _buiId)
                .limit(1)
            resultQH = await resultQH;

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, 'buildings.photo');
            //добавление координат
            QH.resultAddLatLon(resultQH, 'buildings.coordinates');

            result = resultQH;;

        }catch (e) {}
        return result;
    }
    async getByAlias(_orgId, _buiAlias) {
        let result = [];
        try {

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);

            //здание
            let resultQH = QH.requestSelect("buildings")
                .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                .where(QH.requestField('organizations.id'), '=' , _orgId)
                .where(QH.requestField('buildings.active'), '=' , QH.sv(true))
                .where(QH.requestField('buildings.alias'), '=' , _buiAlias)
                .limit(1)
            resultQH = await resultQH;

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, 'buildings.photo');
            //добавление координат
            QH.resultAddLatLon(resultQH, 'buildings.coordinates');

            result = resultQH;

        }catch (e) {}
        return result;
    }
    async getAllExtended(_orgId, _wheres, _count) {
        let result = [];
        try {

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);

            //здания
            let resultQH = QH.requestSelect("buildings")
                .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                .where(QH.requestField('organizations.id'), '=' , _orgId)
                .where(QH.requestField('buildings.active'), '=' , QH.sv(true))
                .orderBy(QH.requestField('buildings.order', QH.order()))
                .orderBy(QH.requestField('buildings.id', QH.orderId()))
            if(typeof (_count) == "number") { resultQH.limit(_count) }
            if(typeof (_wheres) == "object") { for(let index in _wheres) {
                resultQH.where(QH.requestField(_wheres[index][0]), _wheres[index][1] , _wheres[index][2])
            }}
            resultQH = await resultQH;

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, 'buildings.photo');
            //добавление координат
            QH.resultAddLatLon(resultQH, 'buildings.coordinates');

            QH.setUseJoin(false, false, true);

            //подразделения
            let resultQHsub = QH.requestSelect("subdivisions")
                .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                .where(QH.requestField('organizations.id'), '=' , _orgId)
                .where(QH.requestField('subdivisions.active'), '=' , QH.sv(true))
            resultQHsub = await resultQHsub;

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQHsub, 'subdivisions.photo');
            QH.resultAddFilesLink(resultQHsub, 'subdivisions.managerPhoto');
            //добавление координат
            QH.resultAddLatLon(resultQHsub, 'subdivisions.coordinates');

            //добавление массивов связей
            resultQH = QH.fromResultGetExtendedResult(resultQH, resultQHsub, "buildings.id", "subdivisions.buildingMain", "buildings.subdivisionsMainArray");
            resultQH = QH.fromResultGetExtendedResult(resultQH, resultQHsub, "buildings.id", "subdivisions.buildings", "buildings.subdivisionsArray", true);

            result = resultQH;

        }catch (e) {
            console.log(e)

        }
        return result;
    }

    async search(_value, _wheres, _count) {
        let result = [];
        try {
            let value = "%"+_value+"%" ;

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);

            //здания
            let resultQH = QH.requestSelect("buildings")
                .where((builder) => builder
                    .where(QH.requestField('organizations.active'), '=' , QH.sv(true))
                    .where(QH.requestField('buildings.active'), '=' , QH.sv(true))
                )
                .andWhere((builder) => builder
                    .where(QH.requestField('buildings.name'), 'LIKE' , value)
                    .orWhere(QH.requestField('buildings.alias'), 'LIKE' , value)
                    .orWhere(QH.requestField('buildings.address'), 'LIKE' , value)
                    .orWhere(QH.requestField('buildings.coordinates'), 'LIKE' , value)
                )
                .orderBy(QH.requestField('organizations.order', QH.order()))
                .orderBy(QH.requestField('organizations.id', QH.orderId()))
                .orderBy(QH.requestField('buildings.order', QH.order()))
                .orderBy(QH.requestField('buildings.id', QH.orderId()))
            if(typeof (_count) == "number") { resultQH.limit(_count) }
            if(typeof (_wheres) == "object") { for(let index in _wheres) {
                resultQH.where(QH.requestField(_wheres[index][0]), _wheres[index][1] , _wheres[index][2])
            }}
            resultQH = await resultQH;

            //добавление ссылок файлов
            QH.resultAddFilesLink(resultQH, 'buildings.photo');
            //добавление координат
            QH.resultAddLatLon(resultQH, 'buildings.coordinates')

            result = resultQH;

        }catch (e) {}
        return result;
    }

    async syncByOid(_orgOid, _buiOid) {
        let result      = {oid: _subOid, result: "error"};
        let checkChang  = function(_newObj, _oldObj){
            let result  = {};
            for(var index in _newObj) {
                if( index != "coordinates" && !global.empty(_newObj[index]) && _newObj[index] != _oldObj[index] ){
                    result[index] = _newObj[index];
                }
                if( index == "coordinates" && !global.empty(_newObj[index]) && "[map]"+_newObj[index]+"[/map]" != _oldObj[index] ){
                    result[index] = "[map]"+_newObj[index]+"[/map]";
                }
            }
            return result;
        }
        let checkCreate = function(_newObj){
            let result  = {};
            for(var index in _newObj) {
                result[index] = _newObj[index];
                if( index != "coordinates" ){
                    result[index] = "[map]"+_newObj[index]+"[/map]";
                }
                if( index != "alias" ){
                    result[index] = QH.generationAlias(_newObj[index]);
                }
            }
            return result;
        }

        try {
            let RH = new Application.Library.RequestHelper();
            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, false);

            //здание QH
            let resultQH = QH.requestSelect("buildings")
                .where(QH.requestField('buildings.oid'), '=' , _buiOid)
                .limit(1)
            resultQH = await resultQH;

            //здание RH
            let link        = RH.map['purs'].paths.mo + "/" + _orgOid + "/" + RH.map['purs'].paths.sub + "/" + RH.map['purs'].paths.build_ + _subOid;
            let resultRH    = await RH.request( link );

            //синхронизация существующей записи
            if( !global.empty(resultQH) && !global.empty(resultQH[0]) ){
                //здание RH - не пусто
                if( resultRH.status && !global.empty(resultRH.response) ){
                    //проверка наличия изменений
                    let resObj = {
                        name:           resultRH.response.name,
                        address:        resultRH.response.address,
                        coordinates:    resultRH.response.coordinates,
                    }
                    let resultCheck = checkChang(resObj, resultQH[0].buildings);

                    //изменение
                    if( Object.keys(resultCheck).length > 0 ){
                        let resultEdit = await this.editItem(resultQH[0].buildings.id, resultCheck);
                        if( !global.empty(resultEdit) )
                            result = {oid: _buiOid, result: "edit"};
                    }else{  result = {oid: _buiOid, result: null};}
                }

                //изменение RH - пусто
                if( !resultRH.status && global.empty(resultRH.response) ){
                    //удаление
                    let resultDelete = await this.deleteItem(resultQH[0].buildings.id);
                    if( !global.empty(resultDelete) )
                        result = {oid: _buiOid, result: "delete"};
                }
            }

            //синхронизация новой записи
            if( global.empty(resultQH) ){
                //здание RH - не пусто
                if(resultRH.status && !global.empty(resultRH.response) ){
                    //родительская запись
                    QH.setUseJoin(false, false, false);
                    let resultQHparent = QH.requestSelect("organizations")
                        .where(QH.requestField('organizations.oid'), '=' , _orgOid)
                        .limit(1)
                    resultQHparent = await resultQHparent;

                    //принятие изменения
                    if( !global.empty(resultQHparent) && !global.empty(resultQHparent[0]) ){
                        let resObj = {
                            parent_item_id: resultQHparent[0].organizations.id,
                            oid:            _buiOid,
                            alias:          resultRH.response.name,
                            name:           resultRH.response.name,
                            fullname:       resultRH.response.name,
                            shortname:      resultRH.response.name,
                            address:        resultRH.response.address,
                            coordinates:    resultRH.response.coordinates,
                            active:         QH.sv(true),
                            order:          50,
                        }

                        let resultCheck = checkCreate(resObj);

                        let resultCrete = await this.createItem(resultCheck);
                        if( !global.empty(resultCrete) )
                            result = {oid: _buiOid, result: "create"};
                    }
                }
            }

        }catch (e) {}

        return result;
    }

    async editItem(_buiId, _values) {
        let result = [];
        try {
            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, false);

            //изменение записи
            let requestUpdate =  QH.requestUpdate("buildings");
                requestUpdate.where(QH.requestField('buildings.id'), '=' ,  _buiId);
            for(var index in _values) {
                requestUpdate.update(QH.requestField('buildings.'+index), _values[index]);
            }
            requestUpdate = await requestUpdate;

            result = requestUpdate;
        }catch (e) {}
        return result;
    }
    async deleteItem(_buiId) {
        let result = [];
        try {
            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, false);

            //удаление значений в ссылающихся записях
            let ModelSubdivisions = new Application.Model.Subdivisions();
            let resultSubdivisionsBuildingMain  = await ModelSubdivisions.deleteJoinArrayItem("buildingMain", _buiId);
            let resultSubdivisionsBuildings     = await ModelSubdivisions.deleteJoinArrayItem("buildings", _buiId);

            //удаление записи
            let requestDelete = QH.requestDeleteByIds("buildings", [_buiId]);
            requestDelete = await requestDelete;

            result = requestDelete;
        }catch (e) {}
        return result;
    }
    async createItem(_values) {
        let result = [];
        try {
            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, false);

            //создание записи
            let requestInsert =  QH.requestInsert("buildings", _values);
            requestInsert = await requestInsert;

            result = requestInsert;
        }catch (e) {}
        return result;
    }

}
