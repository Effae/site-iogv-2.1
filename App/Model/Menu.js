module.exports = class extends Application.System.Model {

    // !!! специальной метод - возвращает результата преобразований Merge, Extended  !!!
    async getMenu(_orgType, _lang, _menuType) {
        let result = [];
        try {

            let QH = new Application.Library.QueryHelper();
            QH.setUseAlias(true).setUseJoin(false, false, true);

            //дефолтный язык
            let resultQH = QH.requestSelect("menuLocal")
                .where((builder) => builder
                    .where(QH.requestField('menu.active'), '=' , QH.sv(true))
                    .where(QH.requestField('menuLocal.active'), '=' , QH.sv(true))
                    .where(QH.requestField('menuLocal.lang'), '=' , QH.sv(QH.dl()))
                )
                .andWhere((builder) => builder
                    .where(QH.requestField('menu.types'), 'REGEXP' , "(^|[^0-9])"+_menuType+"([^0-9]|$)")
                    .orWhere(QH.requestField('menu.types'), '=' , 0)
                    .orWhere(QH.requestField('menu.types'), '=' , "")
                )
                .andWhere((builder) => builder
                    .where(QH.requestField('menu.orgtypes'), 'REGEXP' , "(^|[^0-9])"+_orgType+"([^0-9]|$)")
                    .orWhere(QH.requestField('menu.orgtypes'), '=' , 0)
                    .orWhere(QH.requestField('menu.orgtypes'), '=' , "")
                )
                .orderBy(QH.requestField('menu.order', QH.order()))
                .orderBy(QH.requestField('menu.id', QH.orderId()))
                .orderBy(QH.requestField('menuLocal.order', QH.order()))
                .orderBy(QH.requestField('menuLocal.id', QH.orderId()))
            resultQH = await resultQH;
            
            //локализации
            if(_lang != QH.dl()){

                let resultQHlocal = QH.requestSelect("menuLocal")
                    .where((builder) => builder
                        .where(QH.requestField('menu.active'), '=' , QH.sv(true))
                        .where(QH.requestField('menuLocal.active'), '=' , QH.sv(true))
                        .where(QH.requestField('menuLocal.lang'), '=' , QH.sv(_lang))
                    )
                    .andWhere((builder) => builder
                        .where(QH.requestField('menu.types'), 'REGEXP' , "(^|[^0-9])"+_menuType+"([^0-9]|$)")
                        .orWhere(QH.requestField('menu.types'), '=' , 0)
                        .orWhere(QH.requestField('menu.types'), '=' , "")
                    )
                    .andWhere((builder) => builder
                        .where(QH.requestField('menu.orgtypes'), 'REGEXP' , "(^|[^0-9])"+_orgType+"([^0-9]|$)")
                        .orWhere(QH.requestField('menu.orgtypes'), '=' , 0)
                        .orWhere(QH.requestField('menu.orgtypes'), '=' , "")
                    )
                resultQHlocal = await resultQHlocal;

                //слитие результатов
                QH.resultMerge(resultQH, resultQHlocal, 'menu.id');
            }

            //функция построения структуры меню
            let genMenu = function (_childrensArray, _parentId) {
                for(let index in resultQH) {
                    let item = resultQH[index];

                    if(item.menu.parent == _parentId){
                        let newObj = item;
                            newObj['childrens']     = [];
                            newObj['isInternal']    = (newObj.menu.internal == "true") ? true : false;

                        _childrensArray.push(newObj);
                        genMenu(newObj['childrens'], newObj.menu.id);
                    }
                }
            }

            //постоение структуры меню
            genMenu(result, "");

        }catch (e) {}
        return result;
    }

}
