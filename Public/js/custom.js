$(document).ready(function () {

    initSubVersion();
    logoAutoSize();
    stickingMenu();

    // ОТРЕДАКТИРОВАННО ДЛЯ ВЕРСИИ 2.1

    //ВИД

        //сворачивание-разворачивание Карты сайта
        $('.system_slideButton_siteMap').on('click', function () {
            $(this).toggleClass('system_slideOpen');
            $('.system_slide_siteMap').slideToggle();
        });

        //сворачивание-разворачивание Языковой панели
        $('.system_slideButton_langEdit').on('click', function () {
            $(this).toggleClass('system_slideOpen');
            $('.system_slide_langEdit').slideToggle();
            //$('.system_slideLangEdit').toggle("slide");
        });

    //ФУНКЦИИ

        //Функция смены языка
        if ($('.system_langEdit').length > 0) {

            init();

            function init() {
                for (let i = 0; i < $('.system_langEdit').length; i++) {
                    let _domObj = $('.system_langEdit')[i];

                    $(_domObj).on('click', function (e) {
                        try {
                            let useLang     = ( _domObj.getAttribute('useLang') !== null ) ? _domObj.getAttribute('useLang') : null;
                            if (useLang){
                                let url         = new URL(String(window.location));
                                let pathArray   = url.pathname.split('/');
                                    pathArray[1]= useLang;
                                window.location.href = url.origin  + pathArray.join("/");
                            }
                        }catch (e) {}
                    });
                }
            }
        }

        //Функция вкл/выкл режима слабовидящих
        if ($('.system_subClass').length > 0) {

            init();

            function init() {
                for (let i = 0; i < $('.system_subClass').length; i++) {
                    let _domObj = $('.system_subClass')[i];

                    $(_domObj).on('click', function (e) {
                        //TODO после чистки CCS удалить изменение класса active
                        try {
                            $('.system_subClass').toggleClass('system_activeOn');
                            $('.system_subClass').toggleClass('system_activeOff');
                            $('.system_subClass').toggleClass('active');

                            $('body').toggleClass('sub');

                            if ($('body').hasClass('sub')) {
                                $('.system_subClassControls').addClass('system_activeOn').addClass('active');
                                $('.system_subClassControls').removeClass('system_activeOff');
                            }else{
                                $('.system_subClassControls').removeClass('system_activeOn').removeClass('active');
                                $('.system_subClassControls').addClass('system_activeOff');
                            }

                            localStorage.setItem("sub_version", $('body').hasClass('sub'));
                        }catch (e) {}
                    });
                }
            }
        }

        //Функция изменения параметра режима слабовидящих
        if ($('.system_subClassControls_item').length > 0) {

            init();

            function init() {

                for (let i = 0; i < $('.system_subClassControls_item').length; i++) {
                    let _domObj = $('.system_subClassControls_item')[i];

                    $(_domObj).on('click', function (e) {

                        //изменение  шрифта
                        if( _domObj.getAttribute('size') !== null ){
                            let value = _domObj.getAttribute('size');

                            $('html').removeClass('large medium small').addClass(value);

                            localStorage.setItem("sub_version", $('body').hasClass('sub'));
                            localStorage.setItem('sub_font_size', value);
                        }

                        //изменение фона
                        let background = null;
                        if( _domObj.getAttribute('background') !== null ){
                            let value = _domObj.getAttribute('background');

                            $('html').removeClass('light dark blue').addClass(value);

                            localStorage.setItem("sub_version", $('body').hasClass('sub'));
                            localStorage.setItem('sub_bg_color', value);
                        }

                        let sizeValue               = localStorage.getItem('sub_font_size');
                        let backgroundValue         = localStorage.getItem('sub_bg_color');

                        //установка активных элементов управления
                        let items = $('.system_subClassControls_item')
                        for (let i = 0; i < items.length; i++) {
                            if( items[i].getAttribute('size') != null && items[i].getAttribute('size') != sizeValue ){
                                $(items[i]).removeClass('active');
                                continue;
                            }
                            if( items[i].getAttribute('background') != null && items[i].getAttribute('background') != backgroundValue ){
                                $(items[i]).removeClass('active');
                                continue;
                            }
                            $(items[i]).addClass('active');
                        }

                    });
                }
            }
        }

        //Функция загрузки из localStorage параметров режима слабовидящих
        function initSubVersion() {

            let sizeValue           = 'small';
            let backgroundValue     = 'blue';

            //прроверка активности панели
            if ( localStorage.getItem('sub_version') == 'true') {

                sizeValue               = localStorage.getItem('sub_font_size')
                backgroundValue         = localStorage.getItem('sub_bg_color')

                $('body').toggleClass('sub');

                $('.system_subClass').toggleClass('system_activeOn');
                $('.system_subClass').toggleClass('system_activeOff');
                $('.system_subClass').toggleClass('active')

                $('.system_subClassControls').addClass('system_activeOn').addClass('active');
            }

            //установка значений
            $('html').addClass(sizeValue);
            $('html').addClass(backgroundValue);

            //установка активных элементов управления
            let items = $('.system_subClassControls_item')
            for (let i = 0; i < items.length; i++) {
                if( items[i].getAttribute('size') != null && items[i].getAttribute('size') != sizeValue ){
                    $(items[i]).removeClass('active');
                    continue;
                }
                if( items[i].getAttribute('background') != null && items[i].getAttribute('background') != backgroundValue ){
                    $(items[i]).removeClass('active');
                    continue;
                }
                $(items[i]).addClass('active');
            }

        }

        //Функция загрузки из localStorage параметров режима слабовидящих
        function logoAutoSize() {
            //оперeделение размера блока
            let availableHeight = 50;
            if ($('.system_logoImg').height() > 50) availableHeight = $('.system_logoImg').height();

            //оперeделение текущего шрифта
            let logo_text   = $('.system_logoSpan');
            let fontSize    = parseInt(logo_text.css('font-size'));
            let height      = logo_text.height();

            //уменьшение размера шрифта
            while (height > availableHeight) {
                fontSize    = fontSize - 1;
                logo_text.css({fontSize: fontSize + 'px'});
                height      = logo_text.height();
            }
        }

        //Функция прилепания меню
        function stickingMenu() {
            //Липкое меню
            let menu            = $("nav");
            let menu_top        = $("nav").offset().top;
            let header          = $("header");
            let header_height   = $("header").height();

            menu.addClass("default");

            //прокрутка
            $(window).scroll(function () {
                if ($(window).width() > 1120) {
                    if ($(this).scrollTop() > menu_top && menu.hasClass("default")) {
                        header.css('height', header_height);
                        menu.removeClass("default").addClass("sticky");
                    } else if ($(this).scrollTop() <= menu_top && menu.hasClass("sticky")) {
                        menu.removeClass("sticky").addClass("default");
                    }
                }
            });
        }

    //ИНИЦИАЛИЗАЦИИ

        //Инициализация слайдер
        if ($('.system_carouselObject').length > 0) {

            $('.system_carouselObject').owlCarousel({
                items: 1,
                loop: true,
                nav: true,
                dots: true
            })
        }

        //Инициализация карты c обьетом
        if ( $('.system_mapObject').length > 0 ) {

            ymaps.ready(init);

            function trflValue(_value){
                let result = false;
                try {
                    if( typeof(_value) == "string") _value = _value.toLowerCase();
                    if( typeof(_value) == "undefined", _value == null, _value == false, _value == "false" ){
                        result = false;
                    }else{
                        result = true;
                    }
                }catch (e) {}
                return result;
            }
            function init() {
                for (let i = 0; i < $('.system_mapObject').length; i++) {
                    let _domObj = $('.system_mapObject')[i];

                    //настройки отображения карты и обекта
                    let zoom                    = ( _domObj.getAttribute('zoom') !== null )              ? _domObj.getAttribute('zoom')              : 7;
                    let latitude                = ( _domObj.getAttribute('latitude') !== null )          ? _domObj.getAttribute('latitude')          : '0,0';
                    let longitude               = ( _domObj.getAttribute('longitude') !== null )         ? _domObj.getAttribute('longitude')         : '0,0';
                    let preset                  = ( _domObj.getAttribute('preset') !== null )            ? _domObj.getAttribute('preset')            : 'islands#darkBlueStretchyIcon';
                    let balloonUse              = ( _domObj.getAttribute('balloonUse') !== null )        ? trflValue(_domObj.getAttribute('balloonUse'))        : false;
                    let balloonHeader           = ( _domObj.getAttribute('balloonHeader') !== null )     ? _domObj.getAttribute('balloonHeader')     : '';
                    let balloonBody             = ( _domObj.getAttribute('balloonBody') !== null )       ? _domObj.getAttribute('balloonBody')       : '';
                    let balloonHidePreset       = ( _domObj.getAttribute('balloonHidePreset') !== null ) ? trflValue(_domObj.getAttribute('balloonHidePreset')) : false;

                    //инициализация карты
                    let map = new ymaps.Map(_domObj, {
                        center: [latitude, longitude],
                        zoom:   zoom
                    });

                    //инициализация обьекта
                    let objCoordinates          = [latitude, longitude];
                    let objPreset               = {preset: preset, hideIconOnBalloonOpen: balloonHidePreset};
                    let objBalloon              = {};
                    if( balloonUse ) objBalloon = {
                        balloonContentHeader    : '<h3 class="balloon-head">' + balloonHeader + '</h3>',
                        balloonContentBody      : '<div class="baloon-content">' + balloonBody + '</div>',
                    }

                    //добавление обьекта на карту
                    map.geoObjects.add(new ymaps.Placemark(objCoordinates, objBalloon, objPreset));
                }
            }
        }

        //Инициализация таблицы
        if ( $('.system_tableObject').length > 0) {

            init();

            function trflValue(_value){
                let result = false;
                try {
                    if( typeof(_value) == "string") _value = _value.toLowerCase();
                    if( typeof(_value) == "undefined", _value == null, _value == false, _value == "false" ){
                        result = false;
                    }else{
                        result = true;
                    }
                }catch (e) {}
                return result;
            }
            function init() {
                for (let i = 0; i < $('.system_tableObject').length; i++) {
                    let _domObj = $('.system_tableObject')[i];
                    let _domObjThead            = _domObj.getElementsByTagName("thead");
                    let _domObjTbody            = _domObj.getElementsByTagName("tbody");

                    //параметры тфблицы
                    let tableObj        = {};

                        tableObj.spoilerClass       = ( _domObj.getAttribute('spoilerClass') !== null )     ? _domObj.getAttribute('spoilerClass')          : "";
                        tableObj.spoilerDownTime    = ( _domObj.getAttribute('spoilerDownTime') !== null )  ? _domObj.getAttribute('spoilerDownTime')       : 200;
                        tableObj.spoilerUpTime      = ( _domObj.getAttribute('spoilerUpTime') !== null )    ? _domObj.getAttribute('spoilerUpTime')         : 200;
                        tableObj.autoWidth          = ( _domObj.getAttribute('autoWidth') !== null )        ? trflValue(_domObj.getAttribute('autoWidth'))  : false;

                        tableObj.lengthStart    = ( _domObj.getAttribute('lengthStart') !== null )  ? _domObj.getAttribute('lengthStart')   : "Показать";
                        tableObj.lengthEnd      = ( _domObj.getAttribute('lengthEnd') !== null )    ? _domObj.getAttribute('lengthEnd')     : "записей";
                        tableObj.zero           = ( _domObj.getAttribute('zero') !== null )         ? _domObj.getAttribute('zero')          : "Нет подходящих результатов";
                        tableObj.empty          = ( _domObj.getAttribute('empty') !== null )        ? _domObj.getAttribute('empty')         : "Нет записей";
                        tableObj.search         = ( _domObj.getAttribute('search') !== null )       ? _domObj.getAttribute('search')        : "Поиск";
                        tableObj.infoStart      = ( _domObj.getAttribute('infoStart') !== null )    ? _domObj.getAttribute('infoStart')     : "Всего";
                        tableObj.infoEnd        = ( _domObj.getAttribute('infoEnd') !== null )      ? _domObj.getAttribute('infoEnd')       : "записей";
                        tableObj.next           = ( _domObj.getAttribute('next') !== null )         ? _domObj.getAttribute('next')          : "Вперед";
                        tableObj.previous       = ( _domObj.getAttribute('previous') !== null )     ? _domObj.getAttribute('previous')      : "Назад";

                    //параметры столбцов
                    let trArrayObj      = [];
                    let trArrayDomObj   = _domObj.getElementsByTagName("th");
                    for (let i = 0; i < trArrayDomObj.length; i++) {
                        let trObj       = {};
                        trObj.name      = trArrayDomObj[i].textContent;
                        trObj.data      = ( trArrayDomObj[i].getAttribute('data') !== null )         ? trArrayDomObj[i].getAttribute('data')         : "field" + i;
                        trObj.class     = ( trArrayDomObj[i].getAttribute('class') !== null )        ? trArrayDomObj[i].getAttribute('class')        : "field" + i;
                        trObj.sortable  = ( trArrayDomObj[i].getAttribute('sortable') !== null )     ? trflValue(trArrayDomObj[i].getAttribute('sortable'))     : true;
                        trObj.visible   = ( trArrayDomObj[i].getAttribute('visible') !== null )      ? trflValue(trArrayDomObj[i].getAttribute('visible'))      : true;
                        trObj.orderable = ( trArrayDomObj[i].getAttribute('orderable') !== null )    ? trflValue(trArrayDomObj[i].getAttribute('orderable'))    : true;

                        //спойлер
                        trObj.inSpoiler = ( trArrayDomObj[i].getAttribute('inSpoiler') !== null )    ? trflValue(trArrayDomObj[i].getAttribute('inSpoiler'))    : false;

                        trArrayObj.push(trObj)
                    }

                    //настройки language
                    let languageValue = {
                        lengthMenu:     tableObj.lengthStart + " " + "_MENU_" + " " + tableObj.lengthEnd,
                        zeroRecords:    tableObj.zero,
                        emptyTable:     tableObj.empty,
                        search:         tableObj.search,
                        info:           tableObj.infoStart + " " + "_TOTAL_" + " " + tableObj.infoEnd,
                        paginate:       {next: tableObj.next, previous: tableObj.previous}
                    }

                    //настройки columns
                    let columnsValue = [];
                    let useSpoiler   = false;
                    for (let i = 0; i < trArrayObj.length; i++) {
                        columnsValue[i] = {
                            "data"      : trArrayObj[i].data,
                            "className" : trArrayObj[i].class,
                            "visible"   : trArrayObj[i].visible,
                            "orderable" : trArrayObj[i].orderable,
                            "sortable"  : trArrayObj[i].sortable,
                        }
                        if ( trArrayObj[i].inSpoiler ){
                            columnsValue[i]["visible"]  = false;
                            useSpoiler                  = true;
                        }
                    }

                    //создание таблицы
                    let table = $(_domObj).DataTable({
                        info: true,
                        dom: '<"row"lf>tp',
                        autoWidth: tableObj.autoWidth,
                        language: languageValue,
                        columns:columnsValue,
                    });

                    //спойлер
                    if( useSpoiler ){
                        //создание спойлера
                        function createSpoiler(_data) {
                            let result = "";
                            try {
                                for (let i = 0; i < trArrayObj.length; i++) {
                                    if( trArrayObj[i].inSpoiler ){
                                        let item = '<div class="system_table_spoiler_content_item"><h3>' + trArrayObj[i].name + '</h3><p>' + _data[trArrayObj[i].data] + '</p></div>';
                                        result = result + item;
                                    }
                                }
                            }catch (e) {}
                            return result;
                        }

                        //управление спойлером
                        $(_domObjTbody).on('click', 'tr', function () {
                            let tr  = $(this).closest('tr');
                            let row = table.row(tr);

                            if (row.child.isShown()) {
                                //закрытие спойлера
                                tr.next().find('.system_table_spoiler_content').slideUp( tableObj.slideUpTime, function() { row.child.hide(); });
                            } else {
                                //открытие спойлера
                                row.child('<div class="system_table_spoiler_content">' + createSpoiler(row.data()) + '</div>').show();
                                tr.addClass('system_table_spoilerShown').next().addClass('system_table_spoiler').addClass(tableObj.spoilerClass).find('.system_table_spoiler_content').hide().slideDown(tableObj.slideDownTime);
                            }
                        });
                    }
                }
            }

        }



    // МОБИЛЬНАЯ ВЕРСИЯ (редактировние 2.1 не проводилось)

    //Кнопка мобильного меню
    $('.mobmenu-bt').on('click', function () {
        // $('.mobmenu-bt').toggleClass('active');
        $('nav').toggleClass('open');
        $('body').toggleClass('mobmenu-open');
    });

    //Пункты мобильного меню
    $('nav li>i').on('click', function () {
        $(this).parent().toggleClass('open');
        $(this).next('.nav__submenu').slideToggle(200);
    });
    $('.lang>ul').hide();

    //Переносим поиск в мобильное меню и закрепляем хедер
    if (win.width() < 1170) {
        var search = $('section.top .search');
        $('header nav .mobmenu-search').prepend(search.html())
        search.detach();
        $('header').addClass('sticky');
    }

});