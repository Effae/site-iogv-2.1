$(document).ready(function () {
    var loc = window.location.href.slice(0, window.location.href.indexOf('\?'));
    loc = document.location.pathname; 
    var res = getUrlVar();
    if(res){
        // if(res.type) {
        //         //     $('#objecttype>option[value='+res.type+']').attr("selected", "selected");
        //         // }
        //         // if(res.organisation){
        //         //     $('#organisation>option[value='+res.organisation+']').attr("selected", "selected");
        //         // }
    }
    $('#objecttype').change(function(){
        var type = $(this).val();                
        location.href = loc+'?type='+type;
    });
    $('#organisation').change(function(){
        var organisation = $(this).val();
        location.href = loc+'?organisation='+organisation;
    });

    function getUrlVar(){
        var urlVar = window.location.search; // получаем параметры из урла        
        var arrayVar = []; // массив для хранения переменных
        var valueAndKey = []; // массив для временного хранения значения и имени переменной
        var resultArray = []; // массив для хранения переменных
        arrayVar = (urlVar.substr(1)).split('&'); // разбираем урл на параметры        
        if(arrayVar[0]=="") return false; // если нет переменных в урле
        for (i = 0; i < arrayVar.length; i ++) { // перебираем все переменные из урла
            valueAndKey = arrayVar[i].split('='); // пишем в массив имя переменной и ее значение
            resultArray[valueAndKey[0]] = valueAndKey[1]; // пишем в итоговый массив имя переменной и ее значение
        }
        return resultArray; // возвращаем результат
    }
});