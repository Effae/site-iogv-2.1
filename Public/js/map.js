$(document).ready(function () {
    var MOmapOrganizations    = null;
    var MOdefaultZoom         = 7;
    var MOfocusZoom           = 7;
    var MOdefaultLocation     = [51.741274, 39.178843];

    ymaps.ready(MOinitMapOrganizations);

    //инициализация карты
    function MOinitMapOrganizations() {
        try {
            //проверка заданных кординат
            let geolocation = ymaps.geolocation;
            if( geolocation.latitude && geolocation.longitude )
                MOdefaultLocation  = [geolocation.latitude, geolocation.longitude];

            //инициализация карты
            MOmapOrganizations = new ymaps.Map("system_mapOrganizations", {
                center: MOdefaultLocation,
                zoom: MOdefaultZoom
            });

            //инициализация обьектов
            $('#system_selectOrganizationsType').change();

        }catch (e) {}
    }

    //создание обьектов карты
    function MOcreateObjects(){
        try {
            if(MOmapOrganizations){

                //обход списка организаций
                $('#system_selectOrganizations option').each(function () {

                    let lastPoint       = null;
                    //добавление обьектов с данными на карту
                    if ($(this).val() != '') {

                        let name        = $(this).text();
                        let latitude    = $(this).attr('latitude')
                        let longitude   = $(this).attr('longitude');
                        let link        = $(this).attr('link');
                        lastPoint       = [latitude,longitude]

                        MOmapOrganizations.geoObjects.add(
                            new ymaps.Placemark([latitude,longitude], {
                                balloonContentHeader: '<h3 class="baloon-header">' + name + '</p>',
                                balloonContent: '<p class="baloon-content"><a  href="' + link + '" target="_blank"><i class="fa fa-external-link" aria-hidden="true" style="margin-right:5px"></i>Перейти на сайт организации</a></p>'
                            }, {
                                preset: 'islands#darkBlueStretchyIcon',
                                hideIconOnBalloonOpen: false
                            })
                        );
                    }
                    //центрирование
                    if(MOmapOrganizations.geoObjects.getLength()>1)
                        MOmapOrganizations.setBounds(MOmapOrganizations.geoObjects.getBounds());
                    if(MOmapOrganizations.geoObjects.getLength()==1)
                        MOmapOrganizations.setBounds(MOmapOrganizations.geoObjects.getBounds());
                    if(MOmapOrganizations.geoObjects.getLength()==0)
                        MOmapOrganizations.setCenter(MOdefaultLocation);

                });

            }
        }catch (e) {}
    }

    //удаление обьектов карты
    function MOdeleteObjects(){
        try {
            if(MOmapOrganizations){
                MOmapOrganizations.balloon.close();
                MOmapOrganizations.geoObjects.removeAll();
            }
        }catch (e) {}
    }

    //изменение организации
    $('#system_selectOrganizations').change(function () {
        try {

            if ($(this).val() == '') {
                //"Выбрать все"
                if(MOmapOrganizations.geoObjects.getLength()>1)
                    MOmapOrganizations.setBounds(MOmapOrganizations.geoObjects.getBounds());
                if(MOmapOrganizations.geoObjects.getLength()==1)
                    MOmapOrganizations.setBounds(MOmapOrganizations.geoObjects.getBounds());
                if(MOmapOrganizations.geoObjects.getLength()==0)
                    MOmapOrganizations.setCenter(MOdefaultLocation);
            } else {
                //организация
                let latitude = $(this).children('option:selected').attr('latitude')
                let longitude = $(this).children('option:selected').attr('longitude');

                MOmapOrganizations.geoObjects.get( $(this)[0].selectedIndex-1 ).balloon.open();
                MOmapOrganizations.setCenter([latitude,longitude]);
            }

        }catch (e) {}
    });

    //изменение типа организации
    $('#system_selectOrganizationsType').change( function (){
        try {
            //очистка карты
            MOdeleteObjects();

            //запрос списка организаций
            $.ajax({
                type: 'get',
                url: '/ajax/' + 'organizationsForMap',
                data: { 'id': $(this).val() },
                success: function (data) {
                    //установка полученных данных
                    $('#system_selectOrganizations').html(data);
                    MOcreateObjects();
                }
            });

        }catch (e) {}
    });

});